package main

import (
	"archive/zip"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"regexp"
	"slices"
	"strings"
	"time"

	"github.com/y-bash/go-gaga"
	"gitlab.com/castaneai/misc/go/bear2dayone"
)

var (
	embeddedImagePatternV1 = regexp.MustCompile(`\[(assets/.+)]`)
	embeddedImagePatternV2 = regexp.MustCompile(`!\[]\((assets/.+)\)`)
	datePattern            = regexp.MustCompile(`[0-9]{4}/[0-9]{2}/[0-9]{2}`)
)

func main() {
	log.SetFlags(0)
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s <Bear2bk file> <DayOne file> <DayOne JournalName>\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()

	bearFile := flag.Arg(0)
	dayOneFile := flag.Arg(1)
	journalName := flag.Arg(2)
	if bearFile == "" || dayOneFile == "" || journalName == "" {
		flag.Usage()
		os.Exit(2)
	}

	// Normalize Japanese on filename (NFD->NFC)
	// https://qiita.com/y-bash/items/8f23c34571871ba8f52e
	norm, err := gaga.Norm(gaga.ComposeVom)
	if err != nil {
		log.Fatalf("failed to initialize japanese normalizer: %+v", err)
	}

	bearBackup, err := bear2dayone.OpenBearBackup(bearFile, norm)
	if err != nil {
		log.Fatalf("failed to load textbundle: %+v", err)
	}
	defer bearBackup.Close()

	f, err := os.Create(dayOneFile)
	if err != nil {
		log.Fatalf("failed to open '%s' for create: %+v", dayOneFile, err)
	}
	defer f.Close()

	zw := zip.NewWriter(f)
	dayOneExport := bear2dayone.NewDayOneExport("1.0")

	var notes []*bear2dayone.BearNote
	for _, note := range bearBackup.Notes {
		if isDiaryNote(note) {
			notes = append(notes, note)
		}
	}
	slices.SortFunc(notes, func(a, b *bear2dayone.BearNote) int {
		t1, err := parseDate(a.Info.CreationDate)
		if err != nil {
			log.Printf("failed to parse date '%s': %+v", a.Info.CreationDate, err)
		}
		t2, err := parseDate(b.Info.CreationDate)
		if err != nil {
			log.Printf("failed to parse date '%s': %+v", b.Info.CreationDate, err)
		}
		return int(t1.Unix() - t2.Unix())
	})

	for _, note := range notes {
		dayOneEntry, err := convertToDayOne(note, zw, norm)
		if err != nil {
			log.Fatalf("failed to convert bear note '%s' to Day One entry: %+v", note.Title(), err)
		}
		summary := note.Title()
		if len(dayOneEntry.Photos) > 0 {
			summary += fmt.Sprintf(" (%d photos)", len(dayOneEntry.Photos))
		}
		log.Printf("converted: %s", summary)
		dayOneExport.Entries = append(dayOneExport.Entries, dayOneEntry)
	}

	journalPath := fmt.Sprintf("%s.json", journalName)
	jw, err := zw.Create(journalPath)
	if err != nil {
		log.Fatalf("failed to open '%s' in Day One export zip: %+v", journalPath, err)
	}
	if err := json.NewEncoder(jw).Encode(dayOneExport); err != nil {
		log.Fatalf("failed to write Day One export JSON: %+v", err)
	}
	if err := zw.Close(); err != nil {
		log.Fatalf("failed to close zip writer: %+v", err)
	}
	log.Printf("finished: %s", dayOneFile)
}

func convertToDayOne(note *bear2dayone.BearNote, zw *zip.Writer, norm *gaga.Normalizer) (*bear2dayone.DayOneEntry, error) {
	creationDate, err := extractDate(note)
	if err != nil {
		return nil, fmt.Errorf("failed to parse creationDate '%s': %w", note.Info.CreationDate, err)
	}
	dayOneEntry := &bear2dayone.DayOneEntry{
		CreationDate: creationDate.UTC().Format(time.RFC3339),
		IsAllDay:     true,
	}
	assetPathToPhoto := map[string]*bear2dayone.DayOnePhoto{}
	for assetPath, asset := range note.Assets {
		photo, err := convertAssetToDayOnePhoto(asset)
		if err != nil {
			return nil, fmt.Errorf("failed to convert bear asset '%s' to Day One photo: %w", assetPath, err)
		}
		assetPathToPhoto[assetPath] = photo
		dayOneEntry.Photos = append(dayOneEntry.Photos, photo)
		if err := copyAssetToDayOneZip(zw, photo, asset); err != nil {
			return nil, err
		}
	}
	dayOneEntry.Text = replaceEmbeddedImage(note, assetPathToPhoto, norm)
	return dayOneEntry, nil
}

func convertAssetToDayOnePhoto(asset *zip.File) (*bear2dayone.DayOnePhoto, error) {
	r, err := asset.Open()
	if err != nil {
		return nil, err
	}
	defer r.Close()

	photo, err := bear2dayone.NewPhoto(r)
	if err != nil {
		return nil, fmt.Errorf("failed to create Day One photo from asset '%s': %w", asset.Name, err)
	}
	return photo, nil
}

func copyAssetToDayOneZip(zw *zip.Writer, photo *bear2dayone.DayOnePhoto, asset *zip.File) error {
	w, err := zw.Create(photo.GetPath())
	if err != nil {
		return fmt.Errorf("failed to open '%s' for create in Day One zip: %w", photo.GetPath(), err)
	}
	r, err := asset.Open()
	if err != nil {
		return fmt.Errorf("failed to open '%s' from bear asset: %w", asset.Name, err)
	}
	defer r.Close()

	if _, err := io.Copy(w, r); err != nil {
		return fmt.Errorf("failed to copy bear asset '%s' to Day One zip: %w", asset.Name, err)
	}
	return nil
}

func replaceEmbeddedImage(note *bear2dayone.BearNote, assetPathToPhoto map[string]*bear2dayone.DayOnePhoto, norm *gaga.Normalizer) string {
	replacedText := note.Text
	for _, pattern := range []*regexp.Regexp{embeddedImagePatternV1, embeddedImagePatternV2} {
		replacedText = pattern.ReplaceAllStringFunc(replacedText, func(s string) string {
			replaced := "<unknown>"
			assetPath, err := url.QueryUnescape(pattern.FindStringSubmatch(s)[1])
			if err == nil {
				assetPath = norm.String(assetPath)
				replaced = fmt.Sprintf("--- asset path to photo mapping not found: '%s' ---", assetPath)
				if photo, ok := assetPathToPhoto[assetPath]; ok {
					replaced = fmt.Sprintf("dayone-moment://%s", photo.Identifier)
				} else {
					log.Printf("asset not found in mapping: '%s' in note '%s'", assetPath, note.Title())
				}
			}
			return fmt.Sprintf(`![](%s)`, replaced)
		})
	}
	return replacedText
}

func isDiaryNote(note *bear2dayone.BearNote) bool {
	if strings.Contains(note.Text, "#diary") {
		note.Text = strings.ReplaceAll(note.Text, "#diary", "")
		return true
	}
	return false
}

func extractDate(note *bear2dayone.BearNote) (time.Time, error) {
	// prefer title date
	matches := datePattern.FindAllString(note.Title(), 1)
	if len(matches) > 0 {
		t, err := time.Parse("2006/01/02", matches[0])
		if err != nil {
			return time.Time{}, fmt.Errorf("failed to parse date from title: '%s': %w", matches[0], err)
		}
		// 12:00:00 UTC
		t = t.Add(12 * time.Hour)
		return t, nil
	}
	t, err := parseDate(note.Info.CreationDate)
	if err != nil {
		return time.Time{}, fmt.Errorf("failed to parse date from info.creationDate: '%s': %w", note.Info.CreationDate, err)
	}
	return t, nil
}

func parseDate(s string) (time.Time, error) {
	// 1. RFC3339 (Bear2 format)
	t, err := time.Parse(time.RFC3339, s)
	if err != nil {
		// 2. Bear1 format
		t, err = time.Parse("2006-01-02T15:04:05-0700", s)
		if err != nil {
			return time.Time{}, err
		}
	}
	return t, nil
}
