module gitlab.com/castaneai/misc/go/bear2dayone

go 1.21.3

require (
	github.com/google/uuid v1.5.0
	github.com/y-bash/go-gaga v0.0.2
)

require github.com/mattn/go-runewidth v0.0.9 // indirect
