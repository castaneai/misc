package bear2dayone

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/google/uuid"
)

type DayOneExport struct {
	Metadata DayOneMetadata `json:"metadata"`
	Entries  []*DayOneEntry `json:"entries"`
}

func NewDayOneExport(version string) *DayOneExport {
	return &DayOneExport{
		Metadata: DayOneMetadata{Version: version},
	}
}

type DayOneMetadata struct {
	Version string `json:"version"`
}

type DayOneEntry struct {
	UUID         string         `json:"uuid,omitempty"`
	Text         string         `json:"text,omitempty"`
	CreationDate string         `json:"creationDate"`
	Photos       []*DayOnePhoto `json:"photos,omitempty"`
	IsAllDay     bool           `json:"isAllDay"`
}

type DayOnePhoto struct {
	Type       string `json:"type"`
	Identifier string `json:"identifier"`
	MD5        string `json:"md5"`
	Width      int    `json:"width,omitempty"`
	Height     int    `json:"height,omitempty"`
}

func (p DayOnePhoto) GetPath() string {
	return fmt.Sprintf("photos/%s.%s", p.MD5, p.Type)
}

func NewIdentifier() string {
	return strings.ToUpper(strings.ReplaceAll(uuid.New().String(), "-", ""))
}

func NewPhoto(r io.Reader) (*DayOnePhoto, error) {
	var b bytes.Buffer
	hash := md5.New()
	w := io.MultiWriter(&b, hash)
	if _, err := io.Copy(w, r); err != nil {
		return nil, fmt.Errorf("failed to read file contents: %w", err)
	}
	photoType := mimeToDayOneType(http.DetectContentType(b.Bytes()))
	return &DayOnePhoto{
		Type:       photoType,
		Identifier: NewIdentifier(),
		MD5:        fmt.Sprintf("%x", hash.Sum(nil)),
	}, nil
}

func NewPhotoFile(filename string) (*DayOnePhoto, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to open file '%s': %w", filename, err)
	}
	defer f.Close()
	return NewPhoto(f)
}

func mimeToDayOneType(mime string) string {
	switch mime {
	case "image/gif":
		return "gif"
	case "image/jpg", "image/jpeg":
		return "jpeg"
	case "image/png":
		return "png"
	case "image/webp":
		return "webp"
	default:
		// In rare cases, jpeg is recognized as octet-stream.
		return "jpeg"
	}
}
