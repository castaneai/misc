package bear2dayone

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"io"
	"path/filepath"
	"strings"

	"github.com/y-bash/go-gaga"
)

type BearBackup struct {
	Notes  map[string]*BearNote
	closer io.Closer
}

func (b *BearBackup) Close() error {
	return b.closer.Close()
}

type BearNote struct {
	Info   *BearNoteInfo
	Text   string
	Assets map[string]*zip.File
}

func (n *BearNote) Title() string {
	i := strings.Index(n.Text, "\n")
	t := n.Text
	if i > -1 {
		t = t[:i]
	}
	return strings.TrimPrefix(t, "# ")
}

type BearNoteInfo struct {
	Identifier   string `json:"uniqueIdentifier"`
	CreationDate string `json:"creationDate"`
}

func OpenBearBackup(filename string, norm *gaga.Normalizer) (*BearBackup, error) {
	r, err := zip.OpenReader(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to open file as zip '%s': %w", filename, err)
	}

	notes := map[string]*BearNote{}
	for _, f := range r.File {
		if tb, ok := extractBundlePath(f.Name, norm); ok {
			if _, ok := notes[tb.bundleName]; !ok {
				notes[tb.bundleName] = &BearNote{Assets: map[string]*zip.File{}}
			}
			// The actual plain text contents. (Whereas * is an arbitrary file extension)
			// https://textbundle.org/spec/
			if strings.HasPrefix(tb.relPath, "text.") {
				text, err := readText(f)
				if err != nil {
					return nil, fmt.Errorf("failed to read text: %w", err)
				}
				notes[tb.bundleName].Text = text
			} else if tb.relPath == "info.json" {
				info, err := readInfo(f)
				if err != nil {
					return nil, fmt.Errorf("failed to read info: %w", err)
				}
				notes[tb.bundleName].Info = info
			} else if strings.HasPrefix(tb.relPath, "assets/") {
				notes[tb.bundleName].Assets[tb.relPath] = f
			}
		}
	}
	return &BearBackup{
		closer: r,
		Notes:  notes,
	}, nil
}

type textBundlePath struct {
	bundleName string
	relPath    string
}

func extractBundlePath(path string, norm *gaga.Normalizer) (*textBundlePath, bool) {
	parts := strings.Split(path, "/")
	for i, p := range parts {
		if strings.HasSuffix(p, ".textbundle") {
			return &textBundlePath{
				bundleName: norm.String(strings.TrimSuffix(parts[i], ".textbundle")),
				relPath:    norm.String(filepath.Join(parts[i+1:]...)),
			}, true
		}
	}
	return nil, false
}

type tbInfo struct {
	BearNoteInfo BearNoteInfo `json:"net.shinyfrog.bear"`
}

func readInfo(f *zip.File) (*BearNoteInfo, error) {
	r, err := f.Open()
	if err != nil {
		return nil, fmt.Errorf("failed to open file in zip '%s': %w", f.Name, err)
	}
	defer r.Close()

	var tbi tbInfo
	if err := json.NewDecoder(r).Decode(&tbi); err != nil {
		return nil, fmt.Errorf("failed to decode file contents as TextBundle info.json: %w", err)
	}
	return &tbi.BearNoteInfo, nil
}

func readText(f *zip.File) (string, error) {
	r, err := f.Open()
	if err != nil {
		return "", fmt.Errorf("failed to open file in zip '%s': %w", f.Name, err)
	}
	defer r.Close()
	bytes, err := io.ReadAll(r)
	if err != nil {
		return "", fmt.Errorf("failed to read file contents: %w", err)
	}
	return string(bytes), nil
}
