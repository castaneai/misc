## Install

### Ubuntu

```
apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly
```

### macOS

```
brew install gst-plugins-good pkg-config gst-plugins-bad gst-plugins-ugly
export PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"
```

## Play your video

```
go run main.go -container-path=/home/sean/video.mp4
```
