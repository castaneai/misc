package main

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

var (
	privateKey *rsa.PrivateKey
)

func main() {
	numID := time.Now().Unix()

	f, err := os.ReadFile("private-key.pem")
	if err != nil {
		log.Fatalf("failed to load private-key.pem: %+v", err)
	}
	block, _ := pem.Decode(f)
	privateKey, _ = x509.ParsePKCS1PrivateKey(block.Bytes)

	followID := "test"
	myHost := "example.com"
	myName := "castaneai"
	toInbox := "https://fedibird.com/users/castaneai/inbox"
	res := map[string]interface{}{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       fmt.Sprintf("https://%s/users/%s/s/%d", myHost, myName, numID),
		"type":     "Follow",
		"actor":    fmt.Sprintf("https://%s/users/%s", myHost, myName),
		"object":   followID,
	}
	headers := signHeaders(res, myName, myHost, toInbox)
	postInbox(toInbox, res, headers)
}

func postInbox(req string, data, headers map[string]interface{}) {
	x, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}
	y, err := http.NewRequest("POST", req, bytes.NewBuffer(x))
	if err != nil {
		log.Fatal(err)
	}
	for k, v := range headers {
		y.Header.Set(k, v.(string))
	}
	resp, err := http.DefaultClient.Do(y)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("response: %+v", string(b))
}

func signHeaders(res map[string]interface{}, myName, myHost, toInbox string) map[string]interface{} {
	u, err := url.Parse(toInbox)
	if err != nil {
		log.Fatal(err)
	}
	gmt, _ := time.LoadLocation("GMT")
	_ = time.Now().In(gmt).Format("Mon, 02 Jan 2006 15:04:05")
	x, err := json.Marshal(res)
	if err != nil {
		log.Fatal(err)
	}
	sha := sha256.Sum256(x)
	s256 := base64.StdEncoding.EncodeToString(sha[:])
	hashed := sha256.Sum256(
		[]byte(
			"(request-target): post " + u.Path + "\n" +
				"host: " + u.Hostname() + "\n" +
				// "date: " + date + " GMT\n" +
				"digest: SHA-256=" + s256,
		),
	)
	sig, err := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, hashed[:])
	if err != nil {
		log.Fatal(err)
	}
	b64 := base64.StdEncoding.EncodeToString(sig[:])
	headers := map[string]interface{}{
		"Host": u.Hostname(),
		// "Date":   date,
		"Digest": "SHA-256=" + s256,
		"Signature": "keyId=\"https://" + myHost + "/users/" + myName + "\"," +
			"algorithm=\"rsa-sha256\"," +
			"headers=\"(request-target) host date digest\"," +
			"signature=\"" + b64 + "\"",
		"Accept":       "application/activity+json",
		"Content-Type": "application/activity+json",
		"User-Agent":   "test",
	}
	return headers
}
