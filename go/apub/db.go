package main

import (
	"context"
	"github.com/go-fed/activity/streams/vocab"
	"net/url"
)

type myDB struct{}

func (d *myDB) Lock(c context.Context, id *url.URL) error {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Unlock(c context.Context, id *url.URL) error {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) InboxContains(c context.Context, inbox, id *url.URL) (contains bool, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) GetInbox(c context.Context, inboxIRI *url.URL) (inbox vocab.ActivityStreamsOrderedCollectionPage, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) SetInbox(c context.Context, inbox vocab.ActivityStreamsOrderedCollectionPage) error {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Owns(c context.Context, id *url.URL) (owns bool, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) ActorForOutbox(c context.Context, outboxIRI *url.URL) (actorIRI *url.URL, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) ActorForInbox(c context.Context, inboxIRI *url.URL) (actorIRI *url.URL, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) OutboxForInbox(c context.Context, inboxIRI *url.URL) (outboxIRI *url.URL, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Exists(c context.Context, id *url.URL) (exists bool, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Get(c context.Context, id *url.URL) (value vocab.Type, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Create(c context.Context, asType vocab.Type) error {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Update(c context.Context, asType vocab.Type) error {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Delete(c context.Context, id *url.URL) error {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) GetOutbox(c context.Context, outboxIRI *url.URL) (inbox vocab.ActivityStreamsOrderedCollectionPage, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) SetOutbox(c context.Context, outbox vocab.ActivityStreamsOrderedCollectionPage) error {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) NewID(c context.Context, t vocab.Type) (id *url.URL, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Followers(c context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Following(c context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	//TODO implement me
	panic("implement me")
}

func (d *myDB) Liked(c context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	//TODO implement me
	panic("implement me")
}
