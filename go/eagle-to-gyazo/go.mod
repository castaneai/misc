module gitlab.com/castaneai/misc/go/eagle-to-gyazo

go 1.21.3

require gitlab.com/castaneai/gyan v0.0.0-20240202112023-94f46037de0f

require (
	github.com/golang/protobuf v1.2.0 // indirect
	golang.org/x/net v0.0.0-20190108225652-1e06a53dbb7e // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	google.golang.org/appengine v1.4.0 // indirect
)
