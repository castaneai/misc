package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"
	"strings"
	"time"

	"gitlab.com/castaneai/gyan"
)

func main() {
	token := os.Getenv("GYAZO_ACCESS_TOKEN")
	if token == "" {
		log.Fatalf("missing env: GYAZO_ACCESS_TOKEN")
	}

	if len(os.Args) < 2 {
		log.Fatalf("missing arg: <inputPath>")
	}
	metadataPath := os.Args[1]
	if metadataPath == "" {
		log.Fatalf("missing arg: <metadataPath>")
	}

	md, err := loadMetadata(metadataPath)
	if err != nil {
		log.Fatalf("failed to load metadata '%s': %+v", metadataPath, err)
	}
	log.Printf("load Eagle item: %+v", md)

	imageFilename := fmt.Sprintf("%s.%s", md.Name, md.Ext)
	imagePath := path.Join(path.Dir(metadataPath), imageFilename)
	f, err := os.Open(imagePath)
	if err != nil {
		log.Fatalf("failed to open image '%s': %+v", imagePath, err)
	}
	defer f.Close()

	req := &gyan.UploadRequest{
		Filename:    imageFilename,
		ImageData:   f,
		RefererURL:  md.URL,
		Application: "Eagle",
		Title:       md.Name,
		Desc:        createDescription(md),
		CreatedAt:   time.UnixMilli(md.CreatedAt),
	}
	c := gyan.NewClient(token)
	ctx := context.Background()
	resp, err := c.Upload(ctx, req)
	if err != nil {
		log.Fatalf("failed to upload to Gyazo: %+v", err)
	}
	fmt.Fprintln(os.Stdout, resp.PermalinkURL)
}

func createDescription(md *Metadata) string {
	tagParts := make([]string, 0, len(md.Tags))
	for _, tag := range md.Tags {
		tagParts = append(tagParts, fmt.Sprintf("#%s", tag))
	}
	return strings.Join(tagParts, " ")
}

type Metadata struct {
	Name      string   `json:"name"`
	Ext       string   `json:"ext"`
	CreatedAt int64    `json:"btime"` // unix milli
	URL       string   `json:"url"`
	Tags      []string `json:"tags"`
}

func loadMetadata(path string) (*Metadata, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open '%s': %w", path, err)
	}
	defer f.Close()

	var md Metadata
	if err := json.NewDecoder(f).Decode(&md); err != nil {
		return nil, fmt.Errorf("failed to decode metadata.json as JSON: %w", err)
	}
	return &md, nil
}
