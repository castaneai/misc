package main

import (
	"context"
	"log"
	"time"

	"github.com/redis/rueidis"
)

func main() {
	copt := rueidis.ClientOption{
		InitAddress:       []string{"redis:6379"},
		DisableCache:      true,
		ConnWriteTimeout:  5 * time.Second,
		PipelineMultiplex: -1,
	}
	rc, err := rueidis.NewClient(copt)
	if err != nil {
		log.Fatalf("failed to connect to redis: %+v", err)
	}

	ctx := context.Background()
	ticker := time.NewTicker(1000 * time.Millisecond)
	defer ticker.Stop()
	for range ticker.C {
		tick(ctx, rc)
	}
}

func tick(ctx context.Context, rc rueidis.Client) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()
	cmd := rc.B().Set().Key("test").Value("value").Build()
	resp := rc.Do(ctx, cmd)
	if err := resp.NonRedisError(); err != nil {
		log.Printf("%+v", err)
	}
}
