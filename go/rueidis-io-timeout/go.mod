module gitlab.com/castaneai/misc/go/rueidis-io-timeout

go 1.22.0

require (
	github.com/redis/rueidis v1.0.50-alpha.6 // indirect
	golang.org/x/sys v0.24.0 // indirect
)
