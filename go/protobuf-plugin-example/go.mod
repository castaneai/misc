module gitlab.com/castaneai/misc/go/protobuf-plugin-example

go 1.19

require (
	github.com/bufbuild/protocompile v0.2.0 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	google.golang.org/protobuf v1.28.2-0.20220831092852-f930b1dc76e8 // indirect
)
