package main

import (
	"context"
	"testing"

	"github.com/bufbuild/protocompile"
	"github.com/bufbuild/protocompile/linker"
	"google.golang.org/protobuf/types/pluginpb"
)

func TestInOut(t *testing.T) {
	compiler := &protocompile.Compiler{
		Resolver: &protocompile.SourceResolver{},
	}
	ctx := context.Background()

	req := &pluginpb.CodeGeneratorRequest{
		ProtoFile: nil,
	}

	files, err := compiler.Compile(ctx,
		"testdata/greet.proto", "testdata/testpkg/parent.proto")
	if err != nil {
		t.Fatal(err)
	}
	for _, file := range files {
		if res, ok := file.(linker.Result); ok {
			t.Logf("%s", res.FileDescriptorProto().GetName())
			req.ProtoFile = append(req.ProtoFile, res.FileDescriptorProto())
		}
	}

	t.Logf("%+v", req)
}

func pluginTestCase(f func(req *pluginpb.CodeGeneratorRequest)) {

}
