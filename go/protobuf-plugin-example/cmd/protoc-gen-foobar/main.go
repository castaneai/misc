package main

import (
	"io"
	"log"
	"os"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/pluginpb"
)

func main() {
	req, err := readInput(os.Stdin)
	if err != nil {
		log.Fatalf("failed to read input: %+v", err)
	}

	for _, pr := range req.ProtoFile {
		log.Printf("%s", *pr.Name)
	}

	resp := &pluginpb.CodeGeneratorResponse{
		File: []*pluginpb.CodeGeneratorResponse_File{
			{Name: strPtr("foo.txt"), Content: strPtr("content of foo.txt")},
		},
	}
	if err := writeOutput(os.Stdout, resp); err != nil {
		log.Fatalf("failed to write output: %+v", err)
	}
}

func strPtr(s string) *string {
	return &s
}

func readInput(r io.Reader) (*pluginpb.CodeGeneratorRequest, error) {
	input, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}
	var req pluginpb.CodeGeneratorRequest
	if err := proto.Unmarshal(input, &req); err != nil {
		return nil, err
	}
	return &req, nil
}

func writeOutput(w io.Writer, resp *pluginpb.CodeGeneratorResponse) error {
	output, err := proto.Marshal(resp)
	if err != nil {
		return err
	}
	if _, err := w.Write(output); err != nil {
		return err
	}
	return nil
}
