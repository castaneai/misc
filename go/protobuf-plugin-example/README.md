# Protobuf Plugin Example

```sh
go build ./cmd/protoc-gen-foobar
protoc --foobar_out=. --plugin=./protoc-gen-foobar ./testdata/greet.proto
```