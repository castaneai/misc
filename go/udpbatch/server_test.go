package udpbatch

import (
	"log"
	"net"
	"sync"
	"syscall"
	"testing"

	"golang.org/x/net/ipv4"
)

const (
	numClients    = 100
	readBatchSize = 100
)

func listenUDP() *net.UDPConn {
	uaddr, err := net.ResolveUDPAddr("udp", ":0")
	if err != nil {
		log.Fatalf("failed to resolve udp addr: %+v", err)
	}
	uconn, err := net.ListenUDP("udp", uaddr)
	if err != nil {
		log.Fatalf("failed to listen UDP: %+v", err)
	}
	return uconn
}

func BenchmarkUDPRead(b *testing.B) {
	uconn := listenUDP()
	go func() {
		buf := make([]byte, 1024)
		for {
			n, addr, err := uconn.ReadFromUDP(buf)
			if err != nil {
				log.Printf("failed to read from UDP: %+v", err)
				return
			}
			if _, err := uconn.WriteToUDP(buf[:n], addr); err != nil {
				log.Printf("failed to write to UDP: %+v", err)
				continue
			}
		}
	}()

	uconnBatch := listenUDP()
	go func() {
		ms := make([]ipv4.Message, readBatchSize)
		for i := 0; i < len(ms); i++ {
			ms[i].Buffers = [][]byte{make([]byte, 1024)}
		}
		pconn := ipv4.NewPacketConn(uconnBatch)
		for {
			n, err := pconn.ReadBatch(ms, syscall.MSG_WAITFORONE)
			if err != nil {
				log.Printf("failed to read batch: %+v", err)
				return
			}
			for i := 0; i < n; i++ {
				if _, err := uconnBatch.WriteToUDP(ms[i].Buffers[0][:ms[i].N], ms[i].Addr.(*net.UDPAddr)); err != nil {
					log.Printf("failed to write to UDP: %+v", err)
					continue
				}
			}
		}
	}()

	benchmarks := []struct {
		name       string
		serverAddr string
	}{
		{name: "Read", serverAddr: uconn.LocalAddr().String()},
		{name: "ReadBatch", serverAddr: uconnBatch.LocalAddr().String()},
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			buf := make([]byte, 1024)
			b.ResetTimer()
			for i := 0; i < b.N; i++ {
				var wg sync.WaitGroup
				for i := 0; i < numClients; i++ {
					wg.Add(1)
					go func(i int) {
						defer wg.Done()
						conn, err := net.Dial("udp", bm.serverAddr)
						if err != nil {
							log.Printf("failed to dial to %v", bm.serverAddr)
							return
						}
						if _, err := conn.Write([]byte("hellohello")); err != nil {
							log.Printf("failed to send: %+v", err)
							return
						}
						if _, err := conn.Read(buf); err != nil {
							log.Printf("failed to recv: %+v", err)
							return
						}
					}(i)
				}
				wg.Wait()
			}
		})
	}
}
