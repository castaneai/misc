package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	pb "agones.dev/agones/pkg/allocation/go"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

const (
	allocateStatusAllocated   = "allocated"
	allocateStatusUnAllocated = "unallocated"
	allocateStatusConflicted  = "conflicted"
	allocateStatusFailed      = "failed"
)

var (
	allocateCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "allocates",
	}, []string{"status"})
	allocateDuration = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "allocate_duration_seconds",
		Buckets: []float64{.1, .2, .4, .8, 1, 2, 4, 8, 16, 30},
	})
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	var rps float64
	flag.Float64Var(&rps, "rps", 1.0, "Allocate request per second")
	var allocatorAddr string
	flag.StringVar(&allocatorAddr, "addr", "localhost:50504", "Allocator address")
	flag.Parse()
	log.Printf("RPS: %.2f, Allocator: %s", rps, allocatorAddr)

	cc, err := grpc.DialContext(ctx, allocatorAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("failed to dial to allocator: %+v", err)
	}
	allocator := pb.NewAllocationServiceClient(cc)

	tick := time.Duration(1.0 / rps * float64(time.Second))
	ticker := time.NewTicker(tick)
	defer ticker.Stop()

	// start prometheus exporter
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		addr := ":2112"
		log.Printf("prometheus endpoint (/metrics) is listening on %s...", addr)
		if err := http.ListenAndServe(addr, nil); err != nil {
			log.Printf("failed to serve prometheus endpoint: %+v", err)
		}
	}()

	for {
		select {
		case <-ctx.Done():
			log.Printf("shutdown signal received")
			return
		case <-ticker.C:
			go doAllocate(ctx, allocator)
		}
	}
}

func doAllocate(ctx context.Context, allocator pb.AllocationServiceClient) {
	allocateStatus := allocateStatusFailed
	defer func() {
		allocateCount.With(prometheus.Labels{"status": allocateStatus}).Inc()
	}()

	start := time.Now()
	_, err := allocator.Allocate(ctx, &pb.AllocationRequest{
		Namespace: "default",
		GameServerSelectors: []*pb.GameServerSelector{
			{GameServerState: pb.GameServerSelector_ALLOCATED, MatchLabels: map[string]string{"agones.dev/sdk-available": "true"}},
			{GameServerState: pb.GameServerSelector_READY, MatchLabels: map[string]string{"agones.dev/sdk-available": "true"}},
		},
		Metadata: &pb.MetaPatch{
			Labels: map[string]string{
				"agones.dev/sdk-available": "false",
			},
		},
	})
	if err != nil {
		if st, ok := status.FromError(err); ok {
			if st.Code() == codes.ResourceExhausted {
				allocateStatus = allocateStatusUnAllocated
				return
			}
			if strings.Contains(st.Message(), "Operation cannot be fulfilled") {
				allocateStatus = allocateStatusConflicted
				return
			}
		}
		log.Printf("failed to allocate: %+v", err)
		return
	}
	allocateStatus = allocateStatusAllocated
	allocateDuration.Observe(time.Since(start).Seconds())
}
