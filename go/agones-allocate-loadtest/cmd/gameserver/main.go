package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	sdkpb "agones.dev/agones/pkg/sdk"
	sdk "agones.dev/agones/sdks/go"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	agones, err := sdk.NewSDK()
	if err != nil {
		log.Fatalf("failed to connect to Agones SDK: %+v", err)
	}
	if err := agones.Ready(); err != nil {
		log.Fatalf("failed to send Ready to Agones SDK: %+v", err)
	}
	if err := agones.SetLabel("available", "true"); err != nil {
		log.Fatalf("failed to set label available to true: %+v", err)
	}
	go func() {
		ticker := time.NewTicker(2 * time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				_ = agones.Health()
			}
		}
	}()

	allocatedCh := make(chan struct{})
	var lastAllocated string
	if err := agones.WatchGameServer(func(gs *sdkpb.GameServer) {
		v, ok := gs.ObjectMeta.Annotations["agones.dev/last-allocated"]
		if !ok {
			return
		}
		if v != lastAllocated {
			lastAllocated = v
			allocatedCh <- struct{}{}
		}
	}); err != nil {
		log.Fatalf("failed to watch gameserver: %+v", err)
	}

	allocatedCount := 0
	for {
		select {
		case <-ctx.Done():
			log.Printf("shutdown...")
			_ = agones.Shutdown()
			return
		case <-allocatedCh:
			allocatedCount++
			if err := agones.SetLabel("available", "true"); err != nil {
				log.Printf("failed to set available label to true: %+v", err)
			}
		}
	}
}
