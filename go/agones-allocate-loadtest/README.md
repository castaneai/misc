# agones-allocate-loadtest

```sh
# Setup tools
aqua i
helmfile sync

# Local (rancher-desktop, orbstack, ...)
skaffold dev

# GKE
export SKAFFOLD_DEFAULT_REPO=xxx-docker.pkg.dev/...
skaffold dev

# Open Grafana user: `admin`, password: `test`
open http://127.0.0.1:8080
```
