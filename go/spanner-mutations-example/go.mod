module github.com/castaneai/spanner-mutations-example

go 1.13

require (
	cloud.google.com/go v0.44.3
	github.com/castaneai/spankeys v0.0.0-20190927061946-5be4c604a277
	github.com/google/uuid v1.1.1
	github.com/stretchr/testify v1.4.0
)
