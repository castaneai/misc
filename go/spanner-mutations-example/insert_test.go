package spanner_mutations_example

import (
	"context"
	"testing"

	"cloud.google.com/go/spanner"
	"github.com/castaneai/spankeys/testutils"
)

func TestInsertWithoutIndex(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
) PRIMARY KEY (ID)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	{
		var muts []*spanner.Mutation
		for i := 0; i < 10000; i++ {
			muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name"}, []interface{}{randomString(), randomString()}))
		}
		// 2 columns * 10000 rows = 20000 mutations
		assertSuccessfulWrite(ctx, t, c, muts)
	}

	{
		var muts []*spanner.Mutation
		for i := 0; i < 10001; i++ {
			muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name"}, []interface{}{randomString(), randomString()}))
		}
		// 2 columns * 10001 rows = 20002 mutations
		assertTooManyMutations(ctx, t, c, muts)
	}
}

func TestInsertWithIndex(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
	Age INT64 NOT NULL,
) PRIMARY KEY (ID)`,
		`CREATE INDEX SinglePK_Name ON SinglePK(Name)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	{
		var muts []*spanner.Mutation
		for i := 0; i < 5000; i++ {
			muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{randomString(), randomString(), 12345}))
		}
		// (3 columns + 1 index) * 5000 rows = 20000 mutations
		assertSuccessfulWrite(ctx, t, c, muts)
	}

	{
		var muts []*spanner.Mutation
		for i := 0; i < 5001; i++ {
			muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{randomString(), randomString(), 12345}))
		}
		// (3 columns + 1 index) * 5001 = 20004  mutations
		assertTooManyMutations(ctx, t, c, muts)
	}
}

func TestInsertWithCompositeIndex(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
	Age INT64 NOT NULL,
) PRIMARY KEY (ID)`,
		`CREATE INDEX SinglePK_Name_Age ON SinglePK(Name, Age)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	{
		var muts []*spanner.Mutation
		for i := 0; i < 5000; i++ {
			muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{randomString(), randomString(), 12345}))
		}
		// (3 columns + 1 index) * 5000 rows = 20000 mutations
		// It is one mutation even if it is composite index.
		assertSuccessfulWrite(ctx, t, c, muts)
	}
}

func TestInsertWithNullDefaultValue(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
	Age INT64,
) PRIMARY KEY (ID)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	{
		var muts []*spanner.Mutation
		for i := 0; i < 6667; i++ {
			muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{randomString(), randomString(), nil}))
		}
		// 3 columns * 6667 rows = 20002 mutations
		assertTooManyMutations(ctx, t, c, muts)
	}
	{
		var muts []*spanner.Mutation
		for i := 0; i < 10000; i++ {
			muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name"}, []interface{}{randomString(), randomString()}))
		}
		// 2 columns * 10000 rows = 20000 mutations
		assertSuccessfulWrite(ctx, t, c, muts)
	}
}
