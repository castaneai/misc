package spanner_mutations_example

import (
	"context"
	"fmt"
	"testing"

	"cloud.google.com/go/spanner"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func randomString() string {
	return uuid.Must(uuid.NewRandom()).String()
}

func assertSuccessfulWrite(ctx context.Context, t *testing.T, c *spanner.Client, muts []*spanner.Mutation) {
	_, err := c.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		return tx.BufferWrite(muts)
	})
	assert.Nil(t, err)
}

func assertSuccessfulDML(ctx context.Context, t *testing.T, c *spanner.Client, stmts []spanner.Statement) {
	_, err := c.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		for _, stmt := range stmts {
			if _, err := tx.Update(ctx, stmt); err != nil {
				return err
			}
		}
		return nil
	})
	assert.Nil(t, err)
}

func assertTooManyMutations(ctx context.Context, t *testing.T, c *spanner.Client, muts []*spanner.Mutation) {
	_, err := c.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		return tx.BufferWrite(muts)
	})
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "too many mutations")
}

func assertTooManyMutationsDML(ctx context.Context, t *testing.T, c *spanner.Client, stmts []spanner.Statement) {
	_, err := c.ReadWriteTransaction(ctx, func(ctx context.Context, tx *spanner.ReadWriteTransaction) error {
		for _, stmt := range stmts {
			if _, err := tx.Update(ctx, stmt); err != nil {
				return err
			}
		}
		return nil
	})
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "too many mutations")
}

type KeyOrder string

const (
	KeyOrderAsc  KeyOrder = "ASC"
	KeyOrderDesc KeyOrder = "DESC"
)

func readKeys(ctx context.Context, c *spanner.Client, keyName string, order KeyOrder, limit int) ([]spanner.Key, error) {
	var keys []spanner.Key
	stmt := spanner.NewStatement(fmt.Sprintf("SELECT `%s` FROM SinglePK ORDER BY `%s` %s LIMIT %d", keyName, keyName, order, limit))
	if err := c.Single().Query(ctx, stmt).Do(func(r *spanner.Row) error {
		var k spanner.Key
		if err := r.ColumnByName(keyName, &k); err != nil {
			return err
		}
		keys = append(keys, k)
		return nil
	}); err != nil {
		return nil, err
	}
	return keys, nil
}
