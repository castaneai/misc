package spanner_mutations_example

import (
	"context"
	"testing"

	"cloud.google.com/go/spanner"
	"github.com/castaneai/spankeys/testutils"
	"github.com/stretchr/testify/assert"
)

func TestDelete(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
) PRIMARY KEY (ID)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var ids []string
	{
		for k := 0; k < 3; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 10000; i++ {
				id := randomString()
				ids = append(ids, id)
				muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name"}, []interface{}{id, randomString()}))
			}
			// 2 * 10000 = 20000 mutations (ok!)
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 30000, len(ids))

	{
		var muts []*spanner.Mutation
		for i := 0; i < 20001; i++ {
			muts = append(muts, spanner.Delete("SinglePK", spanner.Key{ids[i]}))
		}
		// 1*20001 = 20001 mutations (too many!)
		assertTooManyMutations(ctx, t, c, muts)
	}
	{
		var muts []*spanner.Mutation
		for i := 0; i < 20000; i++ {
			muts = append(muts, spanner.Delete("SinglePK", spanner.Key{ids[i]}))
		}
		// 1*20000 = 20000 mutations (ok!)
		assertSuccessfulWrite(ctx, t, c, muts)
	}
}

func TestDeleteKeyRange(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
) PRIMARY KEY (ID)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var ids []string
	{
		for k := 0; k < 5; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 10000; i++ {
				id := randomString()
				ids = append(ids, id)
				muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name"}, []interface{}{id, randomString()}))
			}
			// 2 * 10000 = 20000 mutations (ok!)
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 50000, len(ids))

	{
		sortedKeys, err := readKeys(ctx, c, "ID", KeyOrderAsc, 40002)
		if err != nil {
			t.Fatal(err)
		}
		var muts []*spanner.Mutation
		for i := 0; i < 40002; i += 2 {
			kr := spanner.KeyRange{sortedKeys[i], sortedKeys[i+1], spanner.ClosedClosed}
			muts = append(muts, spanner.Delete("SinglePK", kr))
		}
		// 1*20001 = 20001 mutations (too many!)
		assertTooManyMutations(ctx, t, c, muts)
	}

	{
		sortedKeys, err := readKeys(ctx, c, "ID", KeyOrderAsc, 40000)
		if err != nil {
			t.Fatal(err)
		}
		var muts []*spanner.Mutation
		for i := 0; i < 40000; i += 2 {
			kr := spanner.KeyRange{sortedKeys[i], sortedKeys[i+1], spanner.ClosedClosed}
			muts = append(muts, spanner.Delete("SinglePK", kr))
		}
		// 1*20000 = 20000 mutations (ok!)
		assertSuccessfulWrite(ctx, t, c, muts)
	}
}
