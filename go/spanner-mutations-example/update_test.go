package spanner_mutations_example

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"cloud.google.com/go/spanner"
	"github.com/castaneai/spankeys/testutils"
)

func TestUpdate(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
	Age INT64 NOT NULL
) PRIMARY KEY (ID)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var ids []string
	{
		for k := 0; k < 4; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 5000; i++ {
				id := randomString()
				ids = append(ids, id)
				muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{id, randomString(), 12345}))
			}
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 20000, len(ids))

	{
		var muts []*spanner.Mutation
		for i := 0; i < 10001; i++ {
			muts = append(muts, spanner.Update("SinglePK", []string{"ID", "Name"}, []interface{}{ids[i], randomString()}))
		}
		// 2 columns * 10001 rows = 20002 mutations
		assertTooManyMutations(ctx, t, c, muts)
	}
	{
		var muts []*spanner.Mutation
		for i := 0; i < 10000; i++ {
			muts = append(muts, spanner.Update("SinglePK", []string{"ID", "Name"}, []interface{}{ids[i], randomString()}))
		}
		// 2 columns * 10000 rows = 20000 mutations
		assertSuccessfulWrite(ctx, t, c, muts)
	}

}

func TestUpdateByDML(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
	Age INT64 NOT NULL
) PRIMARY KEY (ID)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var ids []string
	{
		for k := 0; k < 4; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 5000; i++ {
				id := randomString()
				ids = append(ids, id)
				muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{id, randomString(), 12345}))
			}
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 20000, len(ids))

	{
		var ss []spanner.Statement
		for i := 0; i < 10001; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["id"] = ids[i]
			s.Params["name"] = randomString()
			ss = append(ss, s)
		}
		// 2 columns * 10001 rows = 20001 mutations
		assertTooManyMutationsDML(ctx, t, c, ss)
	}
	{
		var ss []spanner.Statement
		for i := 0; i < 10000; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["id"] = ids[i]
			s.Params["name"] = randomString()
			ss = append(ss, s)
		}
		// 2 columns * 10000 rows = 20000 mutations
		assertSuccessfulDML(ctx, t, c, ss)
	}

	// Each mutation is counted individually even if all update-mutations to the same row
	{
		var ss []spanner.Statement
		for i := 0; i < 10001; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["id"] = ids[0]
			s.Params["name"] = randomString()
			ss = append(ss, s)
		}
		// 2 columns * 10001 rows = 20002 mutations
		assertTooManyMutationsDML(ctx, t, c, ss)
	}
	{
		var ss []spanner.Statement
		for i := 0; i < 10000; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["id"] = ids[0]
			s.Params["name"] = randomString()
			ss = append(ss, s)
		}
		// 2 columns * 10000 rows = 20000 mutations
		assertSuccessfulDML(ctx, t, c, ss)
	}
}

func TestUpdateWithPKIndex(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
	Age INT64 NOT NULL
) PRIMARY KEY (ID)`,
		`CREATE INDEX SinglePK_ID ON SinglePK(ID)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var ids []string
	{
		for k := 0; k < 4; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 5000; i++ {
				id := randomString()
				ids = append(ids, id)
				muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{id, randomString(), 12345}))
			}
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 20000, len(ids))

	// The index for not modified column is not counted for mutations
	{
		var muts []*spanner.Mutation
		for i := 0; i < 10000; i++ {
			muts = append(muts, spanner.Update("SinglePK", []string{"ID", "Name"}, []interface{}{ids[i], randomString()}))
		}
		// 2 columns * 10000 rows = 20000 mutations
		assertSuccessfulWrite(ctx, t, c, muts)
	}

	// The index for column in WHERE clause is not counted for mutations
	{
		var ss []spanner.Statement
		for i := 0; i < 10000; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["id"] = ids[i]
			s.Params["name"] = randomString()
			ss = append(ss, s)
		}
		// 2 column * 10000 rows = 20000 mutations
		assertSuccessfulDML(ctx, t, c, ss)
	}
}

func TestUpdateWithIndex(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
			ID STRING(36) NOT NULL,
			Name STRING(255) NOT NULL,
			Age INT64 NOT NULL,
		) PRIMARY KEY (ID)`,
		`CREATE INDEX SinglePK_Name ON SinglePK(Name)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var ids []string
	{
		for k := 0; k < 4; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 5000; i++ {
				id := randomString()
				ids = append(ids, id)
				muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{id, randomString(), 12345}))
			}
			// (3 columns + 1 index) * 5000 rows = 20000 mutations
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 20000, len(ids))

	{
		var ss []spanner.Statement
		for i := 0; i < 6667; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["name"] = randomString()
			s.Params["id"] = ids[i]
			ss = append(ss, s)
		}
		// (1 columns + 1 index + 1 ID where) * 6667 rows = 20001 mutations
		assertTooManyMutationsDML(ctx, t, c, ss)
	}
	{
		var ss []spanner.Statement
		for i := 0; i < 5001; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["name"] = randomString()
			s.Params["id"] = ids[i]
			ss = append(ss, s)
		}
		// (1 columns + 1 index + 1 ID where + 1?) * 5001 = 20001 mutations
		assertTooManyMutationsDML(ctx, t, c, ss)
	}
	{
		var ss []spanner.Statement
		for i := 0; i < 5000; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["name"] = randomString()
			s.Params["id"] = ids[i]
			ss = append(ss, s)
		}
		// (1 columns + 1 index + 1 ID where + 1?) * 5000 = 20000 mutations
		assertSuccessfulDML(ctx, t, c, ss)
	}
}

func TestUpdateWithDuplicateIndex(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE SinglePK (
    ID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
	Age INT64 NOT NULL,
) PRIMARY KEY (ID)`,
		`CREATE INDEX SinglePK_Name ON SinglePK(Name)`,
		`CREATE INDEX SinglePK_Name_2 ON SinglePK(Name)`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var ids []string
	{
		for k := 0; k < 5; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 4000; i++ {
				id := randomString()
				ids = append(ids, id)
				muts = append(muts, spanner.Insert("SinglePK", []string{"ID", "Name", "Age"}, []interface{}{id, randomString(), 12345}))
			}
			// (3 columns + 2 indexes) * 4000 = 20000 mutations
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 20000, len(ids))

	{
		var ss []spanner.Statement
		for i := 0; i < 5001; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["name"] = randomString()
			s.Params["id"] = ids[i]
			ss = append(ss, s)
		}
		// (1 column + 2 index + 1 ID where) * 5001 = 20004 mutations
		assertTooManyMutationsDML(ctx, t, c, ss)
	}
	{
		var ss []spanner.Statement
		for i := 0; i < 5000; i++ {
			s := spanner.NewStatement("UPDATE SinglePK SET Name = @name WHERE ID = @id")
			s.Params["name"] = randomString()
			s.Params["id"] = ids[i]
			ss = append(ss, s)
		}
		// (1 column + 2 index + 1 ID where) * 5000 = 20000 mutations
		assertSuccessfulDML(ctx, t, c, ss)
	}
}

func TestUpdateWithInterleave(t *testing.T) {
	ctx := context.Background()

	if err := testutils.PrepareDatabase(ctx, []string{
		`CREATE TABLE Parent (
    ParentID STRING(36) NOT NULL,
) PRIMARY KEY (ParentID)`,
		`CREATE TABLE Child (
    ParentID STRING(36) NOT NULL,
    ChildID STRING(36) NOT NULL,
    Name STRING(255) NOT NULL,
) PRIMARY KEY (ParentID, ChildID), INTERLEAVE IN PARENT Parent ON DELETE CASCADE`,
	}); err != nil {
		t.Fatal(err)
	}

	c, err := testutils.NewSpannerClient(ctx)
	if err != nil {
		t.Fatal(err)
	}

	var pids []string
	var cids []string
	{
		var muts []*spanner.Mutation
		for i := 0; i < 20000; i++ {
			pid := randomString()
			pids = append(pids, pid)
			muts = append(muts, spanner.Insert("Parent", []string{"ParentID"}, []interface{}{pid}))
		}
		// 1 * 20000 = 20000 mutations (ok!)
		assertSuccessfulWrite(ctx, t, c, muts)
	}
	assert.Equal(t, 20000, len(pids))

	{
		for k := 0; k < 4; k++ {
			var muts []*spanner.Mutation
			for i := 0; i < 5000; i++ {
				cid := randomString()
				cids = append(cids, cid)
				muts = append(muts, spanner.Insert("Child", []string{"ParentID", "ChildID", "Name"}, []interface{}{pids[i+5000*k], cid, randomString()}))
			}
			// 3 * 5000 = 15000 mutations (ok!)
			assertSuccessfulWrite(ctx, t, c, muts)
		}
	}
	assert.Equal(t, 20000, len(cids))

	{
		var muts []*spanner.Mutation
		for i := 0; i < 6666; i++ {
			muts = append(muts, spanner.Update("Child", []string{"ParentID", "ChildID", "Name"}, []interface{}{pids[i], cids[i], randomString()}))
		}
		// 3 * 6666 = 19998 mutations (ok!)
		assertSuccessfulWrite(ctx, t, c, muts)
	}

	{
		var muts []*spanner.Mutation
		for i := 0; i < 6667; i++ {
			muts = append(muts, spanner.Update("Child", []string{"ParentID", "ChildID", "Name"}, []interface{}{pids[i], cids[i], randomString()}))
		}
		// 3 * 6667 = 20001 mutations (too many!)
		assertTooManyMutations(ctx, t, c, muts)
	}
}
