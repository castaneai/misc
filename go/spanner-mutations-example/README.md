# spanner-mutations-example

## Testing

Set following environment variables.

- `SPANNER_PROJECT_ID`
- `SPANNER_INSTANCE_ID`
- `SPANNER_DATABASE_ID`

```
go test ./...
```

