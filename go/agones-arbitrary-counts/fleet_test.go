package agones_arbitrary_counts

import (
	"testing"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestFleet(t *testing.T) {
	ns := newRandomNamespace(t)
	image := getLatestGameServerImage(t)

	gsSpec := agonesv1.GameServerSpec{
		Ports: []agonesv1.GameServerPort{{Name: "default", Protocol: "TCP", ContainerPort: 7654}},
		Template: corev1.PodTemplateSpec{
			Spec: corev1.PodSpec{
				Containers: []corev1.Container{
					{Name: "gameserver", Image: image},
				},
			},
		},
		Counters: map[string]agonesv1.CounterStatus{
			counterTotalSessions:   {Count: 0, Capacity: 10},
			counterCurrentSessions: {Count: 0, Capacity: 3},
		},
		Lists: map[string]agonesv1.ListStatus{
			"test_list": {Capacity: 3, Values: []string{"a", "b", "c", "d"}},
		},
	}

	flt := newFleet(t, "fleet-1", ns, 3, &agonesv1.GameServerTemplateSpec{
		ObjectMeta: v1.ObjectMeta{Name: "fleet-gs"},
		Spec:       gsSpec,
	})
	t.Logf("fleet (%s) counters:", flt.Name)
	for key, counter := range flt.Status.Counters {
		t.Logf("  %s: %d/%d", key, counter.Count, counter.Capacity)
	}
	for key, list := range flt.Status.Lists {
		t.Logf("  %s: %d/%d", key, list.Count, list.Capacity)
	}
}
