package agones_arbitrary_counts

import (
	"context"
	"fmt"
	"os"
	"testing"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	allocationv1 "agones.dev/agones/pkg/apis/allocation/v1"
	sdk "agones.dev/agones/sdks/go"
	"github.com/stretchr/testify/require"
	corev1 "k8s.io/api/core/v1"
)

const (
	counterCurrentSessions = "current_sessions"
	counterTotalSessions   = "total_sessions"
)

func TestAllocateByCounts(t *testing.T) {
	ns := newRandomNamespace(t)
	image := getLatestGameServerImage(t)

	totalSessions := int64(10)
	maxConcurrentSessions := int64(3)

	newGameServer(t, "gs1", ns, agonesv1.GameServerSpec{
		Ports: []agonesv1.GameServerPort{{Name: "default", Protocol: "TCP", ContainerPort: 7654}},
		Template: corev1.PodTemplateSpec{
			Spec: corev1.PodSpec{
				Containers: []corev1.Container{
					{Name: "gameserver", Image: image},
				},
			},
		},
		Counters: map[string]agonesv1.CounterStatus{
			counterTotalSessions:   {Count: 0, Capacity: totalSessions},
			counterCurrentSessions: {Count: 0, Capacity: maxConcurrentSessions},
		},
	})

	allocator := newTestAllocator(ns)

	// within capacity
	var sdks []*sdk.SDK
	for i := 0; i < int(maxConcurrentSessions); i++ {
		resp := allocate(t, allocator)
		sdk := connectSDK(t, resp)
		sdks = append(sdks, sdk)
	}

	totalLeft := totalSessions - maxConcurrentSessions

	for i := totalLeft; i > 0; i-- {
		// over capacity!
		resp := allocate(t, allocator)
		require.Equal(t, allocationv1.GameServerAllocationUnAllocated, resp.Status.State)

		// finish single session
		finishGameSession(t, sdks[0])

		// allocatable!
		resp = allocate(t, allocator)
		require.Equal(t, allocationv1.GameServerAllocationAllocated, resp.Status.State)
	}

	// finish single session
	finishGameSession(t, sdks[0])

	// not allocatable because total sessions exceeded
	resp := allocate(t, allocator)
	require.Equal(t, allocationv1.GameServerAllocationUnAllocated, resp.Status.State)
}

func allocate(t *testing.T, allocator *Allocator) *allocationv1.GameServerAllocation {
	resp, err := allocator.Allocate(context.Background(), allocationv1.GameServerAllocationSpec{
		Selectors: []allocationv1.GameServerSelector{
			{
				GameServerState: ref(agonesv1.GameServerStateAllocated),
				Counters: map[string]allocationv1.CounterSelector{
					counterCurrentSessions: {MinAvailable: 1},
					counterTotalSessions:   {MinAvailable: 1},
				},
			},
			{
				GameServerState: ref(agonesv1.GameServerStateReady),
				Counters: map[string]allocationv1.CounterSelector{
					counterCurrentSessions: {MinAvailable: 1},
					counterTotalSessions:   {MinAvailable: 1},
				},
			},
		},
		Counters: map[string]allocationv1.CounterAction{
			counterCurrentSessions: {Action: ref("Increment"), Amount: ref(int64(1))},
			counterTotalSessions:   {Action: ref("Increment"), Amount: ref(int64(1))},
		},
	})
	require.NoError(t, err)
	switch resp.Status.State {
	case allocationv1.GameServerAllocationAllocated:
		sdk := connectSDK(t, resp)
		t.Logf("allocated: %s %s", resp.Status.GameServerName, getGameServerCapacityInfo(t, sdk))
	default:
		t.Logf("unallocated(state: %s)", resp.Status.State)
	}
	return resp
}

func connectSDK(t *testing.T, resp *allocationv1.GameServerAllocation) *sdk.SDK {
	os.Setenv("AGONES_SDK_GRPC_PORT", fmt.Sprintf("%d", resp.Status.Ports[0].Port))
	defer os.Unsetenv("AGONES_SDK_GRPC_PORT")
	agones, err := sdk.NewSDK()
	require.NoError(t, err)
	return agones
}

func getGameServerCapacityInfo(t *testing.T, sdk *sdk.SDK) string {
	current, max := getCapacity(t, sdk, counterCurrentSessions)
	totalCurrent, totalMax := getCapacity(t, sdk, counterTotalSessions)
	return fmt.Sprintf("sessions: (%d/%d), total: (%d/%d)",
		current, max, totalCurrent, totalMax)
}

func getCapacity(t *testing.T, sdk *sdk.SDK, counterKey string) (current, max int64) {
	current, err := sdk.Alpha().GetCounterCount(counterKey)
	require.NoError(t, err)
	max, err = sdk.Alpha().GetCounterCapacity(counterKey)
	require.NoError(t, err)
	return current, max
}

func finishGameSession(t *testing.T, sdk *sdk.SDK) {
	_, err := sdk.Alpha().DecrementCounter(counterCurrentSessions, 1)
	require.NoError(t, err)
	gs, err := sdk.GameServer()
	require.NoError(t, err)
	t.Logf("finished session on %s %s", gs.ObjectMeta.Name, getGameServerCapacityInfo(t, sdk))
}

func ref[T any](s T) *T {
	return &s
}
