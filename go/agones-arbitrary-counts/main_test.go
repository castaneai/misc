package agones_arbitrary_counts

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	e2eframework "agones.dev/agones/test/e2e/framework"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	namespacePrefix = "agones-test-"
)

var framework *e2eframework.Framework

func TestMain(m *testing.M) {
	fw, err := e2eframework.NewFromFlags()
	if err != nil {
		log.Fatalf("failed to init e2e e2eframework: %+v", err)
	}
	framework = fw

	var exitCode int
	defer func() {
		os.Exit(exitCode)
	}()
	exitCode = m.Run()
}

func getLatestGameServerImage(t *testing.T) string {
	ctx := context.Background()
	res, err := framework.AgonesClient.AgonesV1().GameServers("default").List(ctx, metav1.ListOptions{Limit: 1})
	if err != nil {
		t.Fatalf("failed to list gameservers in default ns: %+v", err)
	}
	require.Len(t, res.Items, 1)
	for _, ct := range res.Items[0].Spec.Template.Spec.Containers {
		if ct.Name != "agones-gameserver-sidecar" {
			return ct.Image
		}
	}
	t.Fatalf("gameserver container not found: %+v", res.Items[0].Spec.Template.Spec.Containers)
	return ""
}

func newFleetResource(name, namespace string, replicas int, gstSpec *agonesv1.GameServerTemplateSpec) *agonesv1.Fleet {
	return &agonesv1.Fleet{
		ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace},
		Spec: agonesv1.FleetSpec{
			Replicas: int32(replicas),
			Template: *gstSpec,
		},
	}
}

func newGameServer(t *testing.T, name, namespace string, spec agonesv1.GameServerSpec) *agonesv1.GameServer {
	gs := agonesv1.GameServer{
		ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace},
		Spec:       spec,
	}
	created, err := framework.CreateGameServerAndWaitUntilReady(t, namespace, &gs)
	if err != nil {
		t.Fatalf("failed to create GameServer: %+v", err)
	}
	return created
}

func newFleet(t *testing.T, name, namespace string, replicas int, gstSpec *agonesv1.GameServerTemplateSpec) *agonesv1.Fleet {
	flt, err := framework.AgonesClient.AgonesV1().
		Fleets(namespace).
		Create(context.Background(), newFleetResource(name, namespace, replicas, gstSpec), metav1.CreateOptions{})
	if err != nil {
		t.Fatalf("failed to create fleet: %+v", err)
	}
	if err := framework.WaitForFleetCondition(t, flt, func(_ *logrus.Entry, f *agonesv1.Fleet) bool {
		ok := (f.Status.ReadyReplicas + f.Status.AllocatedReplicas + f.Status.ReservedReplicas) >= int32(replicas)
		if ok {
			flt = f
		}
		return ok
	}); err != nil {
		t.Fatalf("failed to wait for fleet stable: %+v", err)
	}
	return flt
}

func newGameServerTemplateSpec(name, image string) *agonesv1.GameServerTemplateSpec {
	return &agonesv1.GameServerTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: agonesv1.GameServerSpec{
			Ports: []agonesv1.GameServerPort{
				{
					ContainerPort: 7000,
					Name:          "tcp",
					PortPolicy:    agonesv1.Dynamic,
					Protocol:      corev1.ProtocolTCP,
				}},
			Template: corev1.PodTemplateSpec{
				Spec: newPodSpec(name, image),
			},
		},
	}
}

func newPodSpec(name, image string) corev1.PodSpec {
	return corev1.PodSpec{
		Containers: []corev1.Container{{
			Name:            name,
			Image:           image,
			ImagePullPolicy: corev1.PullIfNotPresent, // prefer local image
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("30m"),
					corev1.ResourceMemory: resource.MustParse("32Mi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("30m"),
					corev1.ResourceMemory: resource.MustParse("32Mi"),
				},
			},
		}},
	}
}

func newRandomNamespace(t *testing.T) string {
	namespace := fmt.Sprintf("%s%s", namespacePrefix, uuid.Must(uuid.NewRandom()))
	if err := framework.CreateNamespace(namespace); err != nil {
		panic(err)
	}
	t.Cleanup(func() {
		_ = framework.DeleteNamespace(namespace)
	})
	return namespace
}

func newTestAllocator(namespace string) *Allocator {
	return &Allocator{c: framework.AgonesClient.AllocationV1().GameServerAllocations(namespace)}
}
