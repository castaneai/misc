package agones_arbitrary_counts

import (
	"context"

	v1 "agones.dev/agones/pkg/apis/allocation/v1"
	allocation "agones.dev/agones/pkg/client/clientset/versioned/typed/allocation/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Allocator struct {
	c allocation.GameServerAllocationInterface
}

func NewAllocator(c allocation.GameServerAllocationInterface) *Allocator {
	return &Allocator{c: c}
}

func (a *Allocator) Allocate(ctx context.Context, spec v1.GameServerAllocationSpec) (*v1.GameServerAllocation, error) {
	gsa := &v1.GameServerAllocation{
		Spec: spec,
	}
	alloc, err := a.c.Create(ctx, gsa, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}
	return alloc, nil
}
