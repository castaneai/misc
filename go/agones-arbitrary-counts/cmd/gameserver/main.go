package main

import (
	"context"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	sdk "agones.dev/agones/sdks/go"
	"golang.org/x/sync/errgroup"
)

func main() {
	agones, err := sdk.NewSDK()
	if err != nil {
		log.Fatalf("failed to new Agones SDK: %+v", err)
	}
	if err := agones.Ready(); err != nil {
		log.Fatalf("failed to send Ready to Agones SDK: %+v", err)
	}
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	defer cancel()

	go startHealth(ctx, agones)

	listener, err := net.Listen("tcp", ":7654")
	if err != nil {
		log.Fatalf("failed to listen: %+v", err)
	}
	for {
		acceptedConn, err := listener.Accept()
		if err != nil {
			log.Fatalf("failed to accept: %+v", err)
		}
		go startSDKProxy(ctx, acceptedConn)
	}
}

func startHealth(ctx context.Context, agones *sdk.SDK) {
	ticker := time.NewTicker(3 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			log.Printf("shutdown requested: %+v", ctx.Err())
			return
		case <-ticker.C:
			if err := agones.Health(); err != nil {
				log.Printf("failed to send Health to Agones SDK: %+v", err)
			}
		}
	}
}

func startSDKProxy(ctx context.Context, acceptedConn net.Conn) {
	defer acceptedConn.Close()

	conn, err := net.Dial("tcp", "localhost:9357")
	if err != nil {
		log.Printf("failed to dial to Agones SDK: %+v", err)
		return
	}
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		_, err := io.Copy(conn, acceptedConn)
		return err
	})
	eg.Go(func() error {
		_, err := io.Copy(acceptedConn, conn)
		return err
	})
	if err := eg.Wait(); err != nil {
		log.Printf("failed to proxy: %+v", err)
	}
}
