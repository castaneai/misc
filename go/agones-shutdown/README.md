# Agones GameServer Shutdown example

## Requirements

- [aqua](https://aquaproj.github.io/)

## Usage

```shell
# setup
aqua i
helmfile sync

# run
skaffold dev

ADDR=$(kubectl get gs -o json | jq -r '.items[0].status | "\(.address):\(.ports[0].port)"')
# trigger os.Exit(1)
curl http://$ADDR/exit1
```
