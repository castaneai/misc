package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	sdk "agones.dev/agones/sdks/go"
	"golang.org/x/sync/errgroup"
)

func main() {
	slog.Info("starting gameserver...")
	agonesSDK, err := sdk.NewSDK()
	if err != nil {
		err := fmt.Errorf("failed to initialize Agones SDK: %w", err)
		slog.Error(err.Error(), "error", err)
		return
	}
	if err := agonesSDK.Ready(); err != nil {
		err := fmt.Errorf("failed to ready Agones SDK: %w", err)
		slog.Error(err.Error(), "error", err)
		return
	}
	slog.Info("gameserver is ready")
	go startHealth(agonesSDK)

	ctx, shutdown := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer shutdown()

	mux := http.NewServeMux()
	addr := ":7654"
	sv := &http.Server{
		Addr:    addr,
		Handler: mux,
	}
	mux.HandleFunc("/shutdown", func(w http.ResponseWriter, r *http.Request) {
		slog.Info("GET /shutdown")
		shutdown()
	})
	mux.HandleFunc("/shutdown-sdk", func(w http.ResponseWriter, r *http.Request) {
		slog.Info("GET /shutdown-sdk")
		if err := agonesSDK.Shutdown(); err != nil {
			err := fmt.Errorf("failed to shutdown Agones SDK: %w", err)
			slog.Error(err.Error(), "error", err)
		}
	})
	mux.HandleFunc("/exit1", func(w http.ResponseWriter, r *http.Request) {
		slog.Info("GET /exit1")
		os.Exit(1)
	})

	var eg errgroup.Group
	eg.Go(func() error {
		slog.Info(fmt.Sprintf("HTTP listening at %s...", addr))
		return sv.ListenAndServe()
	})
	eg.Go(func() error {
		ticker := time.NewTicker(2 * time.Second)
		defer ticker.Stop()
		for range ticker.C {
			slog.Info("game loop...")
		}
		return nil
	})

	<-ctx.Done()
	slog.Info("signal received, shutting down...")
	if err := sv.Shutdown(context.Background()); err != nil {
		err := fmt.Errorf("failed to shutdown HTTP server: %w", err)
		slog.Error(err.Error(), "error", err)
	}
	if err := eg.Wait(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			err := fmt.Errorf("failed to close HTTP server: %w", err)
			slog.Error(err.Error(), "error", err)
		}
	}
}

func startHealth(agonesSDK *sdk.SDK) {
	ticker := time.NewTicker(2 * time.Second)
	defer ticker.Stop()
	for range ticker.C {
		if err := agonesSDK.Health(); err != nil {
			err := fmt.Errorf("failed to health Agones SDK: %w", err)
			slog.Error(err.Error(), "error", err)
		}
	}
}
