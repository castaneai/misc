package tests

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	e2eframework "agones.dev/agones/test/e2e/framework"
	"connectrpc.com/connect"
	"github.com/alicebob/miniredis/v2"
	"github.com/bufbuild/protovalidate-go"
	"github.com/google/uuid"
	"github.com/redis/rueidis"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobby"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1/lobbyv1connect"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/services"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	namespacePrefix = "agoneslobby-test-"
)

var framework *e2eframework.Framework

func TestMain(m *testing.M) {
	fw, err := e2eframework.NewFromFlags()
	if err != nil {
		log.Fatalf("failed to init e2e e2eframework: %+v", err)
	}
	framework = fw

	var exitCode int
	defer func() {
		os.Exit(exitCode)
	}()
	exitCode = m.Run()
}

func startLobbyServer(t *testing.T, namespace string) *httptest.Server {
	t.Helper()
	redisSv := miniredis.RunT(t)
	redisClient, err := rueidis.NewClient(rueidis.ClientOption{
		InitAddress:  []string{redisSv.Addr()},
		DisableCache: true,
	})
	if err != nil {
		t.Fatalf("failed to create redis client: %+v", err)
	}
	manager := lobby.NewRedisRoomManager(redisClient)
	allocator := lobby.NewAgonesAllocator(framework.AgonesClient, namespace)
	validator, err := protovalidate.New()
	if err != nil {
		t.Fatalf("failed to new validator: %+v", err)
	}
	interceptors := []connect.Interceptor{
		services.NewProtoValidateInterceptor(validator),
	}
	mux := http.NewServeMux()
	mux.Handle(lobbyv1connect.NewAdminServiceHandler(services.NewAdminService(manager, allocator), connect.WithInterceptors(interceptors...)))
	mux.Handle(lobbyv1connect.NewInternalServiceHandler(services.NewInternalService(manager), connect.WithInterceptors(interceptors...)))
	server := httptest.NewUnstartedServer(h2c.NewHandler(mux, &http2.Server{}))
	server.EnableHTTP2 = true
	server.Start()
	t.Cleanup(server.Close)
	return server
}

func newRandomNamespace(t *testing.T) string {
	t.Helper()
	namespace := fmt.Sprintf("%s%s", namespacePrefix, uuid.Must(uuid.NewRandom()))
	if err := framework.CreateNamespace(namespace); err != nil {
		panic(err)
	}
	t.Cleanup(func() {
		_ = framework.DeleteNamespace(namespace)
	})
	return namespace
}

func newFleet(t *testing.T, name, namespace string, replicas, maxRoomsPerGS int) *agonesv1.Fleet {
	t.Helper()
	flt, err := framework.AgonesClient.AgonesV1().
		Fleets(namespace).
		Create(context.Background(), &agonesv1.Fleet{
			ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace},
			Spec: agonesv1.FleetSpec{
				Replicas: int32(replicas),
				Template: agonesv1.GameServerTemplateSpec{
					Spec: agonesv1.GameServerSpec{
						Ports: []agonesv1.GameServerPort{{Name: "default", Protocol: "TCP", ContainerPort: 7654}},
						Template: corev1.PodTemplateSpec{
							Spec: corev1.PodSpec{
								Containers: []corev1.Container{
									{Name: "gameserver", Image: getTestGameServerImage(t)},
								},
							},
						},
						Lists: map[string]agonesv1.ListStatus{
							lobby.AgonesListKeyRooms: {Capacity: int64(maxRoomsPerGS)},
						},
					},
				},
			},
		}, metav1.CreateOptions{})
	if err != nil {
		t.Fatalf("failed to create fleet: %+v", err)
	}
	if err := framework.WaitForFleetCondition(t, flt, func(_ *logrus.Entry, f *agonesv1.Fleet) bool {
		ok := (f.Status.ReadyReplicas + f.Status.AllocatedReplicas + f.Status.ReservedReplicas) >= int32(replicas)
		if ok {
			flt = f
		}
		return ok
	}); err != nil {
		t.Fatalf("failed to wait for fleet stable: %+v", err)
	}
	return flt
}

// ref: skaffold.yaml
func getTestGameServerImage(t *testing.T) string {
	ctx := context.Background()
	res, err := framework.AgonesClient.AgonesV1().GameServers("default").List(ctx, metav1.ListOptions{Limit: 1})
	if err != nil || len(res.Items) < 1 {
		t.Fatalf("failed to get testgameserver in default namespace: %+v", err)
	}
	require.Len(t, res.Items, 1)
	for _, ct := range res.Items[0].Spec.Template.Spec.Containers {
		if ct.Name != "agones-gameserver-sidecar" {
			return ct.Image
		}
	}
	t.Fatalf("gameserver container not found in %+v", res.Items[0].Spec.Template.Spec.Containers)
	return ""
}
