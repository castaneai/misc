package tests

import (
	"context"
	"fmt"
	"testing"
	"time"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	"connectrpc.com/connect"
	"github.com/stretchr/testify/require"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobbysdk"
	lobbyv1 "gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1/lobbyv1connect"
)

func TestScenario(t *testing.T) {
	ns := newRandomNamespace(t)
	flt := newFleet(t, "test-fleet", ns, 3, 3)
	ctx := context.Background()
	server := startLobbyServer(t, ns)

	gss, err := framework.ListGameServersFromFleet(flt)
	require.NoError(t, err)
	sdks := map[string]*lobbysdk.LobbySDK{}
	for _, gs := range gss {
		gsAddr := getAgonesAddress(&gs)
		sdk, err := lobbysdk.NewLobbySDK(ctx, server.URL, lobbysdk.WithAgonesSDKAddr(gsAddr))
		require.NoError(t, err)
		sdks[gsAddr] = sdk
	}

	admin := lobbyv1connect.NewAdminServiceClient(server.Client(), server.URL)
	{
		resp, err := admin.ListRoom(ctx, connect.NewRequest(&lobbyv1.ListRoomRequest{}))
		require.NoError(t, err)
		require.Empty(t, resp.Msg.Rooms)
	}

	roomID1 := "room1"
	playerID1 := "player1"
	{
		resp, err := admin.AllocateRoom(ctx, connect.NewRequest(&lobbyv1.AllocateRoomRequest{
			RoomIds: []string{roomID1},
		}))
		require.NoError(t, err)
		allocatedRoom := resp.Msg.AllocatedRooms[0]
		t.Logf("allocated: %+v", allocatedRoom)
		require.NotEmpty(t, allocatedRoom.RoomId)
		require.Equal(t, roomID1, allocatedRoom.RoomId)

		// wait room added
		time.Sleep(100 * time.Millisecond)

		// Connect player
		sdks[allocatedRoom.Address].AddPlayer(roomID1, playerID1)
	}

	roomID2 := "room2"
	playerID2 := "player2"
	{
		resp, err := admin.AllocateRoom(ctx, connect.NewRequest(&lobbyv1.AllocateRoomRequest{
			RoomIds: []string{roomID2},
		}))
		require.NoError(t, err)
		allocatedRoom := resp.Msg.AllocatedRooms[0]
		t.Logf("allocated: %+v", allocatedRoom)
		require.NotEmpty(t, allocatedRoom.RoomId)
		require.Equal(t, roomID2, allocatedRoom.RoomId)

		// wait room added
		time.Sleep(100 * time.Millisecond)

		// connect Player
		sdks[allocatedRoom.Address].AddPlayer(roomID2, playerID2)
	}
	require.NotEqual(t, roomID1, roomID2)

	// wait apply players
	time.Sleep(100 * time.Millisecond)

	{
		resp, err := admin.ListRoom(ctx, connect.NewRequest(&lobbyv1.ListRoomRequest{}))
		require.NoError(t, err)
		require.Len(t, resp.Msg.Rooms, 2)
		require.Equal(t, playerID1, resp.Msg.Rooms[0].Players[0].PlayerId)
		require.Equal(t, playerID2, resp.Msg.Rooms[1].Players[0].PlayerId)
	}

}

func getAgonesAddress(gs *agonesv1.GameServer) string {
	if len(gs.Status.Ports) < 1 {
		return ""
	}
	return fmt.Sprintf("%s:%d", gs.Status.Address, gs.Status.Ports[0].Port)
}
