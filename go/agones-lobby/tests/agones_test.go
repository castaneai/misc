package tests

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/k8sutil"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobby"
)

func TestAgonesAllocator(t *testing.T) {
	ns := newRandomNamespace(t)
	maxRoomsPerGS := 2
	newFleet(t, "test-fleet", ns, 2, maxRoomsPerGS)
	agonescs, err := k8sutil.NewAgonesClient()
	require.NoError(t, err)
	allocator := lobby.NewAgonesAllocator(agonescs, ns)

	ctx := context.Background()
	// packed scheduling by allocate priority
	// gs1: [room1 room2]
	// gs2: []
	resp, err := allocator.AllocateRoom(ctx, nil, []lobby.RoomID{"room1", "room2"})
	require.NoError(t, err)

	// room3 is allocated to the next gs2 since gs1 has reached maxRoomsPerGS.
	// gs1: [room1 room2]
	// gs2: [room3]
	resp3, err := allocator.AllocateRoom(ctx, nil, []lobby.RoomID{"room3"})
	require.NoError(t, err)
	require.NotEqual(t, resp[0].Address, resp3[0].Address)

	// gs1: [room1 room2]
	// gs2: [room3 room4]
	resp4, err := allocator.AllocateRoom(ctx, nil, []lobby.RoomID{"room4"})
	require.NoError(t, err)
	require.Equal(t, resp3[0].Address, resp4[0].Address)
	require.NotEqual(t, resp[0].Address, resp4[0].Address)

	resp5, err := allocator.AllocateRoom(ctx, nil, []lobby.RoomID{"room5"})
	require.ErrorIs(t, err, lobby.ErrRoomUnAllocated)
	require.Empty(t, resp5)
}
