# agones-lobby

## Install

Using [Aqua](https://github.com/aquaproj/aqua).

```
aqua i
```

## Testing

```sh
kubectl config current-context
# => rancher-desktop or orbstack

helmfile apply
skaffold dev
go test -v ./...
```
