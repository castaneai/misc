package main

import (
	"log"
	"net/http"

	"connectrpc.com/connect"
	"github.com/bufbuild/protovalidate-go"
	"github.com/kelseyhightower/envconfig"
	"github.com/redis/rueidis"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/k8sutil"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobby"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1/lobbyv1connect"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/services"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

type config struct {
	RedisAddr string `envconfig:"REDIS_ADDR" required:"true"`
}

func main() {
	var conf config
	envconfig.MustProcess("", &conf)
	log.Printf("load config: %+v", conf)

	agonesClient, err := k8sutil.NewAgonesClient()
	if err != nil {
		log.Fatalf("failed to create agones client: %+v", err)
	}
	allocator := lobby.NewAgonesAllocator(agonesClient, "default")
	redis, err := rueidis.NewClient(rueidis.ClientOption{InitAddress: []string{conf.RedisAddr}})
	if err != nil {
		log.Fatalf("failed to init redis client: %+v", err)
	}
	manager := lobby.NewRedisRoomManager(redis)
	admin := services.NewAdminService(manager, allocator)

	validator, err := protovalidate.New()
	if err != nil {
		log.Fatalf("failed to new validator: %+v", err)
	}
	interceptors := []connect.Interceptor{
		services.NewProtoValidateInterceptor(validator),
	}

	mux := http.NewServeMux()
	path, handler := lobbyv1connect.NewAdminServiceHandler(admin, connect.WithInterceptors(interceptors...))
	mux.Handle(path, handler)
	addr := "localhost:8080"
	log.Printf("lobby server listening on %s...", addr)
	if err := http.ListenAndServe(
		addr,
		h2c.NewHandler(mux, &http2.Server{}),
	); err != nil {
		log.Fatal(err)
	}
}
