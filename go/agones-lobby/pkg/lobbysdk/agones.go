package lobbysdk

import (
	"context"
	"errors"
	"fmt"
	"io"
	"time"

	sdkpb "agones.dev/agones/pkg/sdk"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type agonesSDKWrapper struct {
	sdk          sdkpb.SDKClient
	healthClient sdkpb.SDK_HealthClient
}

func newAgonesSDKWrapper(ctx context.Context, sdkAddr string) (*agonesSDKWrapper, error) {
	cc, err := grpc.DialContext(ctx, sdkAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("failed to dial to Agones SDK: %w", err)
	}
	sdk := sdkpb.NewSDKClient(cc)
	healthClient, err := sdk.Health(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to dial to Agones SDK: %w", err)
	}
	return &agonesSDKWrapper{
		sdk:          sdk,
		healthClient: healthClient,
	}, nil
}

func (w *agonesSDKWrapper) Ready(ctx context.Context) error {
	if _, err := w.sdk.Ready(ctx, &sdkpb.Empty{}); err != nil {
		return fmt.Errorf("failed to send ready to Agones SDK: %w", err)
	}
	return nil
}

func (w *agonesSDKWrapper) Health() error {
	if err := w.healthClient.Send(&sdkpb.Empty{}); err != nil {
		return fmt.Errorf("failed to send Health to Agones SDK: %w", err)
	}
	return nil
}

func (w *agonesSDKWrapper) WatchGameServer(ctx context.Context, f func(gs *sdkpb.GameServer), onError func(err error)) error {
	stream, err := w.sdk.WatchGameServer(ctx, &sdkpb.Empty{})
	if err != nil {
		return fmt.Errorf("failed to watch GameServer: %w", err)
	}
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				gs, err := stream.Recv()
				if errors.Is(err, io.EOF) {
					return
				}
				if err != nil {
					onError(err)
					time.Sleep(1 * time.Second)
					continue
				}
				f(gs)
			}
		}
	}()
	return nil
}
