package lobbysdk

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	agonespb "agones.dev/agones/pkg/sdk"
	"connectrpc.com/connect"
	lobbyv1 "gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1/lobbyv1connect"
	"golang.org/x/net/http2"
)

const (
	AgonesListKeyRooms = "rooms"
)

type addPlayerMsg struct {
	RoomID string
	Player *lobbyv1.Player
}

type deletePlayerMsg struct {
	RoomID   string
	PlayerID string
}

type LobbySDK struct {
	api            lobbyv1connect.InternalServiceClient
	agones         *agonesSDKWrapper
	roomDeleteCh   chan string
	playerAddCh    chan addPlayerMsg
	playerDeleteCh chan deletePlayerMsg
	options        *options
}

type options struct {
	agonesSDKAddr        string
	agonesHealthInterval time.Duration
	ackRoomInterval      time.Duration
	// TODO: api retry & timeout config
}

func defaultOptions() *options {
	return &options{
		agonesSDKAddr:        "127.0.0.1:9357",
		agonesHealthInterval: 5 * time.Second,
		ackRoomInterval:      5 * time.Second,
	}
}

type Options interface {
	apply(opts *options)
}

type OptionsFunc func(*options)

func (f OptionsFunc) apply(opts *options) {
	f(opts)
}

func WithAgonesSDKAddr(addr string) Options {
	return OptionsFunc(func(opts *options) {
		opts.agonesSDKAddr = addr
	})
}

func WithAckRoomInterval(d time.Duration) Options {
	return OptionsFunc(func(opts *options) {
		opts.ackRoomInterval = d
	})
}

func NewLobbySDK(ctx context.Context, internalAPIUrl string, ops ...Options) (*LobbySDK, error) {
	opts := defaultOptions()
	for _, o := range ops {
		o.apply(opts)
	}
	agones, err := newAgonesSDKWrapper(ctx, opts.agonesSDKAddr)
	if err != nil {
		return nil, fmt.Errorf("failed to new AgonesSDK: %w", err)
	}
	if err := agones.Ready(ctx); err != nil {
		return nil, fmt.Errorf("failed to send Ready to Agones: %w", err)
	}
	api := lobbyv1connect.NewInternalServiceClient(newInsecureClient(), internalAPIUrl)
	sdk := &LobbySDK{
		api:            api,
		agones:         agones,
		roomDeleteCh:   make(chan string),
		playerAddCh:    make(chan addPlayerMsg),
		playerDeleteCh: make(chan deletePlayerMsg),
		options:        opts,
	}
	go sdk.startAgonesHealth(ctx)
	go sdk.startLoop(ctx)
	return sdk, nil
}

func (s *LobbySDK) DeleteRoom(roomID string) {
	s.roomDeleteCh <- roomID
}

func (s *LobbySDK) AddPlayer(roomID, playerID string) {
	s.playerAddCh <- addPlayerMsg{RoomID: roomID, Player: &lobbyv1.Player{PlayerId: playerID}}
}

func (s *LobbySDK) DeletePlayer(roomID, playerID string) {
	s.playerDeleteCh <- deletePlayerMsg{RoomID: roomID, PlayerID: playerID}
}

func (s *LobbySDK) startLoop(ctx context.Context) {
	rooms := newRoomMap()
	roomAddedCh := make(chan *lobbyv1.AllocatedRoom)
	if err := s.agones.WatchGameServer(ctx, func(gs *agonespb.GameServer) {
		l, ok := gs.Status.Lists[AgonesListKeyRooms]
		if !ok {
			return
		}
		roomAdded := rooms.onNewValues(l.Values)
		for _, roomID := range roomAdded {
			roomAddedCh <- &lobbyv1.AllocatedRoom{RoomId: roomID, Address: getAgonesAddress(gs)}
		}
	}, func(err error) {
		log.Printf("error occured in watch agones GS: %+v", err)
	}); err != nil {
		log.Printf("failed to start watch agones GS: %+v", err)
		return
	}

	ticker := time.NewTicker(s.options.ackRoomInterval)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case room := <-roomAddedCh:
			if _, err := s.api.AddRoom(ctx, connect.NewRequest(&lobbyv1.AddRoomRequest{AddRooms: []*lobbyv1.AllocatedRoom{room}})); err != nil {
				log.Printf("failed to add room: %+v", err)
			}
		case roomID := <-s.roomDeleteCh:
			if _, err := s.api.DeleteRoom(ctx, connect.NewRequest(&lobbyv1.DeleteRoomRequest{DeleteRoomIds: []string{roomID}})); err != nil {
				log.Printf("failed to delete room: %+v", err)
			}
		case msg := <-s.playerAddCh:
			if _, err := s.api.AddPlayer(ctx, connect.NewRequest(&lobbyv1.AddPlayerRequest{
				RoomId: msg.RoomID,
				Player: msg.Player,
			})); err != nil {
				log.Printf("failed to add player: %+v", err)
			}
		case msg := <-s.playerDeleteCh:
			if _, err := s.api.DeletePlayer(ctx, connect.NewRequest(&lobbyv1.DeletePlayerRequest{
				RoomId:   msg.RoomID,
				PlayerId: msg.PlayerID,
			})); err != nil {
				log.Printf("failed to delete player: %+v", err)
			}
		case <-ticker.C:
			ackRoomIDs := rooms.values()
			if len(ackRoomIDs) == 0 {
				continue
			}
			if _, err := s.api.AckRoom(ctx, connect.NewRequest(&lobbyv1.AckRoomRequest{AckRoomIds: rooms.values()})); err != nil {
				log.Printf("failed to ack room: %+v", err)
			}
		}
	}
}

func (s *LobbySDK) startAgonesHealth(ctx context.Context) {
	ticker := time.NewTicker(s.options.agonesHealthInterval)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			_ = s.agones.Health()
		}
	}
}

type roomMap struct {
	m  map[string]struct{}
	mu sync.RWMutex
}

func newRoomMap() *roomMap {
	return &roomMap{
		m:  map[string]struct{}{},
		mu: sync.RWMutex{},
	}
}

func (m *roomMap) values() []string {
	m.mu.RLock()
	defer m.mu.RUnlock()
	var roomIDs []string
	for rid := range m.m {
		roomIDs = append(roomIDs, rid)
	}
	return roomIDs
}

func (m *roomMap) onNewValues(values []string) (roomAdded []string) {
	m.mu.Lock()
	defer m.mu.Unlock()
	for _, rid := range values {
		if _, ok := m.m[rid]; !ok {
			m.m[rid] = struct{}{}
			roomAdded = append(roomAdded, rid)
		}
	}
	return
}

func newInsecureClient() *http.Client {
	return &http.Client{
		Transport: &http2.Transport{
			AllowHTTP: true,
			DialTLSContext: func(ctx context.Context, network, addr string, cfg *tls.Config) (net.Conn, error) {
				return net.Dial(network, addr)
			},
			// Don't forget timeouts!
		},
	}
}

func getAgonesAddress(gs *agonespb.GameServer) string {
	if len(gs.Status.Ports) < 1 {
		return ""
	}
	return fmt.Sprintf("%s:%d", gs.Status.Address, gs.Status.Ports[0].Port)
}
