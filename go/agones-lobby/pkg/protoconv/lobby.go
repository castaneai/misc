package protoconv

import (
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobby"
	lobbyv1 "gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1"
)

func RoomFromProto(pbRoom *lobbyv1.Room) *lobby.Room {
	var players []*lobby.Player
	for _, pb := range pbRoom.Players {
		players = append(players, PlayerFromProto(pb))
	}
	return &lobby.Room{
		RoomID:  lobby.RoomID(pbRoom.RoomId),
		Address: pbRoom.Address,
		Players: players,
	}
}

func RoomToProto(room *lobby.Room) *lobbyv1.Room {
	var players []*lobbyv1.Player
	for _, p := range room.Players {
		players = append(players, PlayerToProto(p))
	}
	return &lobbyv1.Room{
		RoomId:  string(room.RoomID),
		Address: room.Address,
		Players: players,
	}
}

func AllocatedRoomToProto(room *lobby.AllocatedRoom) *lobbyv1.AllocatedRoom {
	return &lobbyv1.AllocatedRoom{
		RoomId:  string(room.RoomID),
		Address: room.Address,
	}
}

func PlayerFromProto(pbPlayer *lobbyv1.Player) *lobby.Player {
	return &lobby.Player{PlayerID: lobby.PlayerID(pbPlayer.PlayerId)}
}

func PlayerToProto(player *lobby.Player) *lobbyv1.Player {
	return &lobbyv1.Player{
		PlayerId: string(player.PlayerID),
	}
}
