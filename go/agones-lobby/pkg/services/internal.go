package services

import (
	"context"
	"fmt"

	"connectrpc.com/connect"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobby"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobbylog"
	lobbyv1 "gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1/lobbyv1connect"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/protoconv"
)

type internalService struct {
	manager lobby.RoomManager
}

func NewInternalService(manager lobby.RoomManager) lobbyv1connect.InternalServiceHandler {
	return &internalService{manager: manager}
}

func (s *internalService) AddRoom(ctx context.Context, req *connect.Request[lobbyv1.AddRoomRequest]) (*connect.Response[lobbyv1.AddRoomResponse], error) {
	var addRooms []*lobby.Room
	for _, addRoom := range req.Msg.AddRooms {
		addRooms = append(addRooms, &lobby.Room{RoomID: lobby.RoomID(addRoom.RoomId), Address: addRoom.Address})
	}
	if err := s.manager.AddRooms(ctx, addRooms); err != nil {
		return nil, fmt.Errorf("failed to add room: %w", err)
	}
	for _, room := range addRooms {
		lobbylog.Debugf("room added '%s'", room)
	}
	return connect.NewResponse(&lobbyv1.AddRoomResponse{}), nil
}

func (s *internalService) AckRoom(ctx context.Context, req *connect.Request[lobbyv1.AckRoomRequest]) (*connect.Response[lobbyv1.AckRoomResponse], error) {
	var ackRoomIDs []lobby.RoomID
	for _, roomID := range req.Msg.AckRoomIds {
		ackRoomIDs = append(ackRoomIDs, lobby.RoomID(roomID))
	}
	if err := s.manager.AckRooms(ctx, ackRoomIDs); err != nil {
		return nil, fmt.Errorf("failed to ack rooms: %w", err)
	}
	return connect.NewResponse(&lobbyv1.AckRoomResponse{}), nil
}

func (s *internalService) DeleteRoom(ctx context.Context, req *connect.Request[lobbyv1.DeleteRoomRequest]) (*connect.Response[lobbyv1.DeleteRoomResponse], error) {
	var deleteRoomIDs []lobby.RoomID
	for _, roomID := range req.Msg.DeleteRoomIds {
		deleteRoomIDs = append(deleteRoomIDs, lobby.RoomID(roomID))
	}
	if err := s.manager.DeleteRooms(ctx, deleteRoomIDs); err != nil {
		return nil, fmt.Errorf("failed to delete rooms: %w", err)
	}
	for _, roomID := range deleteRoomIDs {
		lobbylog.Debugf("room deleted '%s'", roomID)
	}
	return connect.NewResponse(&lobbyv1.DeleteRoomResponse{}), nil
}

func (s *internalService) AddPlayer(ctx context.Context, req *connect.Request[lobbyv1.AddPlayerRequest]) (*connect.Response[lobbyv1.AddPlayerResponse], error) {
	if err := s.manager.AddPlayers(ctx, map[lobby.RoomID][]*lobby.Player{
		lobby.RoomID(req.Msg.RoomId): {protoconv.PlayerFromProto(req.Msg.Player)},
	}); err != nil {
		return nil, err
	}
	lobbylog.Debugf("player added '%s' to room '%s'", req.Msg.Player.PlayerId, req.Msg.RoomId)
	return connect.NewResponse(&lobbyv1.AddPlayerResponse{}), nil
}

func (s *internalService) DeletePlayer(ctx context.Context, req *connect.Request[lobbyv1.DeletePlayerRequest]) (*connect.Response[lobbyv1.DeletePlayerResponse], error) {
	if err := s.manager.DeletePlayers(ctx, map[lobby.RoomID][]lobby.PlayerID{
		lobby.RoomID(req.Msg.RoomId): {lobby.PlayerID(req.Msg.PlayerId)},
	}); err != nil {
		return nil, err
	}
	lobbylog.Debugf("player deleted '%s' from room '%s'", req.Msg.PlayerId, req.Msg.RoomId)
	return connect.NewResponse(&lobbyv1.DeletePlayerResponse{}), nil
}
