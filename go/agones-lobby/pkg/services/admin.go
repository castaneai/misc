package services

import (
	"context"
	"errors"
	"fmt"

	"connectrpc.com/connect"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobby"
	lobbyv1 "gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1/lobbyv1connect"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/protoconv"
)

type adminService struct {
	manager   lobby.RoomManager
	allocator lobby.RoomAllocator
}

func NewAdminService(manager lobby.RoomManager, allocator lobby.RoomAllocator) lobbyv1connect.AdminServiceHandler {
	return &adminService{manager: manager, allocator: allocator}
}

func (s *adminService) ListRoom(ctx context.Context, req *connect.Request[lobbyv1.ListRoomRequest]) (*connect.Response[lobbyv1.ListRoomResponse], error) {
	// TODO: selector
	rooms, err := s.manager.ListRoom(ctx, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to list room: %w", err)
	}
	var pbRooms []*lobbyv1.Room
	for _, room := range rooms {
		pbRooms = append(pbRooms, protoconv.RoomToProto(room))
	}
	return connect.NewResponse(&lobbyv1.ListRoomResponse{
		Rooms: pbRooms,
	}), nil
}

func (s *adminService) AllocateRoom(ctx context.Context, req *connect.Request[lobbyv1.AllocateRoomRequest]) (*connect.Response[lobbyv1.AllocateRoomResponse], error) {
	var roomIDs []lobby.RoomID
	for _, roomID := range req.Msg.RoomIds {
		roomIDs = append(roomIDs, lobby.RoomID(roomID))
	}
	// TODO: selector
	rooms, err := s.allocator.AllocateRoom(ctx, nil, roomIDs)
	if errors.Is(err, lobby.ErrRoomUnAllocated) {
		return connect.NewResponse(&lobbyv1.AllocateRoomResponse{
			Status: lobbyv1.AllocateRoomStatus_ALLOCATE_ROOM_STATUS_FAILED,
		}), nil
	}
	if err != nil {
		return nil, err
	}

	var allocs []*lobbyv1.AllocatedRoom
	for _, room := range rooms {
		allocs = append(allocs, protoconv.AllocatedRoomToProto(&room))
	}
	return connect.NewResponse(&lobbyv1.AllocateRoomResponse{
		Status:         lobbyv1.AllocateRoomStatus_ALLOCATE_ROOM_STATUS_OK,
		AllocatedRooms: allocs,
	}), nil
}
