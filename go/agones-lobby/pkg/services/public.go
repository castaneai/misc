package services

import (
	"context"
	"errors"
	"math/rand"

	"connectrpc.com/connect"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/lobby"
	v1 "gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1"
	"gitlab.com/castaneai/misc/go/agones-lobby/pkg/proto/lobby/v1/lobbyv1connect"
)

type publicService struct {
	manager   lobby.RoomManager
	allocator lobby.RoomAllocator
}

func NewPublicService(manager lobby.RoomManager, allocator lobby.RoomAllocator) lobbyv1connect.PublicServiceHandler {
	return &publicService{manager: manager, allocator: allocator}
}

func (s *publicService) FindRandomRoom(ctx context.Context, req *connect.Request[v1.FindRandomRoomRequest]) (*connect.Response[v1.FindRandomRoomResponse], error) {
	rooms, err := s.manager.ListRoom(ctx, nil)
	if err != nil {
		return nil, err
	}
	if len(rooms) == 0 {
		return nil, connect.NewError(connect.CodeNotFound, errors.New("no room in lobby"))
	}
	room := rooms[rand.Intn(len(rooms))]
	return connect.NewResponse(&v1.FindRandomRoomResponse{RoomIds: []string{string(room.RoomID)}}), nil
}

func (s *publicService) JoinRoom(ctx context.Context, c *connect.Request[v1.JoinRoomRequest]) (*connect.Response[v1.JoinRoomResponse], error) {
	panic("implement me")
}
