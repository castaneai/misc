package lobby

import (
	"context"
	"fmt"
	"time"
	"unsafe"

	"github.com/redis/rueidis"
	"github.com/vmihailenco/msgpack/v5"
)

const (
	redisKeyRoomIDs = "roomIDs"
	roomExpires     = 1 * time.Minute
	playerExpires   = 1 * time.Minute
)

func redisKeyRoom(roomID RoomID) string {
	return fmt.Sprintf("room_%s", roomID)
}

func redisKeyPlayer(playerID PlayerID) string {
	return fmt.Sprintf("player_%s", playerID)
}

type RedisRoomManager struct {
	c rueidis.Client
}

func NewRedisRoomManager(client rueidis.Client) *RedisRoomManager {
	return &RedisRoomManager{c: client}
}

func (m *RedisRoomManager) ListRoom(ctx context.Context, selector RoomSelector) ([]*Room, error) {
	query := m.c.B().Smembers().Key(redisKeyRoomIDs).Build()
	roomIDs, err := m.c.Do(ctx, query).AsStrSlice()
	if err != nil {
		return nil, err
	}
	queries := make([]rueidis.Completed, len(roomIDs))
	for i, roomID := range roomIDs {
		queries[i] = m.c.B().Get().Key(redisKeyRoom(RoomID(roomID))).Build()
	}
	var rooms []*Room
	for _, res := range m.c.DoMulti(ctx, queries...) {
		if err := res.Error(); err != nil {
			if rueidis.IsRedisNil(err) {
				// room not found
				continue
			}
			return nil, fmt.Errorf("failed to exec redis command: %w", err)
		}
		b, err := res.AsBytes()
		if err != nil {
			return nil, fmt.Errorf("failed to read redis value as bytes: %w", err)
		}
		var room Room
		if err := msgpack.Unmarshal(b, &room); err != nil {
			return nil, fmt.Errorf("failed to unmarshal room bytes: %w", err)
		}
		rooms = append(rooms, &room)
	}
	return rooms, nil
}

func (m *RedisRoomManager) AddRooms(ctx context.Context, rooms []*Room) error {
	queries := make([]rueidis.Completed, len(rooms)+1)
	roomIDs := make([]string, len(rooms))
	keys := make([]string, len(rooms))
	for i, room := range rooms {
		roomIDs[i] = string(room.RoomID)
		keys[i] = redisKeyRoom(room.RoomID)
		data, err := msgpack.Marshal(room)
		if err != nil {
			return fmt.Errorf("failed to marshal room bytes to msgpack: %w", err)
		}
		queries[i] = m.c.B().Set().Key(keys[i]).Value(unsafe.String(&data[0], len(data))).Ex(roomExpires).Build()
	}
	queries[len(queries)-1] = m.c.B().Sadd().Key(redisKeyRoomIDs).Member(roomIDs...).Build()
	for _, res := range m.c.DoMulti(ctx, queries...) {
		if err := res.Error(); err != nil {
			return fmt.Errorf("failed to exec redis command: %w", err)
		}
	}
	return nil
}

func (m *RedisRoomManager) AckRooms(ctx context.Context, roomIDs []RoomID) error {
	queries := make([]rueidis.Completed, len(roomIDs))
	for i, roomID := range roomIDs {
		expireSeconds := int64(roomExpires / time.Second)
		queries[i] = m.c.B().Expire().Key(redisKeyRoom(roomID)).Seconds(expireSeconds).Build()
	}
	for _, res := range m.c.DoMulti(ctx, queries...) {
		if err := res.Error(); err != nil {
			return fmt.Errorf("failed to set expire to redis: %w", err)
		}
	}
	// TODO: ack Player expires
	return nil
}

func (m *RedisRoomManager) DeleteRooms(ctx context.Context, roomIDs []RoomID) error {
	deleteRoomIDs := make([]string, len(roomIDs))
	for i, roomID := range roomIDs {
		deleteRoomIDs[i] = string(roomID)
	}
	queries := []rueidis.Completed{
		m.c.B().Srem().Key(redisKeyRoomIDs).Member(deleteRoomIDs...).Build(),
		m.c.B().Del().Key(deleteRoomIDs...).Build(),
	}
	for _, res := range m.c.DoMulti(ctx, queries...) {
		if err := res.Error(); err != nil {
			return fmt.Errorf("failed to delete room: %w", err)
		}
	}
	return nil
}

func (m *RedisRoomManager) AddPlayers(ctx context.Context, players map[RoomID][]*Player) error {
	var readQueries []rueidis.Completed
	var writeQueries []rueidis.Completed
	for roomID, addPlayers := range players {
		readQueries = append(readQueries, m.c.B().Get().
			Key(redisKeyRoom(roomID)).Build(),
		)
		for _, player := range addPlayers {
			data, err := msgpack.Marshal(player)
			if err != nil {
				return fmt.Errorf("failed to marshal player to msgpack: %w", err)
			}
			writeQueries = append(writeQueries, m.c.B().Set().
				Key(redisKeyPlayer(player.PlayerID)).
				Value(unsafe.String(&data[0], len(data))).
				Ex(playerExpires).Build(),
			)
		}
	}
	updateRooms := map[RoomID]*Room{}
	for _, res := range m.c.DoMulti(ctx, readQueries...) {
		if err := res.Error(); err != nil {
			if rueidis.IsRedisNil(err) {
				// room not found
				continue
			}
			return err
		}
		data, err := res.AsBytes()
		if err != nil {
			return err
		}
		var room Room
		if err := msgpack.Unmarshal(data, &room); err != nil {
			return fmt.Errorf("failed to unmarshal room bytes: %w", err)
		}
		updateRooms[room.RoomID] = &room
	}
	for roomID, addPlayers := range players {
		if _, ok := updateRooms[roomID]; ok {
			updateRooms[roomID].Players = append(updateRooms[roomID].Players, addPlayers...)
		}
	}
	for _, room := range updateRooms {
		data, err := msgpack.Marshal(room)
		if err != nil {
			return fmt.Errorf("failed to marshal room bytes: %w", err)
		}
		writeQueries = append(writeQueries, m.c.B().Set().
			Key(redisKeyRoom(room.RoomID)).
			Value(unsafe.String(&data[0], len(data))).
			Ex(roomExpires).Build(),
		)
	}
	for _, res := range m.c.DoMulti(ctx, writeQueries...) {
		if err := res.Error(); err != nil {
			return err
		}
	}
	return nil
}

func (m *RedisRoomManager) DeletePlayers(ctx context.Context, players map[RoomID][]PlayerID) error {
	var readQueries []rueidis.Completed
	var writeQueries []rueidis.Completed
	for roomID, delPlayerIDs := range players {
		readQueries = append(readQueries, m.c.B().Get().
			Key(redisKeyRoom(roomID)).Build(),
		)
		for _, playerID := range delPlayerIDs {
			writeQueries = append(writeQueries, m.c.B().Del().
				Key(redisKeyPlayer(playerID)).Build(),
			)
		}
	}
	updateRooms := map[RoomID]*Room{}
	for _, res := range m.c.DoMulti(ctx, readQueries...) {
		if err := res.Error(); err != nil {
			if rueidis.IsRedisNil(err) {
				// room not found
				continue
			}
			return err
		}
		data, err := res.AsBytes()
		if err != nil {
			return err
		}
		var room Room
		if err := msgpack.Unmarshal(data, &room); err != nil {
			return fmt.Errorf("failed to unmarshal room bytes: %w", err)
		}
		updateRooms[room.RoomID] = &room
	}
	for roomID, delPlayerIDs := range players {
		if _, ok := updateRooms[roomID]; ok {
			updateRooms[roomID].Players = filter(updateRooms[roomID].Players, func(player *Player) bool {
				return !contains(delPlayerIDs, player.PlayerID)
			})
		}
	}
	for _, room := range updateRooms {
		data, err := msgpack.Marshal(room)
		if err != nil {
			return fmt.Errorf("failed to marshal room bytes: %w", err)
		}
		writeQueries = append(writeQueries, m.c.B().Set().
			Key(redisKeyRoom(room.RoomID)).
			Value(unsafe.String(&data[0], len(data))).
			Ex(roomExpires).Build(),
		)
	}
	for _, res := range m.c.DoMulti(ctx, writeQueries...) {
		if err := res.Error(); err != nil {
			return err
		}
	}
	return nil
}

func filter[T any](list []T, predicate func(T) bool) []T {
	var fl []T
	for _, v := range list {
		if predicate(v) {
			fl = append(fl, v)
		}
	}
	return fl
}

func contains[T comparable](list []T, n T) bool {
	for _, v := range list {
		if v == n {
			return true
		}
	}
	return false
}
