package lobby

import (
	"context"
	"fmt"
)

type RoomID string
type PlayerID string

type Room struct {
	RoomID  RoomID
	Address string
	Players []*Player
}

func (r *Room) String() string {
	var pids []PlayerID
	for _, p := range r.Players {
		pids = append(pids, p.PlayerID)
	}
	return fmt.Sprintf("Room(id: %s, addr: %s, players: %v)", r.RoomID, r.Address, pids)
}

type AllocatedRoom struct {
	RoomID  RoomID
	Address string
}

type Player struct {
	PlayerID PlayerID
}

type RoomManager interface {
	ListRoom(ctx context.Context, selector RoomSelector) ([]*Room, error)
	AddRooms(ctx context.Context, rooms []*Room) error
	AckRooms(ctx context.Context, roomIDs []RoomID) error
	DeleteRooms(ctx context.Context, roomIDs []RoomID) error
	AddPlayers(ctx context.Context, players map[RoomID][]*Player) error
	DeletePlayers(ctx context.Context, players map[RoomID][]PlayerID) error
}

type RoomSelector interface{}
