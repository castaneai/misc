package lobby

import (
	"context"
	"errors"
)

type RoomAllocator interface {
	AllocateRoom(ctx context.Context, selector RoomSelector, rooms []RoomID) ([]AllocatedRoom, error)
}

var (
	ErrRoomUnAllocated = errors.New("room unallocated")
)
