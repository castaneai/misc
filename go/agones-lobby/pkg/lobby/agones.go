package lobby

import (
	"context"
	"fmt"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	allocationv1 "agones.dev/agones/pkg/apis/allocation/v1"
	"agones.dev/agones/pkg/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	AgonesListKeyRooms = "rooms"
)

type AgonesAllocator struct {
	cs        versioned.Interface
	namespace string
}

func NewAgonesAllocator(cs versioned.Interface, namespace string) *AgonesAllocator {
	return &AgonesAllocator{
		cs:        cs,
		namespace: namespace,
	}
}

func (m *AgonesAllocator) AllocateRoom(ctx context.Context, selector RoomSelector, rooms []RoomID) ([]AllocatedRoom, error) {
	// TODO: selector
	var roomsList []string
	for _, rid := range rooms {
		roomsList = append(roomsList, string(rid))
	}
	alloc := &allocationv1.GameServerAllocation{
		Spec: allocationv1.GameServerAllocationSpec{
			Selectors: []allocationv1.GameServerSelector{
				{GameServerState: ref(agonesv1.GameServerStateAllocated), Lists: map[string]allocationv1.ListSelector{
					AgonesListKeyRooms: {MinAvailable: int64(len(rooms))},
				}},
				{GameServerState: ref(agonesv1.GameServerStateReady), Lists: map[string]allocationv1.ListSelector{
					AgonesListKeyRooms: {MinAvailable: int64(len(rooms))},
				}},
			},
			Lists: map[string]allocationv1.ListAction{
				AgonesListKeyRooms: {AddValues: roomsList},
			},
			Priorities: []agonesv1.Priority{
				{Type: "List", Key: AgonesListKeyRooms, Order: "Ascending"},
			},
		},
	}
	resp, err := m.cs.AllocationV1().GameServerAllocations(m.namespace).Create(ctx, alloc, metav1.CreateOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to allocate room: %w", err)
	}
	if resp.Status.State != allocationv1.GameServerAllocationAllocated {
		return nil, ErrRoomUnAllocated
	}
	var results []AllocatedRoom
	for _, roomID := range rooms {
		results = append(results, AllocatedRoom{RoomID: roomID, Address: getAgonesAddress(resp)})
	}
	return results, nil
}

func getAgonesAddress(alloc *allocationv1.GameServerAllocation) string {
	if len(alloc.Status.Ports) < 1 {
		return ""
	}
	return fmt.Sprintf("%s:%d", alloc.Status.Address, alloc.Status.Ports[0].Port)
}

func ref[T any](s T) *T {
	return &s
}
