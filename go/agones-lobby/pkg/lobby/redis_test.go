package lobby

import (
	"context"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/redis/rueidis"
	"github.com/stretchr/testify/require"
)

func TestRedisRoomManager(t *testing.T) {
	s := miniredis.RunT(t)
	c, err := rueidis.NewClient(rueidis.ClientOption{InitAddress: []string{s.Addr()}, DisableCache: true})
	require.NoError(t, err)
	manager := NewRedisRoomManager(c)
	ctx := context.Background()

	rooms, err := manager.ListRoom(ctx, nil)
	require.NoError(t, err)
	require.Empty(t, rooms)

	room1 := &Room{RoomID: "room1", Address: "addr1"}
	room2 := &Room{RoomID: "room2", Address: "addr2"}
	err = manager.AddRooms(ctx, []*Room{room1, room2})
	require.NoError(t, err)

	rooms, err = manager.ListRoom(ctx, nil)
	require.NoError(t, err)
	require.Len(t, rooms, 2)

	err = manager.DeleteRooms(ctx, []RoomID{room1.RoomID})
	require.NoError(t, err)

	rooms, err = manager.ListRoom(ctx, nil)
	require.NoError(t, err)
	require.Len(t, rooms, 1)
	require.Equal(t, RoomID("room2"), rooms[0].RoomID)
	require.Empty(t, rooms[0].Players)

	err = manager.AddPlayers(ctx, map[RoomID][]*Player{
		"room2": {
			{PlayerID: "p1"},
			{PlayerID: "p2"},
		},
	})
	require.NoError(t, err)

	rooms, err = manager.ListRoom(ctx, nil)
	require.NoError(t, err)
	require.Len(t, rooms, 1)
	require.ElementsMatch(t, []*Player{{PlayerID: "p1"}, {PlayerID: "p2"}}, rooms[0].Players)

	err = manager.DeletePlayers(ctx, map[RoomID][]PlayerID{
		"room1": {"p1"}, // room not found
		"room2": {"p1"},
	})
	require.NoError(t, err)

	rooms, err = manager.ListRoom(ctx, nil)
	require.NoError(t, err)
	require.Len(t, rooms, 1)
	require.ElementsMatch(t, []*Player{{PlayerID: "p2"}}, rooms[0].Players)
}
