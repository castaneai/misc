package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	agonessdk "agones.dev/agones/sdks/go"
	"golang.org/x/sync/errgroup"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	defer cancel()
	sdk, err := agonessdk.NewSDK()
	if err != nil {
		log.Fatalf("failed to new Agones SDK: %+v", err)
	}
	go startHealth(ctx, sdk)

	addr := ":7654"
	if p := os.Getenv("PORT"); p != "" {
		addr = fmt.Sprintf(":%s", p)
	}
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen: %+v", err)
	}
	log.Printf("gameserver (lobby SDK forwarder) now listening on %s...", addr)
	for {
		acceptedConn, err := listener.Accept()
		if err != nil {
			log.Fatalf("failed to accept: %+v", err)
		}
		go startSDKProxy(ctx, acceptedConn)
	}
}

func startHealth(ctx context.Context, agones *agonessdk.SDK) {
	if err := agones.Ready(); err != nil {
		log.Printf("failed to send Ready to Agones SDK: %+v", err)
	}
	ticker := time.NewTicker(3 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			log.Printf("shutdown requested: %+v", ctx.Err())
			return
		case <-ticker.C:
			if err := agones.Health(); err != nil {
				log.Printf("failed to send Health to Agones SDK: %+v", err)
			}
		}
	}
}

func startSDKProxy(ctx context.Context, acceptedConn net.Conn) {
	defer acceptedConn.Close()

	conn, err := net.Dial("tcp", "localhost:9357") // Agones SDK sidecar addr
	if err != nil {
		log.Printf("failed to dial to Agones SDK: %+v", err)
		return
	}
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		_, err := io.Copy(conn, acceptedConn)
		return err
	})
	eg.Go(func() error {
		_, err := io.Copy(acceptedConn, conn)
		return err
	})
	if err := eg.Wait(); err != nil {
		log.Printf("failed to proxy: %+v", err)
	}
}
