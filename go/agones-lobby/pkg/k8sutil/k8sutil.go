package k8sutil

import (
	"agones.dev/agones/pkg/client/clientset/versioned"
	"k8s.io/client-go/tools/clientcmd"
)

func NewAgonesClient() (versioned.Interface, error) {
	// https://pkg.go.dev/k8s.io/client-go/tools/clientcmd#pkg-overview
	rule := clientcmd.NewDefaultClientConfigLoadingRules()
	configOverrides := &clientcmd.ConfigOverrides{}
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(rule, configOverrides)
	restConfig, err := kubeConfig.ClientConfig()
	if err != nil {
		return nil, err
	}
	restConfig.QPS = 100
	restConfig.Burst = 100
	return versioned.NewForConfig(restConfig)
}
