package main

import (
	"context"
	"flag"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/examples/helloworld/helloworld"
)

func main() {
	var addr string
	flag.StringVar(&addr, "addr", "localhost:50501", "server addr")
	flag.Parse()
	log.Printf("start client (target: %s)", addr)

	ticker := time.NewTicker(1000 * time.Millisecond)
	defer ticker.Stop()

	for range ticker.C {
		cc, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			log.Fatalf("failed to dial to server: %+v", err)
		}
		log.Printf("gRPC conn target: %s", cc.Target())
		client := helloworld.NewGreeterClient(cc)

		ctx := context.Background()
		reply, err := client.SayHello(ctx, &helloworld.HelloRequest{Name: "test"})
		if err != nil {
			log.Fatalf("failed to say hello: %+v", err)
		}
		log.Printf("reply: %s", reply.Message)
	}
}
