package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/examples/helloworld/helloworld"
)

func main() {
	s := grpc.NewServer()
	helloworld.RegisterGreeterServer(s, &greeterServer{})
	addr := ":50501"
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("greeter server listening on %s...", addr)
	if err := s.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

type greeterServer struct {
	helloworld.UnimplementedGreeterServer
}

func (s *greeterServer) SayHello(ctx context.Context, request *helloworld.HelloRequest) (*helloworld.HelloReply, error) {
	log.Printf("on sayhello")
	return &helloworld.HelloReply{Message: "hello"}, nil
}
