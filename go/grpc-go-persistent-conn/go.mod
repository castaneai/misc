module gitlab.com/castaneai/misc/go/grpc-go-persistent-conn

go 1.19

require (
	google.golang.org/grpc v1.54.0
	google.golang.org/grpc/examples v0.0.0-20230419175434-2cd95c7514a3
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
