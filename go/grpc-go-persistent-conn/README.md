# gRPC-Go persistent connection example

```
aqua i
make up
make run
stern server
```

gRPC-Go Client performs load balancing on the client side for the same destination address.

![](./stern.jpg)
