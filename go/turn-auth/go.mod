module gitlab.com/castaneai/misc/go/turn-auth

go 1.18

require github.com/pion/turn/v2 v2.0.8

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pion/logging v0.2.2 // indirect
	github.com/pion/randutil v0.1.0 // indirect
	github.com/pion/stun v0.3.5 // indirect
	github.com/pion/transport v0.13.0 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
)
