package main

import (
	"log"
	"net"
	"os"
	"os/signal"

	"github.com/pion/turn/v2"
)

const (
	realm        = "example.com"
	sharedSecret = "foobar"
)

func main() {
	udpLis, err := net.ListenPacket("udp4", "localhost:3478")
	if err != nil {
		log.Fatalf("failed to listen UDP: %+v", err)
	}

	s, err := turn.NewServer(turn.ServerConfig{
		Realm:       realm,
		AuthHandler: turn.NewLongTermAuthHandler(sharedSecret, nil),
		PacketConnConfigs: []turn.PacketConnConfig{
			{
				PacketConn: udpLis,
				RelayAddressGenerator: &turn.RelayAddressGeneratorStatic{
					RelayAddress: net.ParseIP("127.0.0.1"),
					Address:      "0.0.0.0",
				},
			},
		},
	})
	if err != nil {
		log.Fatalf("failed to new turn server: %+v", err)
	}
	defer s.Close()
	log.Printf("listening on udp:%s...", udpLis.LocalAddr())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}
