package main

import (
	"fmt"
	"log"
	"net"
	"time"

	"github.com/pion/turn/v2"
)

const (
	realm        = "example.com"
	sharedSecret = "foobar"
)

func main() {
	conn, err := net.ListenPacket("udp4", "0.0.0.0:0")
	if err != nil {
		log.Fatalf("failed to listen packet conn: %+v", err)
	}

	turnAddr := "localhost:3478"
	username, password, err := turn.GenerateLongTermCredentials(sharedSecret, 3*time.Second)
	c, err := turn.NewClient(&turn.ClientConfig{
		STUNServerAddr: turnAddr,
		TURNServerAddr: turnAddr,
		Username:       username,
		Password:       password,
		Realm:          realm,
		Conn:           conn,
	})
	if err != nil {
		log.Fatalf("failed to new client: %+v", err)
	}
	if err := c.Listen(); err != nil {
		log.Fatalf("failed to listen: %+v", err)
	}
	relayConn, err := c.Allocate()
	if err != nil {
		log.Fatalf("failed to allocate: %+v", err)
	}
	log.Printf("relayConn.LocalAddr: %s", relayConn.LocalAddr())
	go func() {
		b := make([]byte, 1024)
		for {
			n, _, err := relayConn.ReadFrom(b)
			if err != nil {
				log.Printf("failed to read relay msg: %+v", err)
				continue
			}
			log.Printf("relay msg receveid: %s", string(b[:n]))
		}
	}()

	ticker := time.NewTicker(3 * time.Second)
	defer ticker.Stop()
	for range ticker.C {
		msg := fmt.Sprintf("%s", time.Now())
		if _, err := relayConn.WriteTo([]byte(msg), relayConn.LocalAddr()); err != nil {
			log.Printf("failed to send relay msg: %+v", err)
		}
	}
}
