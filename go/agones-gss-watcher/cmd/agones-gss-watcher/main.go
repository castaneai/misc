package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"sort"
	"syscall"
	"time"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	"agones.dev/agones/pkg/client/clientset/versioned"
	"github.com/jedib0t/go-pretty/v6/table"
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/duration"
	"k8s.io/client-go/tools/clientcmd"
)

func main() {
	ctx, shutdown := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer shutdown()

	var namespace, fleetName, kubeConfigPath string
	flag.StringVar(&namespace, "namespace", "default", "k8s Namespace to watch")
	flag.StringVar(&fleetName, "fleet", "test-fleet", "Agones fleet name to watch")
	flag.StringVar(&kubeConfigPath, "kubeconfig", defaultKubeConfigPath(), "Agones fleet name to watch")
	flag.Parse()

	client, err := clientcmd.BuildConfigFromFlags("", kubeConfigPath)
	if err != nil {
		log.Fatalf("failed to create k8s client: %+v", err)
	}
	cs := versioned.NewForConfigOrDie(client)

	flt, err := cs.AgonesV1().Fleets(namespace).Get(ctx, fleetName, metav1.GetOptions{})
	if err != nil {
		log.Fatalf("failed to get fleet: %+v", err)
	}
	log.Printf("watching namespace: %s, kubeconfig: %s", namespace, kubeConfigPath)
	if flt.Spec.Strategy.Type == appsv1.RollingUpdateDeploymentStrategyType {
		log.Printf("Fleet(%s) update strategy: %s, maxSurge: %s, maxUnavailable: %s",
			fleetName, flt.Spec.Strategy.Type, flt.Spec.Strategy.RollingUpdate.MaxSurge, flt.Spec.Strategy.RollingUpdate.MaxUnavailable)
	} else {
		log.Printf("Fleet(%s) update strategy: %s", fleetName, flt.Spec.Strategy.Type)
	}

	gssWatch, err := cs.AgonesV1().
		GameServerSets(namespace).
		Watch(ctx, metav1.ListOptions{LabelSelector: labels.Set{agonesv1.FleetNameLabel: fleetName}.String()})
	if err != nil {
		log.Fatalf("failed to watch gameserversets: %+v", err)
	}
	defer gssWatch.Stop()

	summaries := map[string]GameServerSetSummary{}
	for {
		select {
		case <-ctx.Done():
			return
		case w := <-gssWatch.ResultChan():
			if gss, ok := w.Object.(*agonesv1.GameServerSet); ok {
				now := NewGameServerSetSummary(gss)
				old, exists := summaries[gss.Name]
				if !exists || old.HasChanged(&now) {
					summaries[gss.Name] = now
					printSummaries(summaries)
					continue
				}
			}
		}
	}
}

func defaultKubeConfigPath() string {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	return filepath.Join(homeDir, ".kube", "config")
}

func printSummaries(summaries map[string]GameServerSetSummary) {
	t := table.NewWriter()
	t.SetCaption("%s", time.Now())
	t.AppendHeader(table.Row{"GSS", "Desired", "Total", "Ready", "Allocated", "Age"})
	var list []GameServerSetSummary
	for _, s := range summaries {
		list = append(list, s)
	}
	sort.Slice(list, func(i, j int) bool {
		return list[i].CreatedAt.Unix() < list[j].CreatedAt.Unix()
	})
	for _, s := range list {
		t.AppendRow(s.ToRow())
	}
	fmt.Println(t.Render())
	fmt.Println()
}

type GameServerSetSummary struct {
	Name      string
	CreatedAt time.Time
	Desired   int32
	Total     int32
	Ready     int32
	Allocated int32
}

func NewGameServerSetSummary(gss *agonesv1.GameServerSet) GameServerSetSummary {
	return GameServerSetSummary{
		Name:      gss.GetName(),
		CreatedAt: gss.CreationTimestamp.Time,
		Desired:   gss.Spec.Replicas,
		Total:     gss.Status.Replicas,
		Ready:     gss.Status.ReadyReplicas,
		Allocated: gss.Status.AllocatedReplicas,
	}
}

func (s *GameServerSetSummary) HasChanged(other *GameServerSetSummary) bool {
	return s.Name != other.Name ||
		s.Desired != other.Desired ||
		s.Total != other.Total ||
		s.Ready != other.Ready ||
		s.Allocated != other.Allocated
}

func (s *GameServerSetSummary) ToRow() table.Row {
	return table.Row{
		s.Name,
		s.Desired,
		s.Total,
		s.Ready,
		s.Allocated,
		duration.HumanDuration(time.Since(s.CreatedAt)),
	}
}
