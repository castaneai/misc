# Agones GameServerSet Watcher

Watch and print the changes of Agones GameServerSet.

```sh
go run cmd/agones-gss-watcher/main.go -namespace default -fleet test-fleet 
```
