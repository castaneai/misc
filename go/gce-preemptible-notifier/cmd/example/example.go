package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"cloud.google.com/go/compute/metadata"
	gce_preemptible_notifier "github.com/castaneai/gce-preemptible-notifier"
	compute "google.golang.org/api/compute/v0.beta"
)

var slackWebhookURL string
var inmemoryValue = "initial"

func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)

	slackWebhookURL = os.Getenv("SLACK_WEBHOOK_URL")
	if slackWebhookURL == "" {
		log.Fatalf("missing env: SLACK_WEBHOOK_URL")
	}

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	mc := metadata.NewClient(http.DefaultClient)
	project, zone, instance, err := getInstanceInfo(mc)
	if err != nil {
		log.Fatalf("failed to get instance info: %+v", err)
	}

	computeClient, err := compute.NewService(ctx)
	if err != nil {
		log.Fatalf("failed to new compute API: %+v", err)
	}

	n := gce_preemptible_notifier.NewNotifier(http.DefaultClient)
	go func() {
		preempted, err := n.Preempted(ctx, true)
		if err != nil {
			logAndPostSlack(fmt.Sprintf("failed to get preempted status: %+v", err))
			return
		}
		inmemoryValue = fmt.Sprintf("preempted at %v", time.Now())
		go func() {
			ticker := time.NewTicker(1 * time.Second)
			defer ticker.Stop()
			for range ticker.C {
				log.Printf("%s", time.Now())
			}
		}()
		logAndPostSlack(fmt.Sprintf("[project/%s/zone/%s/instance/%s] preempted status has changed to %v", project, zone, instance, preempted))
		suspendCall := computeClient.Instances.Suspend(project, zone, instance)
		if _, err := suspendCall.Context(ctx).Do(); err != nil {
			log.Printf("failed to call suspend: %+v", err)
		}
		logAndPostSlack(fmt.Sprintf("[project/%s/zone/%s/instance/%s] instance suspended.", project, zone, instance))
	}()

	logAndPostSlack(fmt.Sprintf("GCE preemptible notifier start (project/%s/zone/%s/instance/%s)", project, zone, instance))
	ticker := time.NewTicker(10 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			log.Printf("inmemory value: %v", inmemoryValue)
		case <-ctx.Done():
			if ctx.Err() != nil {
				log.Printf("signal received: %+v", ctx.Err())
			}
		}
	}
}

func getInstanceInfo(mc *metadata.Client) (project, zone, instance string, err error) {
	project, err = mc.ProjectID()
	if err != nil {
		return
	}
	zone, err = mc.Zone()
	if err != nil {
		return
	}
	instance, err = mc.InstanceName()
	if err != nil {
		return
	}
	return
}

func logAndPostSlack(msg string) {
	log.Printf(msg)
	if err := gce_preemptible_notifier.PostSlackMessage(slackWebhookURL, &gce_preemptible_notifier.SlackMessage{Text: msg}); err != nil {
		log.Printf("failed to send slack message: %+v", err)
	}
}
