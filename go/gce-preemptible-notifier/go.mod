module github.com/castaneai/gce-preemptible-notifier

go 1.16

require (
	cloud.google.com/go v0.84.0 // indirect
	google.golang.org/api v0.48.0 // indirect
)
