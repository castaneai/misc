package gce_preemptible_notifier

import (
	"cloud.google.com/go/compute/metadata"
	"context"
	"net/http"
)

type Notifier struct {
	client *metadata.Client
}

func NewNotifier(hc *http.Client) *Notifier {
	return &Notifier{
		client: metadata.NewClient(hc),
	}
}

func (n *Notifier) Preempted(ctx context.Context, waitForChange bool) (bool, error) {
	respch := make(chan string)
	errch := make(chan error)
	go func() {
		suffix := "/instance/preempted"
		if waitForChange {
			suffix += "?wait_for_change=true"
		}
		resp, err := n.client.Get(suffix)
		if err != nil {
			errch <- err
			return
		}
		respch <- resp
	}()
	select {
	case <-ctx.Done():
		return false, ctx.Err()
	case resp := <-respch:
		return resp == "TRUE", nil
	}
}
