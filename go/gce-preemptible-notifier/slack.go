package gce_preemptible_notifier

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type SlackMessage struct {
	Text string `json:"text"`
}

func PostSlackMessage(webhookURL string, message *SlackMessage) error {
	buf, err := json.Marshal(message)
	if err != nil {
		return err
	}
	resp, err := http.Post(webhookURL, "application/json", bytes.NewReader(buf))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode >= 400 {
		return fmt.Errorf("unexpected status code(%s) from slack", resp.Status)
	}
	return nil
}
