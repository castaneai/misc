package main

import (
	"github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/castaneai/misc/go/bubbletea/pkg/monitable"
	"log"
	"time"
)

var normalStyle = lipgloss.NewStyle()
var selectedStyle = normalStyle.Background(lipgloss.Color("87")).Foreground(lipgloss.Color("0"))
var addedStyle = normalStyle.Foreground(lipgloss.Color("10"))   // green
var removedStyle = normalStyle.Foreground(lipgloss.Color("55")) // purple

type model struct {
	table monitable.Model
}

type AddRow struct {
	Row monitable.Row
}

type addMsg struct {
	Row monitable.Row
}

type blinkOffMsg struct {
	rowID int
}

func doTick() tea.Cmd {
	return tea.Tick(1*time.Second, func(t time.Time) tea.Msg {
		return tickMsg{}
	})
}

func (m model) Init() tea.Cmd {
	return doTick()
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	m.table, cmd = m.table.Update(msg)

	switch msg := msg.(type) {
	case tickMsg:
		rows := append(m.table.Rows(), monitable.Row{time.Now().String(), "Pending"})
		m.table.SetRows(rows)
		m.table.EnableRowStyle(len(rows)-1, addedStyle)
		return m, doTick()
	case tea.WindowSizeMsg:
		m.table.SetWidth(msg.Width)
		m.table.SetHeight(msg.Height - 1)
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit
		}
	}

	return m, cmd
}

func (m model) View() string {
	return m.table.View()
}

func main() {
	columns := []monitable.Column{
		{Title: "Time", Width: 50},
		{Title: "Status", Width: 50},
	}
	rows := []monitable.Row{
		{time.Now().String(), "Active"},
	}

	styles := monitable.DefaultStyles()
	styles.Selected = selectedStyle
	t := monitable.New(
		monitable.WithColumns(columns),
		monitable.WithRows(rows),
		monitable.WithFocused(true),
		monitable.WithStyles(styles))

	m := model{table: t}
	if _, err := tea.NewProgram(m, tea.WithAltScreen()).Run(); err != nil {
		log.Fatal(err)
	}
}
