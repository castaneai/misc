package wiretest

import (
	"testing"
)

func TestGreeter(t *testing.T) {
	greeter := InitializeGreeter()
	t.Logf("%s", greeter.Greet())
	greeter = InitializeMockGreeter()
	t.Logf("%s", greeter.Greet())
}
