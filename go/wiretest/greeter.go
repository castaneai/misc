package wiretest

type Greeter interface {
	Greet() string
}

type MyGreeter struct{}

func NewMyGreeter() *MyGreeter {
	return &MyGreeter{}
}

func (g *MyGreeter) Greet() string {
	return "MyGreeter"
}

type MockGreeter struct{}

func NewMockGreeter() *MockGreeter {
	return &MockGreeter{}
}

func (g *MockGreeter) Greet() string {
	return "MockGreeter"
}
