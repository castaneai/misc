//go:build wireinject
// +build wireinject

package wiretest

import "github.com/google/wire"

func InitializeGreeter() Greeter {
	wire.Build(wire.NewSet(
		NewMyGreeter,
		wire.Bind(new(Greeter), new(*MyGreeter)),
	))
	return nil
}

func InitializeMockGreeter() Greeter {
	wire.Build(wire.NewSet(
		NewMockGreeter,
		wire.Bind(new(Greeter), new(*MockGreeter)),
	))
	return nil
}
