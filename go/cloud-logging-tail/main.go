package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"

	logging "cloud.google.com/go/logging/apiv2"
	loggingpb "google.golang.org/genproto/googleapis/logging/v2"
)

func main() {
	log.SetFlags(0)

	ctx := context.Background()
	projectID := os.Getenv("GCP_PROJECT_ID")
	if projectID == "" {
		log.Fatalf("missing env: GCP_PROJECT_ID")
	}
	parent := fmt.Sprintf("projects/%s", projectID)

	client, err := logging.NewClient(ctx)
	if err != nil {
		log.Fatalf("failed to create logging client: %+v", err)
	}
	defer client.Close()

	stream, err := client.TailLogEntries(ctx)
	if err := stream.Send(&loggingpb.TailLogEntriesRequest{
		ResourceNames: []string{parent},
	}); err != nil {
		log.Fatalf("failed to request tail log entries: %+v", err)
	}
	defer stream.CloseSend()
	for {
		resp, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			log.Printf("--- EOF ---")
			break
		}
		if err != nil {
			log.Fatalf("failed to recv tail response: %+v", err)
		}
		for _, entry := range resp.Entries {
			entry.GetJsonPayload()
			log.Printf("%s [%s] %v", entry.Timestamp.AsTime(), entry.Severity, formatEntry(entry))
		}
	}
}

func formatEntry(entry *loggingpb.LogEntry) string {
	isHttpRequest := entry.HttpRequest != nil
	if isHttpRequest {
		req := entry.HttpRequest
		return fmt.Sprintf("HTTP %d %s %s (latency: %v)", req.Status, req.RequestMethod, req.RequestUrl, req.Latency)
	}
	return formatEntryPayload(entry.Payload)
}

func formatEntryPayload(payload interface{}) string {
	switch p := payload.(type) {
	case *loggingpb.LogEntry_JsonPayload:
		b, _ := p.JsonPayload.MarshalJSON()
		return fmt.Sprintf("%s", string(b))
	case *loggingpb.LogEntry_TextPayload:
		return p.TextPayload
	case *loggingpb.LogEntry_ProtoPayload:
		return p.ProtoPayload.String()
	}
	return fmt.Sprintf("<unknown type: %T>", payload)
}
