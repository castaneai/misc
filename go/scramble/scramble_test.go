package scramble

import (
	"image"
	_ "image/jpeg"
	_ "image/png"
	"os"
	"testing"
)

func TestScramble(t *testing.T) {
	f, err := os.Open("testdata/example.jpeg")
	if err != nil {
		t.Fatalf("failed to open example image file: %+v", err)
	}
	defer f.Close()

	img, format, err := image.Decode(f)
	if err != nil {
		t.Fatalf("failed to decode image: %+v", err)
	}
	t.Logf("img: %T (format: %s)", img, format)
}
