use crate::types::{ClientId, ClientToMaster, ClientToRoom, RoomId, RoomToClient};
use std::collections::HashMap;
use tokio::sync::mpsc;

type RoomMap = HashMap<RoomId, RoomProxy>;
type ClientMap = HashMap<ClientId, ClientProxy>;

#[derive(Debug)]
struct ClientProxy {
    sender_to_client: mpsc::Sender<RoomToClient>,
}

#[derive(Debug)]
struct RoomProxy {
    master_to_room: mpsc::Sender<MasterToRoom>,
}

#[derive(Debug)]
enum MasterToRoom {
    Join {
        client_id: ClientId,
        sender_to_client: mpsc::Sender<RoomToClient>,
    },
    Leave {
        client_id: ClientId,
    },
}

pub(crate) async fn master_task(mut client_to_master: mpsc::Receiver<ClientToMaster>) {
    let mut rooms = RoomMap::new();
    loop {
        tokio::select! {
            Some(msg) = client_to_master.recv() => {
                handle_client_to_master(msg, &mut rooms).await;
            }
            else => break
        }
    }
}

async fn handle_client_to_master(msg: ClientToMaster, rooms: &mut RoomMap) {
    match msg {
        ClientToMaster::Join {
            room_id,
            client_id,
            sender_to_client,
        } => {
            if !rooms.contains_key(room_id.as_str()) {
                // spawn new room
                let (master_to_room, receiver_from_master) = mpsc::channel(8);
                let (client_to_room, receiver_from_client) = mpsc::channel(8);
                let room_proxy = RoomProxy { master_to_room };
                let room = Room::new(room_id.clone(), client_to_room.clone());
                tokio::spawn(room_task(room, receiver_from_master, receiver_from_client));
                rooms.insert(room_id.clone(), room_proxy);
            }
            let room = rooms.get(room_id.as_str()).unwrap();
            room.master_to_room
                .send(MasterToRoom::Join {
                    client_id,
                    sender_to_client,
                })
                .await
                .unwrap();
        }
        ClientToMaster::Leave { room_id, client_id } => {
            if let Some(room) = rooms.get(room_id.as_str()) {
                room.master_to_room
                    .send(MasterToRoom::Leave { client_id })
                    .await
                    .unwrap();
            }
        }
    }
}

async fn room_task(
    mut room: Room,
    mut receiver_from_master: mpsc::Receiver<MasterToRoom>,
    mut receiver_from_client: mpsc::Receiver<ClientToRoom>,
) {
    // TODO: subscribe room topic
    loop {
        tokio::select! {
            Some(msg) = receiver_from_master.recv() => {
                room.handle_master_to_room(msg).await;
            }
            Some(msg) = receiver_from_client.recv() => {
                room.handle_client_to_room(msg).await;
            }
            // TODO: from pub/sub
            else => break
        }
    }
}

#[derive(Debug)]
struct Room {
    room_id: RoomId,
    clients: ClientMap,
    client_to_room: mpsc::Sender<ClientToRoom>,
}

impl Room {
    fn new(room_id: RoomId, client_to_room: mpsc::Sender<ClientToRoom>) -> Self {
        Self {
            room_id,
            clients: ClientMap::new(),
            client_to_room,
        }
    }

    async fn handle_master_to_room(&mut self, msg: MasterToRoom) {
        match msg {
            MasterToRoom::Join {
                client_id,
                sender_to_client,
            } => {
                let client = ClientProxy { sender_to_client };
                client
                    .sender_to_client
                    .send(RoomToClient::JoinResponse {
                        room_id: self.room_id.clone(),
                        sender_to_room: self.client_to_room.clone(),
                    })
                    .await
                    .unwrap();
                self.clients.insert(client_id.clone(), client);
                println!("[{}] client({}) joined room", self.room_id, client_id);
            }
            MasterToRoom::Leave { client_id } => {
                self.clients.remove(client_id.as_str());
                println!("[{}] client({}) left room", self.room_id, client_id);
            }
        }
    }

    async fn handle_client_to_room(&self, msg: ClientToRoom) {
        match msg {
            ClientToRoom::Message { payload } => {
                println!("[{}] broadcast message: {:?}", self.room_id, payload);
                for client in self.clients.values() {
                    // TODO: parallel send
                    client
                        .sender_to_client
                        .send(RoomToClient::Message {
                            payload: payload.clone(),
                        })
                        .await
                        .unwrap();
                }
            }
        }
    }
}
