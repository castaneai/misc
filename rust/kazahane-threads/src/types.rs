use tokio::sync::mpsc;

pub(crate) type ClientId = String;
pub(crate) type RoomId = String;

#[derive(Debug)]
pub(crate) enum ClientToMaster {
    Join {
        room_id: RoomId,
        client_id: ClientId,
        sender_to_client: mpsc::Sender<RoomToClient>,
    },
    Leave {
        room_id: RoomId,
        client_id: ClientId,
    },
}

#[derive(Debug)]
pub(crate) enum ClientToRoom {
    Message { payload: String },
}

#[derive(Debug)]
pub(crate) enum RoomToClient {
    JoinResponse {
        room_id: RoomId,
        sender_to_room: mpsc::Sender<ClientToRoom>,
    },
    Message {
        payload: String,
    },
}
