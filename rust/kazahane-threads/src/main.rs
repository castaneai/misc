mod server;
mod types;

use crate::server::master_task;
use crate::types::{ClientId, ClientToMaster, ClientToRoom, RoomId, RoomToClient};
use anyhow::{bail, Context};
use std::time::Duration;
use tokio::sync::mpsc;

type Result<T> = anyhow::Result<T>;

#[tokio::main]
async fn main() {
    let (sender_to_master, receiver_from_client) = mpsc::channel(8);
    tokio::spawn(master_task(receiver_from_client));

    // echo
    let mut c1 = Client::new("c1", sender_to_master.clone());
    c1.join_room("room1").await.unwrap();
    c1.send_message("hello").await.unwrap();
    let resp = c1.recv_message().await.unwrap();
    assert_eq!(resp, "hello");

    // broadcast to room
    let mut c2 = Client::new("c2", sender_to_master.clone());
    c2.join_room("room1").await.unwrap();
    c2.send_message("hello2").await.unwrap();
    assert_eq!(c1.recv_message().await.unwrap(), "hello2");
    assert_eq!(c2.recv_message().await.unwrap(), "hello2");

    // leave
    c1.leave_room("room1").await.unwrap();
    tokio::time::sleep(Duration::from_millis(1000)).await;
}

enum ClientSender {
    Connected(mpsc::Sender<ClientToRoom>),
    Disconnected,
}

struct Client {
    client_id: ClientId,
    sender_to_master: mpsc::Sender<ClientToMaster>,
    sender_to_room: ClientSender,
    sender_to_client: mpsc::Sender<RoomToClient>,
    receiver: mpsc::Receiver<RoomToClient>,
}

impl Client {
    fn new(client_id: impl Into<ClientId>, sender_to_master: mpsc::Sender<ClientToMaster>) -> Self {
        let (sender_to_client, receiver) = mpsc::channel(8);
        Self {
            client_id: client_id.into(),
            sender_to_master,
            sender_to_room: ClientSender::Disconnected,
            sender_to_client,
            receiver,
        }
    }

    async fn join_room(&mut self, room_id: impl Into<RoomId>) -> Result<()> {
        self.sender_to_master
            .send(ClientToMaster::Join {
                client_id: self.client_id.clone(),
                room_id: room_id.into(),
                sender_to_client: self.sender_to_client.clone(),
            })
            .await
            .context("failed to send join")?;
        match self.receiver.recv().await {
            Some(RoomToClient::JoinResponse {
                room_id,
                sender_to_room,
            }) => {
                self.sender_to_room = ClientSender::Connected(sender_to_room);
                Ok(())
            }
            Some(msg) => {
                bail!("JoinResponse expected, but got {:?}", msg)
            }
            None => {
                bail!("disconnected while waiting for JoinResponse")
            }
        }
    }

    async fn leave_room(&mut self, room_id: impl Into<RoomId>) -> Result<()> {
        self.sender_to_master
            .send(ClientToMaster::Leave {
                client_id: self.client_id.clone(),
                room_id: room_id.into(),
            })
            .await
            .context("failed to send join")
    }

    async fn send_message(&self, payload: impl Into<String>) -> Result<()> {
        match &self.sender_to_room {
            ClientSender::Connected(sender) => sender
                .send(ClientToRoom::Message {
                    payload: payload.into(),
                })
                .await
                .context("failed to send"),
            ClientSender::Disconnected => bail!("not connected"),
        }
    }

    async fn recv_message(&mut self) -> Result<String> {
        match self.receiver.recv().await {
            Some(RoomToClient::Message { payload }) => Ok(payload),
            Some(msg) => {
                bail!("Message expected, but got {:?}", msg)
            }
            None => {
                bail!("disconnected while waiting for Message")
            }
        }
    }
}
