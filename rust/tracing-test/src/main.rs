use axum::Router;
use std::net::SocketAddr;
use tracing::{debug, info, info_span};
use tracing_stackdriver::Stackdriver;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_subscriber::EnvFilter;

#[derive(Debug)]
struct Test {
    name: String,
}

#[tokio::main]
async fn main() {
    init_tracing();

    let app = Router::new().route("/", axum::routing::get(root));

    let test = Test {
        name: "hello".to_string(),
    };
    {
        let _span = info_span!("test-span", test = "abc").entered();
        info!(?test);
    }
    debug!("out of span");
    test_fn();

    static DEFAULT_PORT: u16 = 8080;
    let port =
        std::env::var("PORT").map_or(DEFAULT_PORT, |v| v.parse::<u16>().unwrap_or(DEFAULT_PORT));
    let addr = SocketAddr::from(([0, 0, 0, 0], port));
    info!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[tracing::instrument]
fn test_fn() {
    info!("test_fn");
}

async fn root() -> &'static str {
    debug!("receive request");
    "Hello, world"
}

fn init_tracing() {
    let filter = EnvFilter::new(std::env::var("RUST_LOG").unwrap_or_else(|_| "debug".into()));
    //let fmt = tracing_subscriber::fmt::layer();
    let stackdriver = Stackdriver::default();
    tracing_subscriber::registry()
        .with(stackdriver)
        // .with(fmt)
        .with(filter)
        .init();
}
