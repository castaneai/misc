use std::time::Duration;

// #[tokio::main]
#[tokio::main(flavor = "multi_thread", worker_threads = 2)]
async fn main() {

    let handle = tokio::runtime::Handle::current();
    {
        let runtime_monitor = tokio_metrics::RuntimeMonitor::new(&handle);
        tokio::spawn(async move {
            for metrics in runtime_monitor.intervals() {
                println!("busy: total:{:?}, min: {:?}, max: {:?}", metrics.total_busy_duration, metrics.min_busy_duration, metrics.max_busy_duration);
                tokio::time::sleep(Duration::from_millis(1000)).await;
            }
        });
    }

    for i in 0..3 {
        tokio::spawn(async move {
            println!("start non-blocking task: {}", i);
            tokio::time::sleep(Duration::from_secs(2)).await;
            println!("finish non-blocking task: {}", i);
        });
    }

    tokio::spawn(async {
        println!("start blocking task");
        spin_for(Duration::from_secs(1));
        println!("finish blocking task");
    });

    tokio::time::sleep(Duration::from_secs(5)).await;
}

/// Block the current thread for a given `duration`.
fn spin_for(duration: Duration) {
    let start = tokio::time::Instant::now();
    while start.elapsed() <= duration {}
}
