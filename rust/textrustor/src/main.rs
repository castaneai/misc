use std::ffi::c_void;
use std::io::Read;
use std::mem::size_of;
use std::ptr::null;
use std::time::Duration;
use injrs::process_windows::*;
use injrs::inject_windows::*;
use mio::{Events, Interest, Poll, Token};
use mio::windows::NamedPipe;
use windows::Win32::Foundation::{BOOL, PWSTR};
use windows::Win32::Security::{InitializeSecurityDescriptor, SECURITY_ATTRIBUTES, SECURITY_DESCRIPTOR, SetSecurityDescriptorDacl};
use windows::Win32::System::Threading::*;
use pretty_hex::*;

const HOOK_PIPE: &str = r"\\.\pipe\TEXTRACTOR_HOOK";
const HOST_PIPE: &str = r"\\.\pipe\TEXTRACTOR_HOST";
const PIPE_AVAILABLE_EVENT: &str = "TEXTRACTOR_PIPE_AVAILABLE";

fn main() {
    let name = "microkiri.exe";
    let dll = r"C:\Users\castaneai\Documents\Textractor\texthook\cmake-build-debug\texthook.dll";
    let process = Process::find_first_by_name(name).unwrap();
    process.inject(dll).unwrap();

    let mut hook_pipe = NamedPipe::new(HOOK_PIPE).unwrap();
    let mut host_pipe = NamedPipe::new(HOST_PIPE).unwrap();
    let mut poll = Poll::new().unwrap();
    poll.registry().register(&mut hook_pipe, Token(0), Interest::READABLE).unwrap();
    poll.registry().register(&mut host_pipe, Token(1), Interest::WRITABLE).unwrap();

    unsafe {
        let mut security_desc = SECURITY_DESCRIPTOR::default();
        InitializeSecurityDescriptor(&mut security_desc, 1);
        SetSecurityDescriptorDacl(&mut security_desc, true, null(), false);
        let all_access = SECURITY_ATTRIBUTES {
            nLength: size_of::<SECURITY_ATTRIBUTES>() as u32,
            lpSecurityDescriptor: &mut security_desc as *mut _ as *mut c_void,
            bInheritHandle: BOOL(0),
        };
        let event_name = PWSTR(PIPE_AVAILABLE_EVENT.encode_utf16().chain([0u16]).collect::<Vec<u16>>().as_mut_ptr());
        let pipe_available_event = CreateEventW(&all_access, false, false, event_name);
        SetEvent(pipe_available_event);
    }

    let mut events = Events::with_capacity(128);
    loop {
        poll.poll(&mut events, Some(Duration::new(1, 0))).unwrap();
        if let Ok(()) = hook_pipe.connect() {
            break
        }
    }
    loop {
        poll.poll(&mut events, None).unwrap();

        for event in events.iter() {
            if event.is_readable() && event.token() == Token(0) {
                let mut buf = [0; 50000];
                let size = hook_pipe.read(&mut buf).unwrap();
                let received = &buf[..size];
                println!("received: {:?}", received.hex_dump());
            }
        }
    }
}