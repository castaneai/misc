use tokio::net::TcpListener;
use tokio_util::codec::{FramedRead, LengthDelimitedCodec};

#[tokio::test]
async fn read_frame() {
    let listener = TcpListener::bind("127.0.0.1:0").await.unwrap();
    let listen_addr = listener.local_addr().unwrap();
    println!("listening on: {}", listen_addr);
    tokio::spawn(async move {
        loop {
            let (mut client, _) = listener.accept().await.unwrap();
            let frame_reader = FramedRead::new(client, LengthDelimitedCodec::new());
        }
    });
    assert!(true);
}