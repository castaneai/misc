use gst::prelude::*;

fn main() {
    gst::init().unwrap();
    println!("gstreamer version: {}", gst::version_string());

    let pipeline = gst::Pipeline::new(None);
    let src = gst::ElementFactory::make("filesrc", None).unwrap();
    src.set_property("location", "test.mpg").unwrap();
    let decodebin = gst::ElementFactory::make("decodebin", None).unwrap();

    pipeline.add_many(&[&src, &decodebin]).unwrap();
    gst::Element::link_many(&[&src, &decodebin]).unwrap();
}