# quinn-example

A minimal QUIC client-server example with [Connection Migration](https://tools.ietf.org/id/draft-tan-quic-connection-migration-00.html).

```sh
# run server
cargo run --example server

# run client
cargo run --example client
```