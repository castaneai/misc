use std::fs;
use rustls::{Certificate, PrivateKey};
use tokio_stream::StreamExt;

#[tokio::main]
async fn main() {
    let (cert, key) = get_certs();
    let server_config = quinn::ServerConfig::with_single_cert(cert, key)
        .expect("failed to build server config");
    let listen_addr = "127.0.0.1:5000".parse().unwrap();
    let (endpoint, mut incoming) = quinn::Endpoint::server(server_config, listen_addr)
        .expect("failed to create server endpoint");
    eprintln!("listening on {}", endpoint.local_addr().unwrap());

    while let Some(conn) = incoming.next().await {
        tokio::spawn(async move {
            handle_connection(conn).await;
        });
    }
}

async fn handle_connection(conn: quinn::Connecting) {
    eprintln!("connection incoming from {}", conn.remote_address());
    let quinn::NewConnection {
        connection: conn,
        mut datagrams,
        .. } = conn.await.expect("failed to handshake conn");
    eprintln!("connection connected stable_id={}", conn.stable_id());
    while let Some(d) = datagrams.next().await {
        match d {
            Ok(payload) => eprintln!("received from (id: {}, addr: {}): {:?}",
                                     conn.stable_id(), conn.remote_address(), payload),
            Err(quinn::ConnectionError::ApplicationClosed(err)) => {
                eprintln!("closed by client(id: {}) (reason: {:?})", conn.stable_id(), err.reason);
                break;
            }
            Err(err) => {
                eprintln!("failed to read datagram: {:?}", err);
                break;
            }
        }
    }
}

fn get_certs() -> (Vec<Certificate>, PrivateKey) {
    let cert = fs::read("testdata/cert.der").expect("failed to read cert file");
    let key = fs::read("testdata/key.der").expect("failed to read key file");
    (vec![rustls::Certificate(cert)], rustls::PrivateKey(key))
}