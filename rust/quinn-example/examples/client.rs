use std::fs;
use std::net::UdpSocket;
use std::time::Duration;
use rustls::Certificate;
use tokio::time::sleep;

#[tokio::main]
async fn main() {
    let mut cert_store = rustls::RootCertStore::empty();
    cert_store.add(&get_cert())
        .expect("failed to add cert to store");
    let client_config = quinn::ClientConfig::with_root_certificates(cert_store);

    let local_addr = "0.0.0.0:0".parse().unwrap();
    let mut endpoint = quinn::Endpoint::client(local_addr)
        .expect("failed to build client");
    endpoint.set_default_client_config(client_config);
    let server_addr = "127.0.0.1:5000".parse().unwrap();
    let quinn::NewConnection {connection: conn, .. } = endpoint.connect(server_addr, "localhost")
        .expect("failed to connect")
        .await
        .expect("failed to connect");
    eprintln!("connected to {}", server_addr);

    for i in 0..3 {
        let payload = format!("hello-{}", i);
        eprintln!("sent: {}", payload);
        conn.send_datagram(payload.into())
            .expect("failed to send diagram");
        sleep(Duration::from_secs(1)).await;
    }

    // connection migration
    let new_bind = UdpSocket::bind("0.0.0.0:0").unwrap();
    endpoint.rebind(new_bind).expect("failed to rebind");

    for i in 3..6 {
        let payload = format!("hello-{}", i);
        eprintln!("sent: {}", payload);
        conn.send_datagram(payload.into())
            .expect("failed to send diagram");
        sleep(Duration::from_secs(1)).await;
    }

    conn.close(0u32.into(), b"done");
    endpoint.wait_idle().await;
}

fn get_cert() -> Certificate {
    let cert = fs::read("testdata/cert.der").expect("failed to read cert file");
    rustls::Certificate(cert)
}