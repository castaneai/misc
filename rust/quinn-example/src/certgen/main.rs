use std::fs;

fn main() {
    let subject_alt_names = vec!["localhost".to_string()];
    let cert = rcgen::generate_simple_self_signed(subject_alt_names).unwrap();
    fs::write("testdata/cert.der", cert.serialize_der().unwrap()).expect("failed to write cert");
    fs::write("testdata/key.der", cert.serialize_private_key_der()).expect("failed to write key");
}