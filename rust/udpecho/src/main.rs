use std::env;
use std::io::Write;
use std::net::{TcpListener, UdpSocket};
use std::process::exit;
use std::thread::spawn;

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        eprintln!("Usage: ./main <bind_addr> <server_name>");
        exit(2);
    }
    let bind_addr = args[1].as_str();
    let server_name = args[2].as_str();
    eprintln!("hello, I'm {}", server_name);

    spawn(|| {
        const RESPONSE: &'static [u8] = b"HTTP/1.1 200 OK\r
Content-Type: text/html; charset=UTF-8\r\n\r
<!DOCTYPE html><html><body>OK</body></html>\r";
        let listener = TcpListener::bind("0.0.0.0:20101").unwrap();
        eprintln!("TCP health checker listening on :20101...");
        loop {
            let (mut stream, _) = listener.accept().unwrap();
            stream.write_all(RESPONSE).unwrap();
            drop(stream);
        }
    });

    let socket = UdpSocket::bind(bind_addr)?;
    eprintln!("udp socket bound on {}...", bind_addr);
    let mut buf = [0; 10];
    loop {
        let (amt, src) = socket.recv_from(&mut buf)?;
        let msg = String::from_utf8(buf[..amt].to_vec()).unwrap();
        eprintln!("received '{}' from {:?}", msg, src);
        let reply = format!("{} from {}\n", msg, server_name);
        socket.send_to(reply.as_bytes(), &src)?;
    }
}
