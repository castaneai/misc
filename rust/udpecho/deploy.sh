#!/bin/bash
for INSTANCE in castaneai-test-01 castaneai-test-02; do
  gcloud compute ssh --zone asia-northeast1-b "${INSTANCE}" -- sudo pkill udpecho
  gcloud compute scp --zone asia-northeast1-b ./target/x86_64-unknown-linux-gnu/release/udpecho "${INSTANCE}:/tmp"
done