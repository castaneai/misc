use com::{class, interfaces, interfaces::iunknown::IUnknown, sys::HRESULT, IID};
use com::sys::NOERROR;
use std::ffi::c_void;

interfaces! {
    #[uuid("9397f121-7705-4dc9-b049-98b698188414")]
    pub unsafe interface IWMSyncReader: IUnknown {
        fn OpenStream(&self, pStream: *const c_void) -> HRESULT;
    }
}

class! {
    pub class WMSyncReader: IWMSyncReader {}
    impl IWMSyncReader for WMSyncReader {
       fn OpenStream(&self, pStream: *const c_void) -> HRESULT {
            println!("------- open stream");
            NOERROR
        }
    }
}