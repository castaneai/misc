#![allow(non_snake_case)]

mod IWMSyncReader;

use com::sys::{HRESULT, NOERROR};
use std::ffi::c_void;
use com::runtime::{create_instance};
use crate::IWMSyncReader::IID_IWMSYNC_READER;
use com::interfaces::IUnknown;
use com::Interface;

// workaround for cross-compile on linux x86_64
// https://github.com/rust-lang/rust/issues/79609#issuecomment-815456186
#[no_mangle]
fn _Unwind_Resume() {}
#[no_mangle]
fn _Unwind_RaiseException() {}

#[no_mangle]
pub extern "system" fn DllMain(_: u32, reason: u32) {
    if reason == 1 /* DLL_PROCESS_ATTACH */ {
        println!("wmvcore attached");
    }
}

#[no_mangle]
pub extern "C" fn DllRegisterServer() {
    println!("register server")
}

#[no_mangle]
pub extern "C" fn WMCreateSyncReader(
    pUnkCert: *const IUnknown,
    dwRights: u32,
    pIWMSyncReader: *mut *mut c_void
) -> HRESULT {
    println!("wm create sync reader!");
    let sync_reader = create_instance::<IWMSyncReader::IWMSyncReader>(&IID_IWMSYNC_READER)
        .expect("failed to create IWMSyncReader");
    /*
    unsafe {
        *pIWMSyncReader = sync_reader.as_raw().as_ptr() as _;
    }
     */
    NOERROR
}