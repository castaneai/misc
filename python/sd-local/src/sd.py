from typing import List
import torch
import transformers
import diffusers

# supress logs
diffusers.logging.disable_default_handler()
transformers.logging.set_verbosity_error()


def get_vram_usage():
    total = torch.cuda.get_device_properties(0).total_memory
    allocated = torch.cuda.max_memory_allocated(0)
    reserved = torch.cuda.max_memory_reserved(0)
    usage = torch.cuda.memory_usage()
    print(f"usage: {usage}, allocated: {allocated / 1024**3 :.2f}GB, reserved: {reserved / 1024**3 :.2f}GB, total: {total / 1024**3 :.2f}GB")


device = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"
dtype = dtype_vae = torch.float16
print(f"device: {device}, dtype: {dtype}")


def create_pipe():
    vae = diffusers.AutoencoderKL.from_pretrained("models/vae/kl-f8-anime2", torch_dtype=dtype_vae)
    pipe = diffusers.StableDiffusionPipeline.from_pretrained(
        "models/7th_anime_v3_B",
        torch_dtype=dtype,
        safety_checker=None,
        vae=vae,
    )
    # DPM-Solver++
    # https://huggingface.co/docs/diffusers/api/schedulers/multistep_dpm_solver
    pipe.scheduler = diffusers.schedulers.DPMSolverMultistepScheduler.from_config(pipe.scheduler.config)
    pipe = pipe.to(device)
    if device == "cuda":
        pipe.enable_xformers_memory_efficient_attention()
    if device == "mps":
        pipe.enable_attention_slicing()
    return pipe


def generate_image(prompts: List[str], width: int, height: int, steps: int):
    pipe = create_pipe()
    # https://huggingface.co/docs/diffusers/optimization/mps#inference-pipeline
    if device == "mps":
        _ = pipe(prompts, num_inference_steps=1)

    with torch.inference_mode():
        return pipe(
            prompts,
            # https://huggingface.co/syaimu/7th_Layer/blob/main/README.md
            negative_prompt=["(worst quality:1.4), (low quality:1.4), (monochrome:1.1)"] * len(prompts),
            width=width,
            height=height,
            num_inference_steps=steps,
        ).images
