import tempfile
import threading
from fastapi import FastAPI, HTTPException
from starlette.responses import FileResponse
from src import sd

app = FastAPI()
sem = threading.BoundedSemaphore(1)


@app.get("/generate")
def generate(prompt: str, steps: int = 3, width: int = 512, height: int = 768):
    if not sem.acquire(timeout=10):
        raise HTTPException(status_code=503, detail="service unavailable")
    try:
        with tempfile.NamedTemporaryFile(suffix=".png", delete=False) as f:
            img = sd.generate_image([prompt], steps=steps, width=width, height=height)[0]
            img.save(f.name)
            return FileResponse(f.name, media_type="image/png")
    finally:
        sem.release()
