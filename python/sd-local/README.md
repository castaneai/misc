# sd-local

A simple example of SD(Stable Diffusion) with fastapi.

## Run server

```sh
poetry shell
uvicorn src.web:app --reload
```

## Converting model (safetensors to diffusers)

```sh
poetry shell
python3 scripts/convert_model.py --checkpoint_path /path/to/foobar.safetensors --from_safetensors --dump_path ./models/foobar
```
