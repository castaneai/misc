import sqlite3
import sys
import tempfile


class Clip:
    def __init__(self, clipfile: str):
        with open(clipfile, "rb") as f:
            data = f.read()
            head = data.find(b"SQLite format 3")
            if head == -1:
                raise Exception(f"invalid clip format: {clipfile}")
        self._tf = tempfile.NamedTemporaryFile()
        self._tf.write(data[head:])
        self._tf.seek(0)
        self._db = sqlite3.connect(self._tf.name)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._db.__exit__(exc_type, exc_val, exc_tb)
        self._tf.__exit__(exc_type, exc_val, exc_tb)

    def has_timelapse(self):
        if self._db.execute("select count(*) from sqlite_master where name = 'TimeLapseRecord'").fetchone()[0] != 1:
            return False
        return self._db.execute("select count(*) from TimeLapseRecord").fetchone()[0] > 0


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: ./check_timelapse.py <CLIP_FILES...>", file=sys.stderr)
        exit(2)
    files = sys.argv[1:]
    for file in files:
        with Clip(file) as clip:
            print(f"{file}: {clip.has_timelapse()}")
