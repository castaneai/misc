import os.path
import sys

if len(sys.argv) != 2:
    print("usage: ./clip.py <CLIP_FILE>", file=sys.stderr)
    exit(2)

clipfile = sys.argv[1]
name = os.path.basename(clipfile)

with open(clipfile, "rb") as f:
    data = f.read()
    head = data.find(b"SQLite format 3")
with open(f"{name}.sqlite3", "wb") as dbf:
    dbf.write(data[head:])
