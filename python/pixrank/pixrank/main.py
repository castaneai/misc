from collections import Counter
from retry import retry
import requests.exceptions
from wordcloud import WordCloud
import cloudscraper
import ratelimit


@ratelimit.sleep_and_retry
@ratelimit.limits(calls=1, period=1)
@retry(exceptions=(requests.exceptions.JSONDecodeError,), tries=3, delay=1)
def get_ranking(mode: str, page: int = 1) -> list[dict]:
    scraper = cloudscraper.create_scraper()
    url = f"https://www.pixiv.net/ranking.php?mode={mode}&p={page}&format=json"
    result = scraper.get(url).json()
    illusts = []
    for d in result['contents']:
        illusts.append(d)
    return illusts


def get_rankings(mode: str, page_range: range) -> list[dict]:
    illusts = []
    for page in page_range:
        illusts += get_ranking(mode, page)
    return illusts


if __name__ == '__main__':
    tags_counter = Counter()
    illusts = get_rankings(mode='rookie', page_range=range(1, 6+1))
    for i in illusts:
        for tag in i["tags"]:
            tags_counter[tag] += 1
    for cnt in [c for c in tags_counter.most_common() if c[1] > 1]:
        print(cnt)

    wc = WordCloud(font_path="fonts/NotoSansCJKjp-Regular.otf", width=1280, height=720, background_color="white")
    wc.generate_from_frequencies(tags_counter)
    wc.to_file("ranking.png")
