import os
import requests

session_id = os.getenv("PIXIV_SESSION_ID", None)
if session_id is None:
    raise Exception("missing env: PIXIV_SESSION_ID")
headers = {
    "accept": "application/json",
    "cookie": f"PHPSESSID={session_id}",
    "referer": "https://www.pixiv.net/dashboard/works",
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
}
data = requests.get("https://www.pixiv.net/ajax/dashboard/works/illust/request_strategy?lang=ja", headers=headers).json()
if data["error"]:
    error_body = data["body"]
    raise Exception(f"pixiv error: {error_body}")

works = []
for work, thumb in zip(data["body"]["data"]["works"], data["body"]["thumbnails"]["illust"]):
    assert work["workId"] == thumb["id"]
    work_id = work["workId"]
    title = thumb["title"]
    works.append({
        "workId": work["workId"],
        "createDate": work["createDate"],
        "title": thumb["title"],
        "viewCount": work["viewCount"],
        "bookmarkCount": work["bookmarkCount"],
        "bookmarkRatio": work["bookmarkCount"] / work["viewCount"],
        "url": f"https://www.pixiv.net/artworks/{work['workId']}"
    })

print("\t".join(["createDate", "title", "bookmarkCount", "viewCount", "url"]))
for w in sorted(works, key=lambda x: x["bookmarkRatio"], reverse=True):
    cols = [str(c) for c in (w["createDate"], w["title"], w["bookmarkCount"], w["viewCount"], w["url"])]
    print("\t".join(cols))
