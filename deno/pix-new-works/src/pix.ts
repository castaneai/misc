export type UserProfile = {
    illustIds: string[]
}

export type Illust = {
    id: string
    title: string
    url: string
}

type pixResponse = {
    error: boolean
    message?: string
    body: any
}

export class Pix {
    constructor(private sessionId: string|undefined) {}

    async getUserProfile(userId: string): Promise<UserProfile> {
        const resp = await this.getJSON(`https://www.pixiv.net/ajax/user/${userId}/profile/all?lang=ja`)
        const illustIds = Object.keys(resp['illusts'])
        return { illustIds }
    }

    async getIllusts(userId: string, ... illustIds: string[]): Promise<Illust[]> {
        const params = new URLSearchParams('work_category=illust&is_first_page=1&lang=ja')
        for (const id of illustIds) {
            params.append('ids[]', id)
        }
        const resp = await this.getJSON(`https://www.pixiv.net/ajax/user/${userId}/profile/illusts?${params}`)
        return Object.values(resp['works'])
    }

    private async getJSON(url: string): Promise<any> {
        const resp = await fetch(url, {headers: this.createHeaders()})
        const payload: pixResponse = await resp.json()
        if (payload.error) {
            throw new Error(payload.message)
        }
        return payload.body
    }

    private createHeaders(): HeadersInit {
        const headers: HeadersInit = {
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:126.0) Gecko/20100101 Firefox/126.0',
        }
        if (this.sessionId) {
            headers['cookie'] = `PHPSESSID=${this.sessionId}`
        }
        return headers
    }
}
