# favolog

Logging X posts you liked

```
npm install
npm run dev
```

```
npm run deploy
```

## Initialize

```sh
pnpm wrangler d1 execute favolog --remote --file=./schema.sql
```

## Usage

```sh
curl "http://<WORKER>/post/<USER_SCREEN_NAME>" -X POST -H 'content-type: application/json' -d '{"text": "test", "author": "MY_SCREEN_NAME", "url": "https://x.com/xxx/status/yyy"}'
```
