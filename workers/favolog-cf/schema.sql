DROP TABLE IF EXISTS favolog;
CREATE TABLE IF NOT EXISTS favolog (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user TEXT NOT NULL,
    tweet_id TEXT NOT NULL UNIQUE,
    author TEXT NOT NULL,
    private INTEGER NOT NULL,
    `text` TEXT NOT NULL,
    url TEXT NOT NULL,
    favorited_at INTEGER NOT NULL,
    posted_at INTEGER NOT NULL
);
CREATE INDEX favolog_user_private on favolog(user, private);
CREATE INDEX favolog_author on favolog(author);
CREATE INDEX favolog_favorited_at on favolog(favorited_at);
CREATE INDEX favolog_posted_at on favolog(posted_at);

DROP TABLE IF EXISTS favolog_user_profile;
CREATE TABLE IF NOT EXISTS favolog_user_profile (
    screen_name TEXT PRIMARY KEY,
    profile_image_url TEXT NOT NULL
);

DROP TABLE IF EXISTS favolog_media;
CREATE TABLE IF NOT EXISTS favolog_media (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    author TEXT NOT NULL,
    tweet_id TEXT NOT NULL,
    media_type TEXT NOT NULL,
    media_url TEXT NOT NULL,
    media_thumbnail_url TEXT NOT NULL,
    created_at INTEGER NOT NULL
);
CREATE INDEX favolog_media_author on favolog_media(author);
CREATE INDEX favolog_media_tweet_id on favolog_media(tweet_id);
CREATE INDEX favolog_media_created_at on favolog_media(created_at);

