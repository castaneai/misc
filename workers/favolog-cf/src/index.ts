import { zValidator } from '@hono/zod-validator'
import { Hono } from 'hono'
import { z } from 'zod'

type Bindings = {
  DB: D1Database
}

const bodySchema = z.object({
  text: z.string(),
  author: z.string(),
  url: z.string().url(),
})

const app = new Hono<{ Bindings: Bindings }>()

app.post('/post/:user', zValidator('json', bodySchema), async (c) => {
  const user = c.req.param('user')
  const body = c.req.valid('json')
  console.log(user, body)
  const tweetId = getTweetIdFromUrl(body.url)
  if (!tweetId) {
    return c.json({error: 'invalid tweet id'}, 500)
  }

  try {
    // insert tweet
    const tweet = await fetchTweet(tweetId)
    console.log('fetched', body.url, tweet.__typename)
    const is_private = tweet.__typename === 'TweetTombstone' ? 1 : 0
    const favorited_at = new Date()
    const posted_at = tweet.__typename === 'Tweet' ? new Date(tweet.created_at) : favorited_at
    const result = await c.env.DB.prepare(`insert or ignore into favolog(user, tweet_id, author, private, text, url, favorited_at, posted_at) values(?, ?, ?, ?, ?, ?, ?, ?)`)
      .bind(user, tweetId, body.author, is_private, body.text, body.url, getUnixtimeSeconds(favorited_at), getUnixtimeSeconds(posted_at))
      .run()
    if (result.error) return c.json({error: result.error}, 500)
    // already reported
    if (result.meta.changes === 0) return c.json({url: body.url}, 208)

    // if public tweet
    if (tweet.__typename === 'Tweet') {
      // insert user profile
      const result = await c.env.DB.prepare(`insert or replace into favolog_user_profile(screen_name, profile_image_url) values(?, ?)`)
        .bind(tweet.user.screen_name, tweet.user.profile_image_url_https)
        .run()
      if (result.error) return c.json({error: result.error}, 500)

      // insert media
      if (tweet.mediaDetails) {
        console.log(`get ${tweet.mediaDetails.length} media(s)`)
        const stmts = tweet.mediaDetails.map(media => {
          switch (media.type) {
            case "photo":
              return { media_type: media.type, media_url: media.media_url_https, media_thumbnail_url: getPhotoThumbnailUrl(media.media_url_https) }
            case "video":
            case "animated_gif":
              return { media_type: media.type, media_url: getLargerVideoUrl(media.video_info), media_thumbnail_url: media.media_url_https }
          }
        }).map(media => {
          return c.env.DB.prepare(`insert into favolog_media(author, tweet_id, media_type, media_url, media_thumbnail_url, created_at) values(?, ?, ?, ?, ?, ?)`)
            .bind(body.author, tweetId, media.media_type, media.media_url, media.media_thumbnail_url, getUnixtimeSeconds(posted_at))
        })
        const results = await c.env.DB.batch(stmts)
        const err = results.find(r => r.error)
        if (err) return c.json({error: err}, 500)
      }
    }

    return c.json({url: body.url}, 201)
  } catch (e) {
    return c.json({ error: e }, 500) 
  }
})

function getTweetIdFromUrl(url: string) {
  return url.split('/').at(-1)
}

function getPhotoThumbnailUrl(url: string) {
  const u = new URL(url)
  u.searchParams.set('format', 'webp')
  u.searchParams.set('name', 'thumb')
  return u.toString()
}

function getLargerVideoUrl(videoInfo: TweetMediaDetailVideoInfo) {
  return videoInfo.variants.sort((a, b) => (a.bitrate && b.bitrate) ? b.bitrate - a.bitrate : 0)[0].url
}

type Tweet = {
  __typename: "Tweet"
  user: TweetUser
  created_at: string
  mediaDetails?: TweetMediaDetail[]
}

type TweetMediaDetail = TweetMediaDetailPhoto | TweetMediaDetailVideo

type TweetMediaDetailPhoto = {
  type: 'photo'
  media_url_https: string
}

type TweetMediaDetailVideo = {
  type: 'video' | 'animated_gif'
  media_url_https: string
  video_info: TweetMediaDetailVideoInfo
}

type TweetMediaDetailVideoInfo = {
  variants: { url: string, content_type: string, bitrate?: number }[]
}

type TweetUser = {
  name: string
  screen_name: string
  profile_image_url_https: ProfileImageUrl
}

type ProfileImageUrl = string
type ProfileImageSize = 'mini' | 'normal' | 'bigger'

function getProfileImageUrl(normal: ProfileImageUrl, size: ProfileImageSize) {
  return normal.replace(/_(normal|mini|bigger)\.(png|jpe?g|gif)$/, `_${size}` + ".$2")
}

type TweetTombstone = {
  __typename: "TweetTombstone"
  tombstone: { text: { text: string }}
}

type TweetResult = Tweet | TweetTombstone

async function fetchTweet(id: string): Promise<TweetResult> {
  const url = `https://cdn.syndication.twimg.com/tweet-result?id=${id}&token=test`
  const headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0",
    "Accept": "*/*",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "Origin": "https://platform.twitter.com",
    "Connection": "keep-alive",
    "Referer": "https://platform.twitter.com/",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "cross-site",
    "Pragma": "no-cache",
    "Cache-Control": "no-cache",
    "TE": "trailers"
  }
  const resp = await fetch(url, {headers: headers})
  if (!resp.ok) {
    throw new Error(`${resp.status} ${resp.statusText} ${await resp.text()}`)
  }
  return await resp.json() as TweetResult
}

function getUnixtimeSeconds(d :Date) {
  return Math.floor(d.getTime() / 1000)
}

export default app
