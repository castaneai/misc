import { json } from "@remix-run/cloudflare";
import { Link, useLoaderData } from "@remix-run/react";

export const loader = async () => {
    const data = await (await fetch('https://httpbin.org/ip')).json() as {origin: string};
    return json({
      posts: [
        {
          slug: "my-first-post",
          title: `My First Post ${data.origin}`,
        },
        {
          slug: "90s-mixtape",
          title: "A Mixtape I Made Just For You",
        },
      ],
    });
  };

export default function Posts() {
    const { posts } = useLoaderData<typeof loader>();
    return (
      <main>
        <h1>Posts</h1>
        <ul>
            {posts.map((post) => (
                <li key={post.slug}>
                    <Link to={post.slug}>{post.title}</Link>
                </li>
            ))}
        </ul>
      </main>
    );
  }
  