import { Hono } from 'hono'
import { AppContainer, Layout } from './view'
import { Media, Post, RichPost, UserProfile, defaultUserProfile } from './types'

type Bindings = {
  DB: D1Database
  USER: string
}

const app = new Hono<{ Bindings: Bindings }>()
const defaultPostsLimit = 100

app.get('/', async (c) => {
  const user = c.env.USER
  if (!user) return c.text('error', 400)
  const limit = parseInt(c.req.query('limit') || `${defaultPostsLimit}`, 10)
  const posts = await getRichPosts(c.env.DB, user, limit)
  const title = `${user}'s favorites`
  const timezone = c.req.raw.cf?.timezone ?? 'Asia/Tokyo';

  return c.html(
    <Layout title={title}>
      <AppContainer title={title} posts={posts} timezone={timezone} />
    </Layout>
  )
})

export default app

async function getRichPosts(db: D1Database, user: string, limit: number): Promise<RichPost[]> {
  const posts = await db.prepare('select * from favolog where user = ? and private = 0 order by favorited_at desc limit ?').bind(user, limit).all<Post>()

  // media
  const mediaMap = new Map(posts.results.map(p => [p.tweet_id, [] as Media[]]))
  const tweetIds = Array.from(mediaMap.keys())
  const medias = await db.prepare(`select * from favolog_media where tweet_id in ${whereIn(tweetIds)}`).all<Media>()
  medias.results.forEach(m => { mediaMap.get(m.tweet_id)?.push(m) })

  // author
  const authorMap = new Map(posts.results.map(p => [p.author, defaultUserProfile]))
  const authorNames = Array.from(authorMap.keys())
  const authors = await db.prepare(`select * from favolog_user_profile where screen_name in ${whereIn(authorNames)}`).all<UserProfile>()
  authors.results.forEach(u => { authorMap.set(u.screen_name, u) })

  return posts.results.map(p => {
    return {
      tweet_id: p.tweet_id,
      url: p.url,
      text: p.text,
      author: authorMap.get(p.author) || defaultUserProfile,
      favorited_at: new Date(p.favorited_at * 1000),
      posted_at: new Date(p.posted_at * 1000),
      medias: mediaMap.get(p.tweet_id) || [],
    }
  })
}

function whereIn(l: string[]) {
  return '(' + l.map(s => `'${s}'`).join(',') + ')'
}
