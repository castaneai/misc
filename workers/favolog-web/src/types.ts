export type Post = {
  tweet_id: string
  author: string
  text: string
  url: string
  favorited_at: number
  posted_at: number
}

export type Media = {
  tweet_id: string
  media_url: string
  media_thumbnail_url: string
}

export type UserProfile = {
  screen_name: string
  profile_image_url: string
}

export const defaultUserProfile: UserProfile = {
  screen_name: 'unknown',
  profile_image_url: 'https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png'
}

export type RichPost = {
  tweet_id: string
  url: string
  text: string
  author: UserProfile
  favorited_at: Date
  posted_at: Date
  medias: Media[]
}
