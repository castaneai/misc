import { css } from 'hono/css'

export const global = css`
// https://hono.dev/helpers/css#global-styles
:-hono-global {
  * {
      box-sizing: border-box;
  }

  html {
    background: #f6f6ef;
  }

  body {
    color: #444;
    margin: 0 auto;
    padding: 0;
    font-family: helvetica neue, arial, sans-serif;
    font-size: 16px;
    line-height: 1.6;
    overflow-wrap: break-word;
  }

  h1 {
    font-size: 1.6em;
  }

  p, ul, li {
    margin: 0;
    padding: 0;
  }

  li {
    list-style: none;
  }
}
`

export const appContainerClass = css`
max-width: 900px;
margin: 0 auto;
padding: 0 0.4em;
`

export const postContainerClass = css`
display: flex;
background: #fff;
padding: 0.6em;
margin: .8em 0;
border-radius: 0.3em;
align-items: flex-start;
`

export const postAsideClass = css`
display: flex;
margin-right: 0.6em;
`

export const postAuthorImageClass = css`
max-width: 48px;
height: auto;
border-radius: 50%;
`

export const postContentClass = css`
display: flex;
flex-direction: column;
width: 100%;
// https://qiita.com/mpyw/items/dfc63c1fed5dfc5eda26#%E6%A8%AA%E6%96%B9%E5%90%91-flex
min-width: 0;
`

export const postContentAuthorClass = css`
color: #828282;

a {
  color: inherit;
  text-decoration: none;
}
`

export const postContentTextClass = css`
white-space: pre-wrap;
`

export const postContentMediaClass = css`
display: flex;
flex-wrap: wrap;
img {
  display: flex;
  max-height: 150px;
  border: 1px solid #eee;
  border-radius: 0.4em;
  margin: 0.2em;
}
`

export const postDateClass = css`
margin-left: 2em;
font-size: small;
color: #828282;

a {
  color: inherit;
  text-decoration: none;
}
a:hover {
  text-decoration: underline;
}
`
