import { PropsWithChildren } from 'hono/jsx'
import { Style } from 'hono/css'
import { appContainerClass, global, postAsideClass, postAuthorImageClass, postContainerClass, postContentAuthorClass, postContentClass, postContentMediaClass, postContentTextClass, postDateClass } from './style'
import { Post, RichPost } from './types'
import { formatInTimeZone } from 'date-fns-tz'

type LayoutProps = {
    title: string
}

export const Layout = ({title, children} : PropsWithChildren<LayoutProps>) =>
  <html>
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="robots" content="noindex,nofollow" />
      <title>{title}</title>
      <Style />
    </head>
    <body class={global}>{children}</body>
  </html>

type AppContainerProps = {
  title: string
  posts: RichPost[]
  timezone: string
}

export const AppContainer = ({ title, posts, timezone } : AppContainerProps) =>
  <div class={appContainerClass}>
    <h1>{title}</h1>
    <ul>
      {posts.map(post => <li><Post post={post} timezone={timezone} /></li>)}
    </ul>
  </div>

type PostProps = {
  post: RichPost
  timezone: string
}

const Post = ({ post, timezone }: PostProps) => 
  <div class={postContainerClass}>
    <div class={postAsideClass}>
      <a href={`https://x.com/${post.author.screen_name}`} target='_blank'>
        <img class={postAuthorImageClass} src={post.author.profile_image_url} />
      </a>
    </div>
    <div class={postContentClass}>
      <div class={postContentAuthorClass}>
        <a href={`https://x.com/${post.author.screen_name}`} target='_blank'>
          @{post.author.screen_name}
        </a>
      </div>
      <div class={postContentTextClass}>{post.text}</div>
      <div class={postContentMediaClass}>
        {post.medias.map(media => <a href={media.media_url} target='_blank'>
          <img src={media.media_thumbnail_url} />
        </a>)}
      </div>
      <div style={{'text-align': 'right', 'margin-top': '0.4em'}}>
        <time class={postDateClass} datetime={post.posted_at.toISOString()}>
          <a href={post.url} target='_blank'>
            {formatInTimeZone(post.posted_at, timezone, "yyyy/MM/dd HH:mm:ss", )}
          </a>
        </time>
      </div>
    </div>
  </div>

// https://developer.x.com/en/docs/twitter-api/v1/accounts-and-users/user-profile-images-and-banners
type ProfileImageSize = 'mini' | 'normal' | 'bigger'

function getSizedProfileImageUrl(normal: string, size: ProfileImageSize) {
  return normal.replace(/_(normal|mini|bigger)\.(png|jpe?g|gif)$/, `_${size}` + ".$2")
}
