const template = (title: string) => `
<style>
  body {
    margin: 1rem;
    font-family: monospace;
  }
</style>
<h2>${title}</h2>
<div>
  <label>Your name: <input id="myName" type="text" /></label>
  <button id="join">Join</button>
</div>
<div>
  <label>Chat: <input id="chat" type="text" /></label>
  <button id="sendChat">Send</button>
</div>
<h4>Logs</h4>
<ul id="events"></ul>
<script>
  let ws
  function websocket(url) {
    ws = new WebSocket(url)
    if (!ws) {
      throw new Error("server didn't accept ws")
    }
    ws.addEventListener("open", ev => {
      addNewEvent('ws opened')
    })
    ws.addEventListener("message", ({ data }) => {
      addNewEvent(data)
    })
    ws.addEventListener("close", () => {
      addNewEvent('ws closed')
    })
  }
  const url = new URL(window.location)
  url.protocol = url.protocol === 'https:' ? "wss" : "ws"
  url.pathname = "/ws"
  websocket(url)
  document.querySelector("#join").addEventListener("click", () => {
    const name = document.querySelector('#myName').value
    ws.send(JSON.stringify({type: 'join', name}))
  })
  document.querySelector("#sendChat").addEventListener("click", () => {
    const message = document.querySelector("#chat").value
    ws.send(JSON.stringify({type: "chat", message}))
    document.querySelector("#chat").value = ""
  })
  const addNewEvent = (data) => {
    const list = document.querySelector("#events")
    const item = document.createElement("li")
    item.innerText = data
    list.prepend(item)
  }
</script>
`

export async function renderTemplate(title: string): Promise<Response> {
  return new Response(template(title), {
    headers: {
      'Content-type': 'text/html; charset=utf-8'
    }
  })
}