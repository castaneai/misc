import { uuid } from "@cfworker/uuid";
import { Env } from ".";

type IncomingMessage = {type: 'join', name: string}
	| {type: 'chat', message: string}
type OutgoingMessage = {type: 'join', name: string} 
	| {type: 'chat', name: string, message: string}
	| {type: 'leave', name: string}

class Member {
	constructor(private ws: WebSocket, public id: string, public name: string) {};
	send(msg: OutgoingMessage) {
		this.ws.send(JSON.stringify(msg))
	}
}

export class ChatRoom {
	private members: Map<WebSocket, Member> = new Map<WebSocket, Member>()
	constructor(private state: DurableObjectState, private env: Env) {}

	async fetch(request: Request) {
		const upgradeHeader = request.headers.get('Upgrade');
		if (!upgradeHeader || upgradeHeader !== 'websocket') {
			return new Response('Expected upgrade: websocket', {status: 426})
		}

		const [client, server] = Object.values(new WebSocketPair())
		await this.handleWebSocket(server)

		return new Response(null, {
			status: 101,
			webSocket: client,
		})
	}

	private async handleWebSocket(ws: WebSocket) {
		ws.accept()
		ws.addEventListener('message', async ({data}) => {
			if (typeof(data) !== 'string') return; // ignore binary data
			const msg = JSON.parse(data) as IncomingMessage
			switch (msg.type) {
			case 'join':
				const id = uuid();
				const newMember = new Member(ws, id, msg.name)
				this.members.set(ws, newMember)
				this.broadcast({type: 'join', name: newMember.name})
				break
			case 'chat':
				const sender = this.members.get(ws)
				if (sender) {
					this.broadcast({type: 'chat', name: sender.name, message: msg.message})
				}
				break
			}
		}) 
		ws.addEventListener('close', _ => {
			const leftMember = this.members.get(ws)
			if (leftMember) {
				this.members.delete(ws)
				this.broadcast({type: 'leave', name: leftMember.name})
			}
		})
	}

	private broadcast(msg: OutgoingMessage) {
		this.members.forEach(member => {
			member.send(msg)
		})
	}
}