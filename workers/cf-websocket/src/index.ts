import {renderTemplate} from "./template"

// In order for the workers runtime to find the class that implements
// our Durable Object namespace, we must export it from the root module.
export { ChatRoom } from './chatroom'

export interface Env {
	ROOMS: DurableObjectNamespace;
}

export default {
	async fetch(request: Request, env: Env): Promise<Response> {
		try {
			const url = new URL(request.url)
			switch (url.pathname) {
				case '/':
					const city: string = (request.cf as any).city || 'unknown'
					return renderTemplate(`Chat in ${city}`)
				case '/ws':
					return wsHandler(request, env)
				default:
					return new Response("not found", {status: 404})
			}
		} catch (err: any) {
			return new Response(err.toString(), {status: 500})
		}
	}
}

async function wsHandler(request: Request, env: Env) {
	const durableObjectsId = env.ROOMS.idFromName("test-room")
	const chatRoom = env.ROOMS.get(durableObjectsId)
	return chatRoom.fetch(request)
}
