# apub-inbox-validator

This Workers protects your ActivityPub server from certain spam.
It will block posts containing 3 or more mentions from reaching your server.

## Usage

### Deploy apub-inbox-validator to Cloudflare Workers

```
pnpm run deploy
```

### Set up a route 

Set up [Workers Routes](https://developers.cloudflare.com/workers/configuration/routing/routes/) as follows

* Route: `your-activitypub-service-host/*`
* Worker: `apub-inbox-validator` 
