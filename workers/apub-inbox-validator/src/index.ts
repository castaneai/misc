export default {
    async fetch(request: Request): Promise<Response> {
        const { pathname } = new URL(request.url)
        if (request.method.toUpperCase() === 'POST' && pathname.endsWith('inbox')) {
            try {
                const body: any = await request.clone().json()
                if (isSpam(body)) {
                    console.log('++++++++++ spam', body.object.id)
                    return new Response(JSON.stringify({error: {message: 'blocked by validator'}}), {status: 202})
                }
            }
            catch (err) {
                console.error('error occured in parsing inbox request body', err)
            }
        }
        return fetch(request)
    },
}

function isSpam(body: any) {
    return body.cc && body.cc.length > 3
}
