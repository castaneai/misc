import { defineConfig } from 'vite'
import devServer from '@hono/vite-dev-server'

export default defineConfig({
  plugins: [
    devServer({
      entry: 'src/index.tsx',
      cf: {
        d1Databases: ['DB']
      }
    }),
  ],
})
