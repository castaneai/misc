import { Hono } from 'hono'

type Env = {
    Bindings: {
        DB: D1Database
    }
}

const app = new Hono<Env>()

app.get('/', async (c) => {
    const result = await c.env.DB.prepare("SELECT 'foo'").first()
    return c.json(result)
})

export default app
