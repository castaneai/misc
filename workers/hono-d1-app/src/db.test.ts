import { describe, test, expect } from 'vitest'
import { Miniflare } from 'miniflare'

describe('db test', () => {
    test('select', async () => {
        const mf = new Miniflare({
            modules: true,
            script: ``,
            d1Databases: { DB: 'db' },
        })
        const db = await mf.getD1Database('DB')
        expect(await db.prepare('select 123').first()).toStrictEqual({123: 123})

        await mf.dispose()
    })
})