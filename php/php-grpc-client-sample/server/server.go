package main

import (
	"context"
	"log"
	"net"
	"time"

	"github.com/castaneai/php-grpc-client-sample/server/proto"
	"google.golang.org/grpc"
)

func main() {
	s := grpc.NewServer()
	proto.RegisterSampleServiceServer(s, &sampleService{})
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatal(err)
	}
	log.Fatal(s.Serve(lis))
}

type sampleService struct {
	proto.UnimplementedSampleServiceServer
}

func (s *sampleService) SampleRpc(ctx context.Context, req *proto.SampleRequest) (*proto.SampleResponse, error) {
	time.Sleep(3 * time.Second)
	return &proto.SampleResponse{Id: "response-" + req.Id}, nil
}
