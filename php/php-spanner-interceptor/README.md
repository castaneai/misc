# php-spanner-interceptor
PHP example of Cloud Spanner access with gRPC interceptor 

## Usage

```
export SPANNER_PROJECT_ID=xxx
export SPANNER_INSTANCE_ID=yyy
export SPANNER_DATABASE_ID=zzz
docker-compose run app php main.php
```