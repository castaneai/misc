<?php

use Grpc\Interceptor;

class TestInterceptor extends Interceptor
{
    public function interceptUnaryUnary($method, $argument, $deserialize, array $metadata = [], array $options = [], $continuation)
    {
        printf('[unary RPC] %s'.PHP_EOL, $method);
        return parent::interceptUnaryUnary($method, $argument, $deserialize, $metadata, $options, $continuation);
    }

    public function interceptUnaryStream($method, $argument, $deserialize, array $metadata = [], array $options = [], $continuation)
    {
        printf('[server stream RPC] %s'.PHP_EOL, $method);
        return parent::interceptUnaryStream($method, $argument, $deserialize, $metadata, $options, $continuation);
    }

    public function interceptStreamUnary($method, $deserialize, array $metadata = [], array $options = [], $continuation)
    {
        printf('[client stream RPC] %s'.PHP_EOL, $method);
        return parent::interceptStreamUnary($method, $deserialize, $metadata, $options, $continuation);
    }

    public function interceptStreamStream($method, $deserialize, array $metadata = [], array $options = [], $continuation)
    {
        printf('[bidirectional stream RPC] %s'.PHP_EOL, $method);
        return parent::interceptStreamStream($method, $deserialize, $metadata, $options, $continuation);
    }
}