<?php

require __DIR__.'/TestInterceptor.php';

use Grpc\Interceptor;

class TestCallInvoker extends \Grpc\DefaultCallInvoker
{
    public function createChannelFactory($hostname, $opts)
    {
        $baseChannel = parent::createChannelFactory($hostname, $opts);
        $interceptor = new TestInterceptor();
        return Interceptor::intercept($baseChannel, $interceptor);
    }
}
