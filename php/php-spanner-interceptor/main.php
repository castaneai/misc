<?php

use Google\Cloud\Spanner\SpannerClient;

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/TestCallInvoker.php';

$projectId = getenv('SPANNER_PROJECT_ID');
$instanceId = getenv('SPANNER_INSTANCE_ID');
$databaseId = getenv('SPANNER_DATABASE_ID');
if (!$projectId || !$instanceId || !$databaseId) {
    throw new Exception('requires 3 env variables: SPANNER_PROJECT_ID, SPANNER_INSTANCE_ID, SPANNER_DATABASE_ID');
}

$callInvoker = new TestCallInvoker();
$baseClient = new Google\Cloud\Spanner\V1\SpannerClient([
    'transportConfig' => [
        'grpc' => [
            'stubOpts' => [
                'grpc_call_invoker' => $callInvoker,
            ]
        ]
    ]
]);

$spannerClientOptions = [
    'projectId' => $projectId,
    'gapicSpannerClient' => $baseClient,
];

$spanner = new SpannerClient($spannerClientOptions);
$db = $spanner->connect($instanceId, $databaseId);
var_dump($db->execute('SELECT 1')->rows()->current());
