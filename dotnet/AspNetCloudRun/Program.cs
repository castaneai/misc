using AspNetCloudRun;
using Google.Cloud.Logging.Console;

var builder = WebApplication.CreateBuilder(args);
if (GcpDetector.OnCloudRun())
{
    builder.WebHost.UseCloudRunPort();
}
if (GcpDetector.OnGoogleCloud())
{
    builder.Logging.AddGoogleCloudConsole();
}
var app = builder.Build();

app.MapGet("/", () =>
{
    var logger = app.Services.GetRequiredService<ILogger<Program>>();
    logger.LogTrace("--- trace");
    logger.LogDebug("--- debug");
    logger.LogInformation("--- info");
    logger.LogWarning("--- warn");
    logger.LogError("--- error");
    logger.LogCritical("--- critical");
    return "Hello World!";
});

app.Run();


internal static class GcpExtensions
{
    public static IWebHostBuilder UseCloudRunPort(this IWebHostBuilder builder)
    {
        var port = Environment.GetEnvironmentVariable("PORT") ?? "8080";
        return builder.UseUrls($"http://*:{port}");
    }
}
