namespace AspNetCloudRun;

public enum GoogleCloudPlatform
{
    UnknownPlatform,
    Gke,
    Gce,
    CloudRun,
    CloudFunctions,
    AppEngineStandard,
    AppEngineFlexible
}

public static class GcpDetector
{
    public static GoogleCloudPlatform DetectPlatform()
    {
        if (OnGke()) return GoogleCloudPlatform.Gke;
        if (OnCloudRun()) return GoogleCloudPlatform.CloudRun;
        if (OnCloudFunctions()) return GoogleCloudPlatform.CloudFunctions;
        if (OnAppEngineStandard()) return GoogleCloudPlatform.AppEngineStandard;
        if (OnAppEngineFlexible()) return GoogleCloudPlatform.AppEngineFlexible;
        
        return GoogleCloudPlatform.UnknownPlatform;
    }

    public static bool OnGoogleCloud() => DetectPlatform() != GoogleCloudPlatform.UnknownPlatform;
    
    public static bool OnGke() => EnvExists("KUBERNETES_SERVICE_HOST");
    public static bool OnGce() => throw new NotImplementedException();
    public static bool OnCloudRun() => EnvExists("K_CONFIGURATION");
    public static bool OnCloudFunctions() => EnvExists("FUNCTION_TARGET");
    public static bool OnAppEngineStandard() => Environment.GetEnvironmentVariable("GAE_ENV") == "standard";
    public static bool OnAppEngineFlexible() => EnvExists("GAE_SERVICE") && !OnAppEngineStandard();

    private static bool EnvExists(string name) => !string.IsNullOrEmpty(Environment.GetEnvironmentVariable(name));
}
