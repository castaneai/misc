A simple example of ASP.NET + Cloud Run

## Build & Deploy

```shell
gcloud auth login
gcloud config set project xxx
export CLOUDRUN_NAME=aspnet-test

# Build with Cloud Run deploying from source code
# https://cloud.google.com/run/docs/deploying-source-code
gcloud run deploy "${CLOUDRUN_NAME}" --source .

# Or build with built-in container support for the .NET SDK
export REGISTRY=xxx-docker.pkg.dev
export IMAGE=foo/bar
make build
make deploy
```
