using System.Net.WebSockets;
using System.Text;
using Microsoft.AspNetCore.Mvc.Testing;

namespace WsTest;

public class Tests
{
    [Test]
    public async Task TestPingPong()
    {
        await using var factory = new WebApplicationFactory<Program>();
        var wsUri = factory.Server.BaseAddress.MakeRelativeUri(new Uri("/ws"));
        var ct = CancellationToken.None;
        using var ws = await factory.Server.CreateWebSocketClient().ConnectAsync(wsUri, ct);
        var client = new WsClient.Client(ws);

        await client.Send(new WsProto.Ping { Payload = "hello" }, ct);
        var response = await client.Receive(ct);
        // Assert.That(response, Is.InstanceOf<WsProto.Pong>().With.Property("Payload").EqualTo("hello"));
    }

    [Test]
    public async Task TestReceiveAndClose()
    {
        var c = new ClientWebSocket();
        await c.ConnectAsync(new Uri("ws://localhost:5002/rooms/test"), default);

        Task.Run(() =>
        {
            try
            {
                while (true)
                {
                    var buffer = new byte[1024];
                    c.ReceiveAsync(buffer, default);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error in receive: ${ex}");
            }
        });

        Console.WriteLine("client closing...");
        await c.CloseAsync(WebSocketCloseStatus.NormalClosure, null, default);
        Console.WriteLine("client close ok");
        await Task.Delay(TimeSpan.FromMilliseconds(2000));
    }
}