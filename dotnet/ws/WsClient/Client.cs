﻿using System.Net.WebSockets;
using MemoryPack;
using WsProto;

namespace WsClient;

public class Client : IClient
{
    private readonly WebSocket _client;
    
    public Client(WebSocket client)
    {
        _client = client;
    }
    
    public async Task Send(IMessage message, CancellationToken ct = default)
    {
        var bin = MemoryPackSerializer.Serialize(message);
        await _client.SendAsync(bin, WebSocketMessageType.Binary, false, ct);
    }

    public async Task<IMessage?> Receive(CancellationToken ct = default)
    {
        var buffer = new byte[1024];
        var result = await _client.ReceiveAsync(buffer, ct);
        if (result.CloseStatus.HasValue)
        {
            return null;
        }
        return MemoryPackSerializer.Deserialize<IMessage>(buffer);
    }

    public void Close()
    {
        _client.CloseAsync(WebSocketCloseStatus.NormalClosure, "test", default);
    }
}