# ws

minimal WebSocket server with .NET.

## Example

```sh
dotnet run
websocat ws://localhost:5105
123
123
# Press Ctrl-D to close
```

## Generating protos

```sh
dotnet protogen ./proto/*.proto --csharp_out=./WsServer/Generated
```

