namespace WsProto;

public interface IClient
{
    public Task Send(IMessage message, CancellationToken ct = default);
    public Task<IMessage?> Receive(CancellationToken ct = default);
    public void Close();
}