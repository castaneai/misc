﻿using MemoryPack;

namespace WsProto;

[MemoryPackable]
[MemoryPackUnion(1, typeof(Ping))]
[MemoryPackUnion(2, typeof(Pong))]
[MemoryPackUnion(3, typeof(Authenticate))]
public partial interface IMessage
{
}

[MemoryPackable]
public partial class Ping : IMessage
{
    public string Payload { get; set; }
}

[MemoryPackable]
public partial class Pong : IMessage
{
    public string Payload { get; set; }
}

[MemoryPackable]
public partial class Authenticate : IMessage
{
}