var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();
var handler = new WsServer.Handler();

app.MapGet("/ws", async (context) =>
{
    if (!context.WebSockets.IsWebSocketRequest)
    {
        context.Response.StatusCode = StatusCodes.Status400BadRequest;
        return;
    }

    using var webSocket = await context.WebSockets.AcceptWebSocketAsync();
    var client = new WsClient.Client(webSocket);
    Console.WriteLine($"joined client: {context.Connection.Id}`");

    var ct = context.RequestAborted;
    while (true)
    {
        var message = await client.Receive(ct);
        if (message is null)
        {
            Console.WriteLine($"connection closed: {context.Connection.Id}");
            break;
        }
        await handler.Handle(client, message);
    }
});

app.MapGet("/", () => "Hello, world");

var webSocketOptions = new WebSocketOptions
{
    KeepAliveInterval = TimeSpan.FromSeconds(5)
};
app.UseWebSockets(webSocketOptions);

app.Run();

public partial class Program
{
}