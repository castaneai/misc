namespace WsServer;

public class Handler
{
    public Handler() {}
    
    public async Task Handle(WsProto.IClient client, WsProto.IMessage message)
    {
        switch (message)
        {
            case WsProto.Ping ping:
                Console.WriteLine("ping received");
                await client.Send(new WsProto.Pong { Payload = ping.Payload });
                break;
            case WsProto.Pong pong:
                Console.WriteLine("pong received");
                break;
            case WsProto.Authenticate auth:
                Console.WriteLine("auth received");
                break;
            default:
                Console.WriteLine($"unknown message received: {message.GetType()}");
                break;
        }
    }
}