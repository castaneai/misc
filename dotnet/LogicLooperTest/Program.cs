﻿// See https://aka.ms/new-console-template for more information

using Cysharp.Threading;

using var looper = new LogicLooper(10);
var timer = default(LogicLooperCoroutine);
Console.WriteLine($"start: {Thread.CurrentThread.ManagedThreadId}");
await looper.RegisterActionAsync((in LogicLooperActionContext ctx) =>
{
    if (timer is null)
    {
        timer = ctx.RunCoroutine(async coCtx =>
        {
            await coCtx.Delay(TimeSpan.FromMilliseconds(1000));
            Console.WriteLine($"--- timer fired: {Thread.CurrentThread.ManagedThreadId}");
        });
    }
    else
    {
        if (timer.IsCompleted)
        {
            timer = null;
        }
    }
    Console.WriteLine($"tick: {Thread.CurrentThread.ManagedThreadId}");
    return true;
});
