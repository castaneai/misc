﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using Server;

namespace ConsoleApp1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var options = new List<ChannelOption>
            {
                new ChannelOption("grpc.keepalive_time_ms", 3000),
                new ChannelOption("grpc.keepalive_timeout_ms", 3000),
                // new ChannelOption("grpc.http2.min_time_between_pings_ms", 1000),
                new ChannelOption("grpc.http2.max_pings_without_data", 0),
            };
            var client =
                new Greeter.GreeterClient(new Grpc.Core.Channel("localhost:5000", ChannelCredentials.Insecure, options));
            using var call = client.SayHello();
            await Task.Delay(TimeSpan.FromDays(1));
            
            Task.Run(async () =>
            {
                while (true)
                {
                    await call.ResponseStream.MoveNext(default);
                    Console.WriteLine(call.ResponseStream.Current.Message);
                }
            });
            for (int i = 0; i < 50000000; i++)
            {
                var name = $"world{i}";
                await call.RequestStream.WriteAsync(new HelloRequest {Name = name});
                Console.WriteLine($"written: {name}");
                await Task.Delay(TimeSpan.FromSeconds(10));
            }
        }
    }
}