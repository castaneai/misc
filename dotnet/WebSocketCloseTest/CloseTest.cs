using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Channels;

namespace WebSocketCloseTest;

public class Tests
{
    private enum ServerOperation
    {
        CloseTcp,
        CloseWebSocket,
    }

    private class TestServer
    {
        private readonly IPEndPoint _listenAddress;
        private readonly Channel<ServerOperation> _operationChannel = Channel.CreateBounded<ServerOperation>(1024);

        public TestServer()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var cts = new CancellationTokenSource();
            var ct = cts.Token;
            Task.Run(async () =>
            {
                var client = await listener.AcceptTcpClientAsync();
                var wsClient = WebSocket.CreateFromStream(client.GetStream(), true, null, Timeout.InfiniteTimeSpan);
                await HandleClient(wsClient, ct);
            });
            _listenAddress = (listener.LocalEndpoint as IPEndPoint)!;
        }

        public WebSocket Connect()
        {
            var client = new TcpClient();
            client.Connect(_listenAddress);
            return WebSocket.CreateClientWebSocket(client.GetStream(), null, 1024, 1024, Timeout.InfiniteTimeSpan, false, null);
        }

        public void RunOperation(ServerOperation operation)
        {
            _operationChannel.Writer.TryWrite(operation);
        }

        private async Task HandleClient(WebSocket client, CancellationToken ct)
        {
            var buffer = new byte[1024];
            var recvTask = client.ReceiveAsync(buffer, ct);
            var opeTask = _operationChannel.Reader.ReadAsync(ct).AsTask();
            while (true)
            {
                await Task.WhenAny(recvTask, opeTask);
                if (recvTask.IsCompleted)
                {
                    var result = recvTask.Result;
                    switch (result.MessageType)
                    {
                        case WebSocketMessageType.Text:
                        case WebSocketMessageType.Binary:
                            var reply = buffer.Take(result.Count).ToArray();
                            await client.SendAsync(reply, result.MessageType, true, ct);
                            break;
                        case WebSocketMessageType.Close:
                            await client.CloseOutputAsync(result.CloseStatus!.Value, result.CloseStatusDescription, ct);
                            break;
                    }
                    recvTask = client.ReceiveAsync(buffer, ct);
                }

                if (opeTask.IsCompleted)
                {
                    var operation = opeTask.Result;
                    switch (operation)
                    {
                        case ServerOperation.CloseTcp:
                            client.Dispose();
                            break;
                        case ServerOperation.CloseWebSocket:
                            await client.CloseAsync(WebSocketCloseStatus.NormalClosure, null, ct);
                            break;
                    }
                    opeTask = _operationChannel.Reader.ReadAsync(ct).AsTask();
                }
            }
        }
    }

    [Test]
    public async Task TcpClosedByServer()
    {
        var server = new TestServer();
        var client = server.Connect();
        await using var _ = new Timer(_ => server.RunOperation(ServerOperation.CloseTcp), 
            null, TimeSpan.FromSeconds(1), Timeout.InfiniteTimeSpan);

        AssertWebSocketError(async () => await client.ReceiveString(), WebSocketError.ConnectionClosedPrematurely);
        AssertWebSocketError(async () => await client.SendString("hello"), WebSocketError.InvalidState);
        AssertWebSocketError(async () => await client.ReceiveString(), WebSocketError.InvalidState);
    }
    
    [Test]
    public async Task WebSocketClosedByServer()
    {
        var server = new TestServer();
        var client = server.Connect();
        await using var _ = new Timer(_ => server.RunOperation(ServerOperation.CloseWebSocket), 
            null, TimeSpan.FromSeconds(1), Timeout.InfiniteTimeSpan);

        var buffer = new byte[1024];
        var result = await client.ReceiveAsync(buffer, default);
        Assert.That(result.MessageType, Is.EqualTo(WebSocketMessageType.Close));
        Assert.That(result.CloseStatus, Is.EqualTo(WebSocketCloseStatus.NormalClosure));
        await client.CloseOutputAsync(result.CloseStatus!.Value, result.CloseStatusDescription, default);
        
        AssertWebSocketError(async () => await client.SendString("hello"), WebSocketError.InvalidState);
        AssertWebSocketError(async () => await client.ReceiveString(), WebSocketError.InvalidState);
    }
    
    [Test]
    public async Task TcpClosedByClient()
    {
        var server = new TestServer();
        var client = server.Connect();
        await using var _ = new Timer(_ => client.Dispose(), 
            null, TimeSpan.FromSeconds(1), Timeout.InfiniteTimeSpan);

        AssertWebSocketError(async () => await client.ReceiveString(), WebSocketError.ConnectionClosedPrematurely);
        AssertWebSocketError(async () => await client.SendString("hello"), WebSocketError.InvalidState);
        AssertWebSocketError(async () => await client.ReceiveString(), WebSocketError.InvalidState);
    }
    
    [Test]
    public async Task WebSocketClosedByClient()
    {
        var server = new TestServer();
        var client = server.Connect();
        await using var _ = new Timer(async _ =>
            {
                await client.CloseAsync(WebSocketCloseStatus.NormalClosure, null, default);
            }, 
            null, TimeSpan.FromSeconds(1), Timeout.InfiniteTimeSpan);

        var buffer = new byte[1024];
        var result = await client.ReceiveAsync(buffer, default);
        Assert.That(result.MessageType, Is.EqualTo(WebSocketMessageType.Close));
        Assert.That(result.CloseStatus, Is.EqualTo(WebSocketCloseStatus.NormalClosure));

        AssertWebSocketError(async () => await client.SendString("hello"), WebSocketError.InvalidState);
        AssertWebSocketError(async () => await client.ReceiveString(), WebSocketError.InvalidState);
    }

    private void AssertWebSocketError(AsyncTestDelegate action, WebSocketError error)
    {
        var ex = Assert.CatchAsync<WebSocketException>(action);
        Assert.That(ex, Is.Not.Null);
        Assert.That(ex.WebSocketErrorCode, Is.EqualTo(error));
    }
}

internal static class WebSocketExtensions
{
    public static Task SendString(this WebSocket ws, string str, CancellationToken ct = default)
    {
        return ws.SendAsync(Encoding.UTF8.GetBytes(str), WebSocketMessageType.Text, true, ct);
    }
    
    public static async Task<string> ReceiveString(this WebSocket ws, CancellationToken ct = default)
    {
        var buffer = new byte[1024];
        var result = await ws.ReceiveAsync(buffer, ct);
        return Encoding.UTF8.GetString(buffer, 0, result.Count);
    }
}