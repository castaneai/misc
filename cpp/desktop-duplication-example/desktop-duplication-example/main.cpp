﻿#include <iostream>
#include <fstream>

#include <d3d11.h>
#include <dxgi1_5.h>
#include <ScreenGrab.h>

using namespace std;

int main()
{
	HRESULT hr = S_OK;



	// create adapter
	IDXGIFactory5* factory;
	hr = CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, __uuidof(IDXGIFactory5), reinterpret_cast<void**>(&factory));
	if (FAILED(hr))
	{
		cout << "failed to crate dxgi factory" << endl;
		return 1;
	}
	IDXGIAdapter1* adapter;
	hr = factory->EnumAdapters1(0, &adapter);
	if (FAILED(hr))
	{
		cout << "faield to get DXGI adapter" << endl;
		return 1;
	}
	DXGI_ADAPTER_DESC desc;
	hr = adapter->GetDesc(&desc);
	if (FAILED(hr))
	{
		cout << "failed to get desc" << endl;
		return 1;
	}
	wcout << "adapter: " << desc.Description << endl;



	// create device
	D3D_FEATURE_LEVEL FeatureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_1
	};
	const UINT numFeatureLevels = ARRAYSIZE(FeatureLevels);
	D3D_FEATURE_LEVEL featureLevel;
	ID3D11Device* device;
	ID3D11DeviceContext* context;
	hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0, FeatureLevels, numFeatureLevels,
		D3D11_SDK_VERSION, &device, &featureLevel, &context);
	if (FAILED(hr))
	{
		cout << "failed to create d3d11 device, hr: " << hr << endl;
		return 1;
	}



	// create output
	IDXGIOutput* output;
	hr = adapter->EnumOutputs(0, &output);
	if (FAILED(hr))
	{
		cout << "enum outputs not found" << endl;
		return 1;
	}
	DXGI_OUTPUT_DESC odesc;
	hr = output->GetDesc(&odesc);
	if (FAILED(hr))
	{
		cout << "failed to get desc output" << endl;
		return 1;
	}
	wcout << "output: " << odesc.DeviceName << endl;
	IDXGIOutput5* output5;
	hr = output->QueryInterface(__uuidof(IDXGIOutput5), reinterpret_cast<void**>(&output5));
	if (FAILED(hr))
	{
		cout << "faield to QI for output5" << endl;
		return 1;
	}



	// start duplication
	DXGI_FORMAT formats[] =
	{
		DXGI_FORMAT_B8G8R8A8_UNORM,
	};
	const UINT formatsNum = ARRAYSIZE(formats);
	IDXGIOutputDuplication* dupl;
	hr = output5->DuplicateOutput1(device, 0, formatsNum, formats, &dupl);
	if (FAILED(hr))
	{
		cout << "failed to duplicate output, hr: " << hr << endl;
		return 1;
	}
	DXGI_OUTDUPL_DESC duplDesc;
	dupl->GetDesc(&duplDesc);
	cout << "start duplication: (" << duplDesc.ModeDesc.Width << "x" << duplDesc.ModeDesc.Height << ")" << endl;
	if (duplDesc.DesktopImageInSystemMemory)
	{
		cout << "notice: desktop image in system memory: true" << endl;
	}
	

	DXGI_OUTDUPL_FRAME_INFO info;

	// acquire desktop image to GPU texture
	for (;;)
	{
		IDXGIResource* desktopResource;
		hr = dupl->AcquireNextFrame(500, &info, &desktopResource);
		if (hr == DXGI_ERROR_WAIT_TIMEOUT)
		{
			cout << "duplicate timeout" << endl;
			return 1;
		}
		if (FAILED(hr))
		{
			cout << "failed to acquire next frame, hr: " << hr << endl;
			return 1;
		}

		DXGI_MAPPED_RECT surf;

		if (duplDesc.DesktopImageInSystemMemory)
		{
			hr = dupl->MapDesktopSurface(&surf);
			if (FAILED(hr))
			{
				cout << "failed to map desktop surface, hr: " << hr << endl;
				dupl->ReleaseFrame();
				return 1;
			}
		}
		else
		{
			// create CPU accessible staging texture
			const RECT rect{ 0, 0, 1920, 1080 };
			const int width = rect.right - rect.left;
			const int height = rect.bottom - rect.top;
			D3D11_TEXTURE2D_DESC stageDesc{};
			RtlZeroMemory(&stageDesc, sizeof(D3D11_TEXTURE2D_DESC));
			stageDesc.Width = width;
			stageDesc.Height = height;
			stageDesc.MipLevels = 1;
			stageDesc.ArraySize = 1; /* When using a texture array. */
			stageDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; /* This is the default data when using desktop duplication, see https://msdn.microsoft.com/en-us/library/windows/desktop/hh404611(v=vs.85).aspx */
			stageDesc.SampleDesc.Count = 1; /* MultiSampling, we can use 1 as we're just downloading an existing one. */
			stageDesc.Usage = D3D11_USAGE_STAGING;
			stageDesc.BindFlags = 0;
			stageDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			stageDesc.MiscFlags = 0;
			ID3D11Texture2D* stage;
			hr = device->CreateTexture2D(&stageDesc, nullptr, &stage);
			if (FAILED(hr))
			{
				cout << "failed to create staging texture 2d, hr: " << hr << endl;
				dupl->ReleaseFrame();
				return 1;
			}

			ID3D11Texture2D* desktopTex;
			hr = desktopResource->QueryInterface(__uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&desktopTex));
			desktopResource->Release();
			desktopResource = nullptr;
			if (FAILED(hr))
			{
				cout << "failed to QI for desktop tex, hr: " << hr << endl;
				dupl->ReleaseFrame();
				return 1;
			}

			D3D11_BOX srcBox;
			srcBox.left = rect.left;
			srcBox.top = rect.top;
			srcBox.right = rect.right;
			srcBox.bottom = rect.bottom;
			srcBox.front = 0;
			srcBox.back = 1;
			context->CopySubresourceRegion(stage, 0, rect.left, rect.top, 0, desktopTex, 0, &srcBox);

			D3D11_MAPPED_SUBRESOURCE map;
			hr = context->Map(stage, 0, D3D11_MAP_READ, 0, &map);
			if (FAILED(hr))
			{
				cout << "failed to map subresource, hr: " << hr << endl;
				dupl->ReleaseFrame();
				return 1;
			}

			if (info.TotalMetadataBufferSize > 0)
			{
				ofstream file("capture.raw", std::ios::binary);
				file.write(reinterpret_cast<const char*>(map.pData), map.DepthPitch);
				cout << "file saved: capture.raw (width: " << width << ", height: " << height << ")" << endl;
				break;
			}
			context->Unmap(stage, 0);
		}


		hr = dupl->ReleaseFrame();
		if (FAILED(hr))
		{
			cout << "failed to release frame" << endl;
			return 1;
		}
	}

	return 0;
}