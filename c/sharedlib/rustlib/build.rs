fn main() {
    bindgen::Builder::default()
        .header("quiche/quiche/include/quiche.h")
        .generate()
        .unwrap()
        .write_to_file("quiche.rs")
        .unwrap();

    csbindgen::Builder::default()
        .input_bindgen_file("quiche.rs")
        .method_filter(|x| x.starts_with("quiche"))
        .csharp_dll_name("rustlib")
        .generate_csharp_file("./Quiche.g.cs")
        .unwrap();
}
