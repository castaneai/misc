#include <stdio.h>

extern char* hello();

int main(int argc, char const *argv[])
{
    printf("%s\n", hello());
    return 0;
}
