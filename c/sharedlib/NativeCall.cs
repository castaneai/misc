using System.Runtime.InteropServices;

public static class NativeCall
{
    [DllImport("libhello.so")]
    internal static extern IntPtr hello();
}
