#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>

struct SinkElements
{
    GstElement *video_sink;
    GstElement *audio_sink;
};

static void on_pad_added (GstElement *element, GstPad *pad, gpointer data)
{
    struct SinkElements *sinks;
    GstPad *sinkpad = NULL;
    GstCaps *caps;
    GstStructure *str;

    sinks = (struct SinkElements*) data;

    caps = gst_pad_query_caps (pad, NULL);
    str = gst_caps_get_structure (caps, 0);
    const char* name = gst_structure_get_name(str);
    g_print("name: %s\n", name);
    if (strcmp(name, "video/x-raw") == 0) {
        sinkpad = gst_element_get_static_pad (sinks->video_sink, "sink");
        gst_pad_link(pad, sinkpad);
    }
    else if (strcmp(name, "audio/x-raw") == 0) {
        sinkpad = gst_element_get_static_pad(sinks->audio_sink, "sink");
        gst_pad_link(pad, sinkpad);
    }
    else {
        g_print("unknown caps name: %s\n", name);
    }
    gst_caps_unref(caps);
    gst_object_unref(sinkpad);
}

int main(int argc, char* argv[]) {
    GMainLoop *loop;
    GstElement *pipeline, *source, *decoder, *video_conv, *audio_conv, *video_sink, *audio_sink;
    const char* file = "example.mpeg";
    struct SinkElements sinks;

    gst_init(&argc, &argv);

    loop = g_main_loop_new(NULL, FALSE);

    pipeline = gst_pipeline_new("player");
    source = gst_element_factory_make("filesrc", "file-source");
    g_object_set(G_OBJECT(source), "location", file, NULL);
    decoder = gst_element_factory_make("decodebin", "decoder");
    video_conv = gst_element_factory_make("videoconvert", "vconv");
    video_sink = gst_element_factory_make("autovideosink", "video output");
    audio_conv = gst_element_factory_make("audioconvert", "aconv");
    audio_sink = gst_element_factory_make("autoaudiosink", "audio output");

    gst_bin_add_many(GST_BIN(pipeline), source, decoder,
                     video_conv, video_sink,
                     audio_conv, audio_sink, NULL);
    gst_element_link(source, decoder);
    gst_element_link(video_conv, video_sink);
    gst_element_link(audio_conv, audio_sink);
    sinks.video_sink = video_conv;
    sinks.audio_sink = audio_conv;
    g_signal_connect(decoder, "pad-added", G_CALLBACK(on_pad_added), &sinks);

    g_print("now playing: %s\n", file);
    gst_element_set_state(pipeline, GST_STATE_PLAYING);

    g_print("running...\n");
    g_main_loop_run(loop);

    g_print("finished\n");
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(GST_OBJECT(pipeline));
    g_main_loop_unref(loop);

    return 0;
}
