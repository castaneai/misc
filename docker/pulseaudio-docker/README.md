# pulseaudio-docker

## Setup

Setup pulseaudio daemon on your host machine with tcp module in pulseaudio config e.g: `/etc/pulse/default.pa`:

```
load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1 auth-anonymous=1
```

## Usage

```
docker-compose up
```
